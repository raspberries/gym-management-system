import VueRouter from "vue-router";
import Vue from "vue";
import AdminLogin from "@/components/AdminLogin";
import ManageIndex from "@/components/manage/ManageIndex";
import UserManager from "@/components/manage/UserManager";
import OrderManager from "@/components/manage/OrderManager";
import AdminManager from "../components/manage/AdminManager";
import CurriculumManager from "../components/manage/CurriculumManager";
import PrivateEducationManager from "../components/manage/PrivateEducationManager";
import MemberManager from "../components/manage/MemberManager";
import IncomeManager from "../components/manage/IncomeManager";
import EquipmentManager from "../components/manage/EquipmentManager";
import PublicClass from "../store/PublicClass";
import PublicClassManager from "../components/manage/PublicClassManager";

Vue.use(VueRouter)
//获取原型对象上的push函数
const originalPush = VueRouter.prototype.push
//修改原型对象中的push方法
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'AdminLogin',
      component: AdminLogin,

    },
    {
      path: '/admin/index',
      name: 'ManageIndex',
      component: ManageIndex,
      redirect: '/admin/user',
      children: [
        {path: '/admin/user', component: UserManager, name: 'UserManager'},
        {path: '/admin/order', component: OrderManager, name: 'OrderManager'},
        {path: '/admin/adminuser', component: AdminManager, name: 'AdminManager'},
        {path: '/admin/curriculum', component: CurriculumManager, name: 'CurriculumManager'},
        {path: '/admin/publicClass', component: PublicClassManager, name: 'PublicClassManager'},
        {path: '/admin/private', component: PrivateEducationManager, name: 'PrivateEducationManager'},
        {path: '/admin/member', component: MemberManager, name: 'MemberManager'},
        {path: '/admin/income', component: IncomeManager, name: 'IncomeManager'},
        {path: '/admin/equipment', component: EquipmentManager, name: 'EquipmentManager'},
      ],
    }

  ],

})


export default router
