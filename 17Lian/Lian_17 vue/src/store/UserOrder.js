import request from "../utils/axiosutils";

export default {

  namespaced:true,
  active:{


  },
  mutations:{
    requestUserOrderData(state){
        request({
          url:'/admin/findAllOrders',
          method:'post',
        }).then(response=>{
          state.userOrderData=response.data.data;
        });
    },

  },

  getters:{
    getUserOrderData(state){
      return state.userOrderData
    }
  },

  state:{

    userOrderData:[
      {
        id:'',
        userId:'',
        title:'',
        price:'',
        createTime:'',
        userList:'',
      }
    ]

  }

}
