import request from "../utils/axiosutils";
import base_url from "../utils/Constants";

export default {
  namespaced: true,
  active:{

  },
  mutations:{
    requestPublicClassData(state){
      request({
        url:"/admin/findAllPublicClass",
        method:'post',
      }).then(res=>{
        res.data.data.forEach(value=>{
          value.photo= base_url + '/picture/getImage/' + value.photo
        })
        state.publicClassData=res.data.data;
      })

    },
  } ,
  getters:{
    getPublicClassData(state){
      return state.publicClassData;
    }
  },
  state:{
    publicClassData:[
      {
        id:'',
        name:'',
        photo:'',
        title:'',
        time:'',
        privateClassList:'',
      }
    ]

  }

}
