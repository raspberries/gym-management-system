//该文件用于创建Vuex中最为核心的store

import request from "../utils/axiosutils";
import base_url from "../utils/Constants";

export default {
  namespaced: true,

  actions: {

  },

  mutations: {
    //请求用户数据
    requestUserData(state) {
      request({
        url: '/admin/findAllUsers',
        method: 'get',

      }).then(res => {
        res.data.data.records.forEach(value => {
          value.userHeadPortrait = base_url + '/picture/getImage/' + value.userHeadPortrait;
          //注册时间
          this.commit("User/registerTime", value.createdTime)
          value.createdTime = this.getters["User/getHandleCreateTime"];
          //更新时间
          this.commit("User/registerTime", value.modifiedTime);
          value.modifiedTime = this.getters["User/getHandleCreateTime"];
        })
        state.tableData = res.data.data.records;
      })
    },
//注册的时间
    registerTime(state, data) {

      let time = new Date(
        data.substr(0, 4),
        parseInt(data.substr(5, 2)) - 1,
        data.substr(8, 2),
        data.substr(11, 2),
        data.substr(14, 2),
        data.substr(17, 2),
      );
      state.time.year = time.getFullYear();
      this.commit("User/handleZero", time.getMonth() + 1)
      state.time.month = state.timi
      this.commit("User/handleZero", time.getDate())
      state.time.day = state.timi
      this.commit("User/handleZero", time.getHours())
      state.time.hour = state.timi
      this.commit("User/handleZero", time.getMinutes())
      state.time.minute = state.timi
      this.commit("User/handleZero", time.getSeconds())
      state.time.second = state.timi
    },

    // 处理时间
    handleZero(state, timi) {
      if (timi < 10) {
        state.timi = '0' + timi;
      } else {
        state.timi = timi;
      }
    }

  },

  getters: {
    getTableData(state) {
      return state.tableData
    },
    getHandleCreateTime(state) {
      return state.time.year + '-' + state.time.month + '-' + state.time.day + ' ' + state.time.hour + ':' + state.time.minute + ':' + state.time.second;
    },


  },
  state: {


//管理时间
    time: {
      month: '',
      day: '',
      hour: '',
      minute: '',
      second: '',
    },
    timi: '',
    //用户数据
    tableData: [{
      id: '',
      username: '',
      account: '',
      password: '',
      userHeadPortrait: '',
      createdTime: '',
      modifiedTime: '',
    }],
  }


}
