import request from "../utils/axiosutils";
import Vue from 'vue'
import router from '../router'

export default {
  namespaced: true,
  actions: {
    adminLogin(context, value) {
      if (value.account.trim() === '' || value.password.trim() === '') {
        Vue.prototype.$message.error('账号或者密码不能为空');
      } else {
        context.commit("AdminLogin", value)
      }
    }
  },

  mutations: {
    //管理员登录
    AdminLogin(state, value) {
      request({
        url: '/admin/login',
        method: 'post',
        params:{
          account: value.account,
          password:value.password,
        }
      }).then(response => {
        if (response.data.state === 200) {
          Vue.prototype.$message({
            type: "success",
            message: '登录成功'
          })
          window.sessionStorage.setItem("adminInfo", JSON.stringify(response.data.data));
          router.push({
            name: "ManageIndex",
            path: '/admin/index'
          })
        } else {
          Vue.prototype.$message.error('账号或者密码错误');
        }
      })
    }
  },
  state: {

  }
}
