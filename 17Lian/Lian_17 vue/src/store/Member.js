import request from "../utils/axiosutils";
import base_url from "../utils/Constants";

export default {
  namespaced: true,
  action: {},

  mutations: {
    requestMember(state) {
      request({
        url: '/admin/findAllMembers',
        method: "post",
      }).then(request => {
        state.memberData = request.data.data;
      });
    }

  }

  ,
  getters: {
    getMemberData(state) {
      return state.memberData
    }
  },

  state:{
    memberData:[
      {
        id:'',
        orderId:'',
        modifiedTime:'',
        userOrderList:[{}],
        UserList:[{}],
      }
    ],
  }
}
