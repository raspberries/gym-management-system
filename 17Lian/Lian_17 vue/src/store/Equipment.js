import request from "../utils/axiosutils";

export default {
  namespaced:true,
  active:{

  },
  mutations:{
    requestEquipmentData(state){
        request({
          url:'/admin/findAllEquipments',
          method:'get',
        }).then(response=>{
          state.equipmentData=response.data.data.records;
        });
    },
  },
  getters:{
    getEquipmentData(state){

      return state.equipmentData;
    }

  },

  state:{

    equipmentData:[
      {
        id:'',
        userId:'',
        name:'',
        number:'',
        day:'',
        createTime:'',
      }
    ]

  }



}
