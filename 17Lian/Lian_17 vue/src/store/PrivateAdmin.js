import request from "../utils/axiosutils";
import base_url from "../utils/Constants";

export default {
  namespaced:true,
  action:{

  },
  mutations: {
   requestPrivateAdmin(state){
      request({
        url:'/admin/findAllPrivateAdmins',
        method:"post",
      }).then(request=>{
        request.data.data.records.forEach(value=>{
          value.privateHeadPortrait = base_url + '/picture/getImage/' + value.privateHeadPortrait;
        })
        state.privateAdminData=request.data.data.records;
      });
   }

  }

  ,
  getters:{
    getPrivateAdminData(state){
      return state.privateAdminData
    }
  },

  state:{
      privateAdminData:[
        {
          id:'',
          privateName:'',
          privateHeadPortrait:'',
          cumulativeClass:'',
          certificate:'',
          favorableRate:'',
          evaluate:'',
          introduction:'',
      }
      ]
  }


}
