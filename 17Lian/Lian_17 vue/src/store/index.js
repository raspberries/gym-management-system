//该文件用于创建Vuex中最为核心的store
import Vue from 'vue'
//引入Vuex
import Vuex from 'vuex'
import AdminUser from './AdminUser'
import User from "./User";
import PrivateClass from "./PrivateClass"
import PrivateAdmin from "./PrivateAdmin";
import Equipment from "./Equipment";
import UserOrder from "./UserOrder";
import PublicClass from "./PublicClass"
import Member from "./Member";

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    AdminUser: AdminUser,
    User:User,
    PrivateClass:PrivateClass,
    PrivateAdmin:PrivateAdmin,
    Equipment:Equipment,
    UserOrder:UserOrder,
    PublicClass:PublicClass,
    Member:Member,
  }
})
