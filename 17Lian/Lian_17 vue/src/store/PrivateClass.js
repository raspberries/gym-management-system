import request from "../utils/axiosutils";
import base_url from "../utils/Constants";

export default {
  namespaced: true,
  active:{

  },
  mutations:{
    requestPrivateClassData(state){
      request({
        url:"/admin/findAllPrivateClass",
        method:'post',
      }).then(res=>{
        res.data.data.forEach(value=>{
          value.photo= base_url + '/picture/getImage/' + value.photo
        })
        state.privateClass=res.data.data;

      })

    },
  } ,
  getters:{
    getPrivateClass(state){
      return state.privateClass
    }
  },
  state:{
    privateClass:[
      {
        id:'',
        teacherId:'',
        name:'',
        title:'',
        photo:'',
        price:'',
        privateAdminList:[],
      }
    ]

  }

}
