// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

//element-ui的loading
import ElementUI, {Loading} from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
//基于断点的隐藏类
import 'element-ui/lib/theme-chalk/display.css';

import 'babel-polyfill'
//引入store
import store from './store'
//引入路由器
import router from './router'
import VCharts from 'v-charts'
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(Loading)
Vue.use(VCharts)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  router,
  store,
  template: '<App/>'
})
