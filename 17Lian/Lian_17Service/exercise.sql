/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : exercise

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 10/04/2022 19:43:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` bigint(0) NOT NULL COMMENT '管理员Id',
  `admin_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员用户名',
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '管理员', '123456', '123456');

-- ----------------------------
-- Table structure for equipment
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number` int(0) NULL DEFAULT NULL,
  `day` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '7',
  `create_time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of equipment
-- ----------------------------
INSERT INTO `equipment` VALUES ('1', '哑铃', 6, '7', '2022-03-29 11:13:01', 1);
INSERT INTO `equipment` VALUES ('1513112847926333441', '测试', 6, '7', NULL, 0);
INSERT INTO `equipment` VALUES ('1513113478187606017', '测试1', 6, '7', '2022-04-10 07:15:40', 0);
INSERT INTO `equipment` VALUES ('2', '杠铃', 6, '7', '2022-03-29 11:13:01', 0);
INSERT INTO `equipment` VALUES ('3', '椭圆机', 2, '7', '2022-03-29 11:13:01', 0);
INSERT INTO `equipment` VALUES ('4', '动感单车', 4, '7', '2022-03-29 11:13:01', 0);
INSERT INTO `equipment` VALUES ('5', '划船器', 8, '7', '2022-03-29 11:13:01', 0);
INSERT INTO `equipment` VALUES ('6', '跑步机', 6, '7', '2022-03-29 11:13:01', 0);

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `order_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_time` datetime(0) NULL DEFAULT NULL,
  `day` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '会员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('2', '1', '2022-04-01 18:54:22', '7');
INSERT INTO `member` VALUES ('3', '2', '2022-04-01 18:54:22', '7');
INSERT INTO `member` VALUES ('4', '3', '2022-04-01 18:54:22', '7');
INSERT INTO `member` VALUES ('5', '4', '2022-04-01 18:54:22', '7');
INSERT INTO `member` VALUES ('6', '5', '2022-04-01 18:54:22', '7');
INSERT INTO `member` VALUES ('7', '6', '2022-04-01 18:54:22', '7');

-- ----------------------------
-- Table structure for private_admin
-- ----------------------------
DROP TABLE IF EXISTS `private_admin`;
CREATE TABLE `private_admin`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `private_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `private_head_portrait` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cumulative_class` int(0) NULL DEFAULT NULL COMMENT '累计上课',
  `certificate` int(0) NULL DEFAULT NULL COMMENT '证书',
  `favorable_rate` float NULL DEFAULT NULL COMMENT '好评率',
  `evaluate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价',
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绍介',
  `deleted` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of private_admin
-- ----------------------------
INSERT INTO `private_admin` VALUES ('1', '雷丽', '1.jpg', 181, 4, 90, '2', '经历：1、美食营养学教练。擅长：1.减脂塑形，增肌', 1);
INSERT INTO `private_admin` VALUES ('1513115580070801409', '测试', '2.jpg', 145, 5, 88, '4', '经历：', 0);
INSERT INTO `private_admin` VALUES ('2', '严涛', '2.jpg', 214, 3, 96.67, '3', '经历：1。擅长：1。', 0);
INSERT INTO `private_admin` VALUES ('3', '转山', '3.jpg', 187, 4, 100, '2', '经历：1。擅长：1。', 0);
INSERT INTO `private_admin` VALUES ('4', '森林教练', '4.jpg', 685, 0, 98, '5', '经历：国家二级运动员 入行三年，年均授课1000+ 曾多次参加马拉松、越野赛获得名次。擅长：减重减脂、增肌塑形、耐力训练、户外训练、提升运动表现。', 0);

-- ----------------------------
-- Table structure for private_class
-- ----------------------------
DROP TABLE IF EXISTS `private_class`;
CREATE TABLE `private_class`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `teacher_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of private_class
-- ----------------------------
INSERT INTO `private_class` VALUES ('1', '1', '增肌课', '增肌课 托妞', '1.jpg', '260', 0);
INSERT INTO `private_class` VALUES ('2', '2', '减脂课', '增肌课 托妞', '2.jpg', '260', 0);
INSERT INTO `private_class` VALUES ('3', '3', '拉伸课', '拉伸课 托妞', '1.jpg', '260', 0);
INSERT INTO `private_class` VALUES ('4', '4', '拳击课', '拳击课 托妞', '2.jpg', '260', 0);
INSERT INTO `private_class` VALUES ('5', '2', '体态调整', '体态调整 托妞', '1.jpg', '260', 0);

-- ----------------------------
-- Table structure for public_class
-- ----------------------------
DROP TABLE IF EXISTS `public_class`;
CREATE TABLE `public_class`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程图片',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `teacher_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '练教id',
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '格价',
  `deleted` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of public_class
-- ----------------------------
INSERT INTO `public_class` VALUES ('1', '有氧搏击 咪炼健身', '1.jpg', '中强度', '1', '19:20-20:10', 0);
INSERT INTO `public_class` VALUES ('10', '有氧搏击 咪炼健身', '1.jpg', '有氧', '4', '19:20-20:40', 0);
INSERT INTO `public_class` VALUES ('11', '拳击课 拓普', '1.jpg', '拳击', '2', '16:20-17:40', 0);
INSERT INTO `public_class` VALUES ('12', '有氧搏击 咪炼健身', '1.jpg', '有氧', '3', '17:30-18:30', 0);
INSERT INTO `public_class` VALUES ('13', '拳击课 拓普', '1.jpg', '拳击', '2', '19:20-20:40', 0);
INSERT INTO `public_class` VALUES ('14', '拳击课 拓有氧搏击 咪炼健身', '1.jpg', '拳击', '1', '13:20-14:40', 0);
INSERT INTO `public_class` VALUES ('2', '人鱼马甲 咪炼健身', '2.jpg', '中强度、稳定性、马甲线', '2', '20:20-21:10', 0);
INSERT INTO `public_class` VALUES ('3', '体态调整 拓普', '2.jpg', '体态调整 拓普', '3', '20:20-21:20', 0);
INSERT INTO `public_class` VALUES ('4', '拳击课 拓普', '1.jpg', '拳击训练', '4', '19:20-20:40', 0);
INSERT INTO `public_class` VALUES ('5', '综合格斗 拓普', '1.jpg', '综合格斗', '3', '19:20-20:20', 0);
INSERT INTO `public_class` VALUES ('6', '减脂课 拓普', '1.jpg', '减脂课', '2', '19:20-20:40', 0);
INSERT INTO `public_class` VALUES ('7', '拉伸课 拓普', '1.jpg', '拉伸课', '1', '15:20-16:20', 0);
INSERT INTO `public_class` VALUES ('8', '人鱼马甲 咪炼健身', '1.jpg', '人鱼马甲', '2', '19:20-20:40', 0);
INSERT INTO `public_class` VALUES ('9', '拳击课 拓普', '1.jpg', '拳击', '3', '14:50-20:30', 0);

-- ----------------------------
-- Table structure for reserve
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `equipment_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `equipment_cancel` int(0) NULL DEFAULT 0,
  `private_admin_cancel` int(0) NULL DEFAULT 0,
  `public_class_cancel` int(0) NULL DEFAULT 0,
  `private_admin_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `public_class_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reserve
-- ----------------------------
INSERT INTO `reserve` VALUES ('1', '1', '9e6ca4e888613ff7671b60be38b8ac95', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511317929511796738', '2', '9e6ca4e888613ff7671b60be38b8ac95', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511322423289180162', '3', 'e696f601a604f135d5529e998d45190b', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511322890165493762', NULL, '9e6ca4e888613ff7671b60be38b8ac95', 0, 0, 0, '3', NULL);
INSERT INTO `reserve` VALUES ('1511324009780408322', NULL, '9e6ca4e888613ff7671b60be38b8ac95', 0, 0, 0, NULL, '3');
INSERT INTO `reserve` VALUES ('1511340750791184386', '6', 'e696f601a604f135d5529e998d45190b', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511342738585096194', '6', 'e696f601a604f135d5529e998d45190b', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511342976519602178', '6', 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511344595411845121', '6', 'e696f601a604f135d5529e998d45190b', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511348938991128577', NULL, 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511349090455834626', NULL, 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511349704434790401', NULL, 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511350081481777153', NULL, 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511350207801630721', '4', 'e696f601a604f135d5529e998d45190b', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511350635775803393', '5', '9e6ca4e888613ff7671b60be38b8ac95', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511350964982530049', NULL, '52e447fa5285dd1ea71faad9aff1d7f9', 0, 0, 0, '3', NULL);
INSERT INTO `reserve` VALUES ('1511351076999806978', NULL, '52e447fa5285dd1ea71faad9aff1d7f9', 0, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511510568588713986', '6', 'ab0d3e9b01851b292a6eea8d57bcb939', 1, 0, 0, NULL, NULL);
INSERT INTO `reserve` VALUES ('1511511404387680257', NULL, '52e447fa5285dd1ea71faad9aff1d7f9', 0, 1, 0, '2', NULL);
INSERT INTO `reserve` VALUES ('1511511757413904385', NULL, '52e447fa5285dd1ea71faad9aff1d7f9', 0, 0, 1, NULL, '5');

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES ('1', '三体云健身', '589.41', '中海汇德里', '周一至周日 9:30-21:30', '18275103527');
INSERT INTO `store` VALUES ('2', '尼安德特健身', '590.49', '万宏路万宏国际A座1层N17号', '7*24h 自助健身', '13500356631');
INSERT INTO `store` VALUES ('3', '瀚斯健身', '594.90', '环城南路668号云纺国际商贸11层19号', '周一至周四 10:00-22:00', '13639147167');
INSERT INTO `store` VALUES ('4', '悦成健身', '598.30', '新亚洲体育诚悦成商业广场3楼', '7*24h 自助健身', '17753275333');
INSERT INTO `store` VALUES ('5', '玩铁健身', '599.15', '华都花园B区东50号商铺', '周一至周日 10:00-21:00', '15689983839');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户id',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `user_head_portrait` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `money` float(10, 2) NULL DEFAULT NULL COMMENT '金额',
  `created_time` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  `created_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册人',
  `modified_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `modified_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改者',
  `deleted` int(0) NOT NULL DEFAULT 0 COMMENT '是否注销（0：未注销，1：已注销）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('3e87a812e73205a4773f99fc0c76ebe3', 'hutt', '1234', '123456', '1.jpg', 200.00, '2022-04-08 13:14:43', NULL, '2022-04-07 23:11:09', '管理员', 0);
INSERT INTO `user` VALUES ('52e447fa5285dd1ea71faad9aff1d7f9', '测试9', '12345689', '12345689', '9.jpg', 201.00, '2022-04-07 10:37:30', '测试4', '2022-04-07 13:11:41', '管理员', 0);
INSERT INTO `user` VALUES ('9c7a3a14d5a138a29a93cdc386e2cc81', '88888', '88888', '88888', '1.jpg', 0.00, '2022-04-08 13:14:44', '88888', '2022-04-08 13:03:24', '88888', 0);
INSERT INTO `user` VALUES ('9e6ca4e888613ff7671b60be38b8ac95', '测试4', '1234568', '1234568', '8.jpg', 300.00, '2022-04-07 10:37:31', '测试4', '2022-03-29 03:10:54', '测试4', 0);
INSERT INTO `user` VALUES ('ab0d3e9b01851b292a6eea8d57bcb939', '测试1', '123456', '123456', '1.jpg', 400.00, '2022-04-07 10:37:32', '测试1', '2022-03-29 01:53:19', '测试1', 1);
INSERT INTO `user` VALUES ('e149aa3927c4231e2a0852d6794e5205', '测试6', '7777', '123123', '9.jpg', 500.00, '2022-04-07 10:37:33', '测试6', '2022-03-29 14:03:54', '测试6', 0);
INSERT INTO `user` VALUES ('e696f601a604f135d5529e998d45190b', '测试5', '66666', '123123', '9.jpg', 600.00, '2022-04-07 10:37:34', '测试5', '2022-03-29 04:20:23', '测试5', 0);
INSERT INTO `user` VALUES ('fae82cdeb94235cae3ab1e188cdd5633', '测试3', '12345678', '12345678', '2.jpg', 700.00, '2022-04-07 10:37:36', '测试3', '2022-03-29 03:09:58', '测试3', 0);

-- ----------------------------
-- Table structure for user_order
-- ----------------------------
DROP TABLE IF EXISTS `user_order`;
CREATE TABLE `user_order`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` float(50, 0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_order
-- ----------------------------
INSERT INTO `user_order` VALUES ('1', '3e87a812e73205a4773f99fc0c76ebe3', '年卡', 1680, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('1511521827941916674', '52e447fa5285dd1ea71faad9aff1d7f9', '团体课', 30, NULL, 0);
INSERT INTO `user_order` VALUES ('1511523076695302146', '52e447fa5285dd1ea71faad9aff1d7f9', '私教', 30, '2022-04-06 09:55:58', 0);
INSERT INTO `user_order` VALUES ('2', '52e447fa5285dd1ea71faad9aff1d7f9', '季卡', 659, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('3', '9e6ca4e888613ff7671b60be38b8ac95', '月卡', 259, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('4', 'ab0d3e9b01851b292a6eea8d57bcb939', '年卡', 1680, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('5', 'e149aa3927c4231e2a0852d6794e5205', '季卡', 659, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('6', 'e696f601a604f135d5529e998d45190b', '月卡', 259, '2022-04-01 18:16:52', 0);
INSERT INTO `user_order` VALUES ('7', 'fae82cdeb94235cae3ab1e188cdd5633', '月卡', 259, '2022-04-01 18:16:52', 0);

SET FOREIGN_KEY_CHECKS = 1;
