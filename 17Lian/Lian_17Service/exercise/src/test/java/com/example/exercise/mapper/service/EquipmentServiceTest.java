package com.example.exercise.mapper.service;

import com.example.exercise.service.EquipmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EquipmentServiceTest {
    @Autowired
    private EquipmentService equipmentService;

    @Test
    public void test1() {
        System.out.println(equipmentService.findAllEquipments("2", "3"));
    }

    @Test
    public void test2() {
        equipmentService.deleteEquipment("1");
    }

    @Test
    public void test3() {
        equipmentService.recoveryEquipment("1");
    }
}
