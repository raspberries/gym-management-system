package com.example.exercise.mapper.service;

import com.example.exercise.entity.User;
import com.example.exercise.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Test
    public void test1() {
        System.out.println(userService.findByAccount("123456"));
    }

    @Test
    public void test2() {
        User user = new User();
        user.setUsername("测试6");
        user.setAccount("7777");
        user.setPassword("123123");
        user.setUserHeadPortrait("9.jpg");
        userService.register(user);
    }

    @Test
    public void test3() {
        System.out.println(userService.login("1234", "1234"));
    }

    @Test
    public void test4() {
        User user = new User();
        user.setId("3e87a812e73205a4773f99fc0c76ebe3");
        user.setUsername("测试4");
        user.setAccount("1234");
        user.setPassword("1234");
        userService.modify(user);
    }
}
