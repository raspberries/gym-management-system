package com.example.exercise.mapper.service;

import com.example.exercise.mapper.PrivateClassMapper;
import com.example.exercise.mapper.PublicClassMapper;
import com.example.exercise.service.PrivateClassService;
import com.example.exercise.service.PublicClassService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PublicClassServiceTest {
    @Autowired
    private PublicClassMapper publicClassMapper;
    @Autowired
    private PublicClassService publicClassService;

    @Test
    public void test1() {
        System.out.println(publicClassMapper.findAllPublicClass());
    }

    @Test
    public void test2() {
        System.out.println(publicClassService.findAllPublicClass());
    }
}
