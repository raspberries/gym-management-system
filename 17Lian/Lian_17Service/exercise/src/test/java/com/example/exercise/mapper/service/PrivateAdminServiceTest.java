package com.example.exercise.mapper.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.User;
import com.example.exercise.service.AdminService;
import com.example.exercise.service.PrivateAdminService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PrivateAdminServiceTest {

    @Autowired
    private PrivateAdminService privateAdminService;

    @Test
    public void test1() {
        System.out.println(privateAdminService.findAllPrivateAdmins("2", "3"));
    }

    @Test
    public void test2() {
        privateAdminService.deletePrivateAdmin("1");
    }

    @Test
    public void test3() {
        System.out.println(privateAdminService.findOneAdminByName("雷丽"));
    }
}
