package com.example.exercise.mapper.service;

import com.example.exercise.entity.UserOrder;
import com.example.exercise.mapper.UserOrderMapper;
import com.example.exercise.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserOrderServiceTest {
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserOrderMapper orderMapper;

    @Test
    public void test1() {
        System.out.println(orderService.findAllOrder());
    }

    @Test
    public void test2() {
        System.out.println(orderMapper.findAllOrder());
    }

    @Test
    public void test3() {
        UserOrder userOrder = new UserOrder();
        userOrder.setUserId("fae82cdeb94235cae3ab1e188cdd5633");
        userOrder.setTitle("测试");
        userOrder.setPrice(11l);
        orderMapper.modifyOrder(userOrder);
    }

    @Test
    public void test4() {
        System.out.println(orderMapper.findOneById("e696f601a604f135d5529e998d45190b"));
    }

    @Test
    public void test5() {
        UserOrder order = new UserOrder();
        order.setUserId("e696f601a604f135d5529e998d45190b");
        order.setTitle("测试1");
        order.setPrice(2l);
        System.out.println(orderMapper.modifyOrder(order));
    }
}
