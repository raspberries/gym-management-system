package com.example.exercise.mapper.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.exercise.mapper.PrivateClassMapper;
import com.example.exercise.service.PrivateClassService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PrivateClassServiceTest {
    @Autowired
    private PrivateClassMapper privateClassMapper;
    @Autowired
    private PrivateClassService privateClassService;


    @Test
    public void test1() {
        System.out.println(privateClassMapper.findAllPrivateClass());
    }

    @Test
    public void test2() {
        System.out.println(privateClassService.findAllPrivateClass());
    }
}
