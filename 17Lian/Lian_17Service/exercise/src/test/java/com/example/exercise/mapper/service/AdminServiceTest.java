package com.example.exercise.mapper.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.Admin;
import com.example.exercise.entity.User;
import com.example.exercise.service.AdminService;
import org.junit.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AdminServiceTest {

    @Autowired
    private AdminService adminService;

    @Test
    public void test1() {
        System.out.println(adminService.login("123456", "123456"));
    }

    @Test
    public void test2() {
        Page<User> allUsers = adminService.findAllUsers("2", "3");
        allUsers.getRecords().forEach(System.out::println);
        System.out.println(allUsers);
        long size = allUsers.getSize();
        long total = allUsers.getTotal();
        System.out.println(size + "============" + total);
    }

    @Test
    public void test3() {
        adminService.deleteUser("3e87a812e73205a4773f99fc0c76ebe3");
    }

    @Test
    public void test4() {
        adminService.recoveryUser("3e87a812e73205a4773f99fc0c76ebe3");
    }

    @Test
    public void test5() {
        User user = new User();
        user.setUsername("txin");
        user.setPassword("88888");
        adminService.modifyUser("3e87a812e73205a4773f99fc0c76ebe3", user);
    }

    @Test
    public void test6() {
        System.out.println(adminService.findOneUser("1234"));
    }
}
