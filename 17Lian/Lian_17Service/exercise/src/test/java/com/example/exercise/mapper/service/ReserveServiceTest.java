package com.example.exercise.mapper.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.User;
import com.example.exercise.mapper.ReserveMapper;
import com.example.exercise.service.AdminService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ReserveServiceTest {

    @Autowired
    private ReserveMapper reserveMapper;

    @Test
    public void test1() {
        System.out.println(reserveMapper.findReserveEquipment("9e6ca4e888613ff7671b60be38b8ac95"));
    }
    @Test
    public void test2() {
        System.out.println(reserveMapper.findReservePrivateAdmin("9e6ca4e888613ff7671b60be38b8ac95"));
    }
    @Test
    public void test3() {
        System.out.println(reserveMapper.findReservePublicClass("9e6ca4e888613ff7671b60be38b8ac95"));
    }
}
