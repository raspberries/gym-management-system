package com.example.exercise.mapper.service;

import com.example.exercise.mapper.MemberMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MemberServiceTest {

    @Autowired
    private MemberMapper mapper;

    @Test
    public void test1() {
        System.out.println(mapper.findAllMember());

    }
    @Test
    public void test2() {
        System.out.println(mapper.modifyMember("1","年卡","1680"));

    }

}
