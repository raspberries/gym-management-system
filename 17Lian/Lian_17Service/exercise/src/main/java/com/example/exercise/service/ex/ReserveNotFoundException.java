package com.example.exercise.service.ex;

public class ReserveNotFoundException extends ServiceException{
    public ReserveNotFoundException() {
        super();
    }

    public ReserveNotFoundException(String message) {
        super(message);
    }

    public ReserveNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReserveNotFoundException(Throwable cause) {
        super(cause);
    }

    protected ReserveNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
