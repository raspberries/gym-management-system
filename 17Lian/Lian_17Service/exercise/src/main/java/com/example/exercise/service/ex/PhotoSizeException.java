package com.example.exercise.service.ex;

public class PhotoSizeException extends ServiceException{
    public PhotoSizeException() {
        super();
    }

    public PhotoSizeException(String message) {
        super(message);
    }

    public PhotoSizeException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhotoSizeException(Throwable cause) {
        super(cause);
    }

    protected PhotoSizeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
