package com.example.exercise.service.ex;

public class PrivateClassDeletedException extends ServiceException{
    public PrivateClassDeletedException() {
        super();
    }

    public PrivateClassDeletedException(String message) {
        super(message);
    }

    public PrivateClassDeletedException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivateClassDeletedException(Throwable cause) {
        super(cause);
    }

    protected PrivateClassDeletedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
