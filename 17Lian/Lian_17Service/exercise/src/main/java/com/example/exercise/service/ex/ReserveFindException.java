package com.example.exercise.service.ex;

public class ReserveFindException extends ServiceException{
    public ReserveFindException() {
        super();
    }

    public ReserveFindException(String message) {
        super(message);
    }

    public ReserveFindException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReserveFindException(Throwable cause) {
        super(cause);
    }

    protected ReserveFindException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
