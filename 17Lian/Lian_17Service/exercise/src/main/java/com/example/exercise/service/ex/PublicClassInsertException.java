package com.example.exercise.service.ex;

public class PublicClassInsertException extends ServiceException{
    public PublicClassInsertException() {
        super();
    }

    public PublicClassInsertException(String message) {
        super(message);
    }

    public PublicClassInsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public PublicClassInsertException(Throwable cause) {
        super(cause);
    }

    protected PublicClassInsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
