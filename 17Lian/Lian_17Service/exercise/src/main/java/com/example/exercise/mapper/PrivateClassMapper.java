package com.example.exercise.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.PrivateClass;

import java.util.List;

public interface PrivateClassMapper extends BaseMapper<PrivateClass>{
    List<PrivateClass> findAllPrivateClass();
}
