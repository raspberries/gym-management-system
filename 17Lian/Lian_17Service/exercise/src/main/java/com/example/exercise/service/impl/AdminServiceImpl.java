package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.Admin;
import com.example.exercise.entity.User;
import com.example.exercise.mapper.AdminMapper;
import com.example.exercise.mapper.UserMapper;
import com.example.exercise.service.AdminService;
import com.example.exercise.service.ex.AdminNotFoundException;
import com.example.exercise.service.ex.UserNotFoundException;
import com.example.exercise.service.ex.UserUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// Service层叫服务层，被称为服务，可以理解就是对一个或多个DAO进行的再次封装，
// 封装成一个服务，所以这里也就不会是一个原子操作了，需要事物控制。
// service层主要负责业务模块的应用逻辑应用设计。
// 同样是首先设计接口，再设计其实现类，接着再Spring的配置文件中配置其实现的关联。
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired(required = false)
    private AdminMapper adminMapper;
    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public Admin login(String account, String password) {
        // QueryWrapper:条件构造器
        // 封装sql对象，包括where条件，order by排序，select哪些字段等等。
        QueryWrapper query = new QueryWrapper();

        // hashmap.put（K key，V value）
        Map<String, Object> map = new HashMap();
        map.put("account", account);
        map.put("password", password);
        // allEq()该方法用来查找所有符合条件的数据
        query.allEq(map);
        // BaseMapper方法selectOne
        Admin admin = adminMapper.selectOne(query);
        if (admin == null) {
            throw new AdminNotFoundException("账号或密码错误");
        }
        return admin;
    }

    @Override
    public Page<User> findAllUsers(String current, String size) {
        // MyBatis-Plus分页查询——Page
        Page<User> page = new Page<>(Integer.parseInt(current), Integer.parseInt(size));
        QueryWrapper query = new QueryWrapper();
        // notIn() 不符合多个条件的值
        query.notIn("deleted", 1);
        query.orderByDesc("created_time");
        Page<User> data = userMapper.selectPage(page, query);
        return data;
    }

    @Override
    public void deleteUser(String id) {
        User user = userMapper.selectById(id);
        if (user == null) {
            throw new UserNotFoundException("账号未注册");
        }
        user.setDeleted(1);
        //更新日志
        user.setModifiedUser("管理员");
        user.setModifiedTime(new Date());
        int row = userMapper.updateById(user);
        if (row != 1) {
            throw new UserUpdateException("用户在更新时产生异常");
        }
    }

    // 恢复删除用户
    @Override
    public void recoveryUser(String id) {
        User user = userMapper.selectById(id);
        if (user == null) {
            throw new UserNotFoundException("账号未注册");
        }
        user.setDeleted(0);
        //更新日志
        user.setModifiedUser("管理员");
        user.setModifiedTime(new Date());
        int row = userMapper.updateById(user);
        if (row != 1) {
            throw new UserUpdateException("用户在更新时产生异常");
        }
    }

    @Override
    public User modifyUser(String id, User newUser) {
        User user = userMapper.selectById(id);
        if (user == null) {
            throw new UserNotFoundException("用户未注册");
        }
        user.setUsername(newUser.getUsername());
        user.setMoney(newUser.getMoney());
        user.setPassword(newUser.getPassword());
        //填写日志
        user.setModifiedUser("管理员");
        user.setModifiedTime(new Date());
        int row = userMapper.updateById(user);
        if (row != 1) {
            throw new UserUpdateException("用户更新资料异常");
        }
        User data = userMapper.selectById(id);
        return data;
    }

    @Override
    public User findOneUser(String account) {
        User data = userMapper.selectById(account);
        return data;
    }
}
