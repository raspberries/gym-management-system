package com.example.exercise.controller;

import com.example.exercise.controller.ex.ControllerException;
import com.example.exercise.service.ex.*;
import com.example.exercise.util.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class BaseController {
    public static final int OK = 200;

    @ExceptionHandler({ServiceException.class, ControllerException.class})
    public JsonResult<Void> handlerException(Throwable e) {
        JsonResult<Void> result = new JsonResult<>(e);

        if (e instanceof UserExistException) {
            result.setState(5001);
            result.setMessage("账号已注册");
        } else if (e instanceof UserInsertException) {
            result.setState(5002);
            result.setMessage("用户在注册过程中产生了位置的异常");
        } else if (e instanceof UserNotFoundException) {
            result.setState(5003);
            result.setMessage("账号未注册");
        } else if (e instanceof UserPasswordException) {
            result.setState(5004);
            result.setMessage("密码错误");
        } else if (e instanceof UserIsDeletedException) {
            result.setState(5005);
            result.setMessage("该用户已注销");
        } else if (e instanceof UserUpdateException) {
            result.setState(5006);
            result.setMessage("用户更新资料异常");
        } else if (e instanceof AdminNotFoundException) {
            result.setState(5007);
            result.setMessage("账号或密码错误");
        } else if (e instanceof PhotoNameNotFound) {
            result.setState(5008);
            result.setMessage("获取图片失败");
        } else if (e instanceof PhotoSizeException) {
            result.setState(5009);
            result.setMessage("图片获取失败,原因:图片太大(不能大于10M)");
        } else if (e instanceof PhotoException) {
            result.setState(5010);
            result.setMessage("图片未知异常");
        } else if (e instanceof PrivateAdminNotFoundException) {
            result.setState(5011);
            result.setMessage("私教不存在");
        } else if (e instanceof PrivateAdminUpdateException) {
            result.setState(5012);
            result.setMessage("私教更新时产生异常");
        } else if (e instanceof EquipmentNotFoundException) {
            result.setState(5013);
            result.setMessage("器材未注册");
        } else if (e instanceof EquipmentUpdateException) {
            result.setState(5014);
            result.setMessage("器材更新时产生异常");
        } else if (e instanceof OrderNotFoundException) {
            result.setState(5015);
            result.setMessage("用户订单未找到");
        } else if (e instanceof MemberUpdateException) {
            result.setState(5016);
            result.setMessage("会员卡更新时异常");
        } else if (e instanceof ReserveInsertException) {
            result.setState(5017);
            result.setMessage("预约插入时异常");
        } else if (e instanceof ReserveUpdateException) {
            result.setState(5018);
            result.setMessage("预约更新时异常");
        } else if (e instanceof ReserveNotFoundException) {
            result.setState(5019);
            result.setMessage("预约订单不存在");
        } else if (e instanceof ReserveFindException) {
            result.setState(5020);
            result.setMessage("预约查询失败");
        } else if (e instanceof EquipmentSizeException) {
            result.setState(5021);
            result.setMessage("器材数量长度异常");
        }
        return result;
    }
}
