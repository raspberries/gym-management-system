package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserOrder {
    private String id;
    private String userId;
    private String title;
    private float price;
    private String createTime;
    private String deleted;
    List<User> userList;
}
