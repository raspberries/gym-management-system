package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.exercise.entity.PrivateAdmin;
import com.example.exercise.entity.PublicClass;
import com.example.exercise.mapper.PrivateAdminMapper;
import com.example.exercise.mapper.PublicClassMapper;
import com.example.exercise.service.PublicClassService;
import com.example.exercise.service.ex.PrivateAdminNotFoundException;
import com.example.exercise.service.ex.PublicClassDeletedExcption;
import com.example.exercise.service.ex.PublicClassInsertException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PublicClassServiceImpl implements PublicClassService {
    @Autowired(required = false)
    private PublicClassMapper publicClassMapper;
    @Autowired(required = false)
    private PrivateAdminMapper privateAdminMapper;

    @Override
    public List<PublicClass> findAllPublicClass() {
        List<PublicClass> data = publicClassMapper.findAllPublicClass();
        return data;
    }

    @Override
    public void insertPublicClass(String teacherName, PublicClass publicClass) {
        QueryWrapper<PrivateAdmin> query = new QueryWrapper<PrivateAdmin>();
        query.eq("private_name", teacherName);
        PrivateAdmin privateAdmin = privateAdminMapper.selectOne(query);
        if (privateAdmin == null) {
            throw new PrivateAdminNotFoundException("私教未注册");
        }
        publicClass.setTeacherId(privateAdmin.getId());
        int rows = publicClassMapper.insert(publicClass);
        if (rows != 1) {
            throw new PublicClassInsertException("团体课插入时异常");
        }
    }

    @Override
    public void deletePublicClass(String id) {
        int rows = publicClassMapper.deleteById(id);
        if (rows != 1) {
            throw new PublicClassDeletedExcption("团体课删除时异常");
        }
    }
}
