package com.example.exercise.service.ex;

public class ReserveUpdateException extends ServiceException{
    public ReserveUpdateException() {
        super();
    }

    public ReserveUpdateException(String message) {
        super(message);
    }

    public ReserveUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReserveUpdateException(Throwable cause) {
        super(cause);
    }

    protected ReserveUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
