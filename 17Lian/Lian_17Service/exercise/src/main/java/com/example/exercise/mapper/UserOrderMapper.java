package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.UserOrder;

import java.util.List;

public interface UserOrderMapper extends BaseMapper<UserOrder> {
    List<UserOrder> findAllOrder();

    Integer modifyOrder(UserOrder userOrder);

    UserOrder findOneById(String id);

}
