package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reserve {
    private String id;
    private String equipmentId;
    private String userId;
    private Integer equipmentCancel;
    private Integer privateAdminCancel;
    private Integer publicClassCancel;
    private String privateAdminId;
    private String publicClassId;
/*    List<Equipment> equipmentList;
    List<PrivateAdmin> privateAdminList;
    List<PublicClass> publicClasses;*/
}
