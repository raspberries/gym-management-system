package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivateClass {
    private String id;
    private String teacherId;
    private String name;
    private String title;
    private String photo;
    private String price;
    private Integer deleted;
    private List<PrivateAdmin> privateAdminList;
}
