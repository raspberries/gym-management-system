package com.example.exercise.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {
    @TableId(type = IdType.UUID)
    private String id;
    private String username;
    private String account;
    private String password;
    private String userHeadPortrait;
    private float money;
    private Integer deleted;
}
