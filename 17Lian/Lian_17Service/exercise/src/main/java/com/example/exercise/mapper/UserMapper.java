package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
