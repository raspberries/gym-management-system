package com.example.exercise.mapper;

import com.example.exercise.entity.Member;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberMapper {
    List<Member> findAllMember();

    Integer modifyMember(@Param("userId") String userId, @Param("title") String title, @Param("price") String price);
}
