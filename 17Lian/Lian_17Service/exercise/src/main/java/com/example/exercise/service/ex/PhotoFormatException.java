package com.example.exercise.service.ex;

public class PhotoFormatException extends ServiceException{
    public PhotoFormatException() {
        super();
    }

    public PhotoFormatException(String message) {
        super(message);
    }

    public PhotoFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhotoFormatException(Throwable cause) {
        super(cause);
    }

    protected PhotoFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
