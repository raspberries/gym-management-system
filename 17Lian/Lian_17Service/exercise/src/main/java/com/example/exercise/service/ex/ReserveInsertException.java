package com.example.exercise.service.ex;

public class ReserveInsertException extends ServiceException{
    public ReserveInsertException() {
        super();
    }

    public ReserveInsertException(String message) {
        super(message);
    }

    public ReserveInsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReserveInsertException(Throwable cause) {
        super(cause);
    }

    protected ReserveInsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
