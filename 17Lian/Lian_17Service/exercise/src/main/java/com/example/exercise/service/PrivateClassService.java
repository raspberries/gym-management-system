package com.example.exercise.service;

import com.example.exercise.entity.PrivateClass;

import java.util.List;

public interface PrivateClassService {
    List<PrivateClass> findAllPrivateClass();

    void insertPrivateClass(String teacherName, PrivateClass privateClass);

    void deletePrivateClass(String id);
}
