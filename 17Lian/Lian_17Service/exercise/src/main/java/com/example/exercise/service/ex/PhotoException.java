package com.example.exercise.service.ex;

public class PhotoException extends ServiceException{
    public PhotoException() {
        super();
    }

    public PhotoException(String message) {
        super(message);
    }

    public PhotoException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhotoException(Throwable cause) {
        super(cause);
    }

    protected PhotoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
