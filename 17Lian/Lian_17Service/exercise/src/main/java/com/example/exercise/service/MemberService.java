package com.example.exercise.service;

import com.example.exercise.entity.Member;

import java.util.List;

public interface MemberService {
    List<Member> findAllMembers();

    Integer modifyMember(String userId, String title, String price);
}
