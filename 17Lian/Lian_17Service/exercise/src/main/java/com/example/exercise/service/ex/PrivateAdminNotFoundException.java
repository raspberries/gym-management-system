package com.example.exercise.service.ex;

public class PrivateAdminNotFoundException extends ServiceException{
    public PrivateAdminNotFoundException() {
        super();
    }

    public PrivateAdminNotFoundException(String message) {
        super(message);
    }

    public PrivateAdminNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivateAdminNotFoundException(Throwable cause) {
        super(cause);
    }

    protected PrivateAdminNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
