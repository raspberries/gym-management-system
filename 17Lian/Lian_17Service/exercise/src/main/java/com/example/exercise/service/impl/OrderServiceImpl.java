package com.example.exercise.service.impl;

import com.example.exercise.entity.UserOrder;
import com.example.exercise.mapper.UserOrderMapper;
import com.example.exercise.service.OrderService;
import com.example.exercise.service.ex.OrderNotFoundException;
import com.example.exercise.service.ex.OrderUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired(required = false)
    private UserOrderMapper userOrderMapper;

    @Override
    public List<UserOrder> findAllOrder() {
        List<UserOrder> data = userOrderMapper.findAllOrder();
        return data;
    }

    @Override
    public Integer modifyOrder(UserOrder userOrder) {
        UserOrder order = findOneById(userOrder.getId());
        order.setTitle(userOrder.getTitle());
        order.setPrice(userOrder.getPrice());
        Integer rows = userOrderMapper.modifyOrder(order);
        if (rows != 1) {
            throw new OrderUpdateException("订单更新时异常");
        }
        return rows;
    }

    @Override
    public UserOrder findOneById(String id) {
        UserOrder order = userOrderMapper.findOneById(id);
        if (order == null) {
            throw new OrderNotFoundException("用户订单未找到");
        }
        return order;
    }

    @Override
    public void buyOfOrder(String userId, String title, float price) {
        UserOrder order = new UserOrder();
        order.setPrice(price);
        order.setTitle(title);
        order.setUserId(userId);
        Date date = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String format = sf.format(date);
        order.setCreateTime(format);
        int rows = userOrderMapper.insert(order);
        if (rows != 1) {
            throw new OrderUpdateException("订单更新时异常");
        }
    }
}
