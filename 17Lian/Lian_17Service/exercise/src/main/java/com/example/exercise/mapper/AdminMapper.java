package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.Admin;

// mybatis-plus BaseMapper:集成了基础的增删改查方法
public interface AdminMapper extends BaseMapper<Admin> {

}
