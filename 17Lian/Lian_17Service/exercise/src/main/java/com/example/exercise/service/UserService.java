package com.example.exercise.service;

import com.example.exercise.entity.User;

public interface UserService {
    User login(String account, String password);

    void register(User user);

    User findByAccount(String account);

    User modify(User user);
}
