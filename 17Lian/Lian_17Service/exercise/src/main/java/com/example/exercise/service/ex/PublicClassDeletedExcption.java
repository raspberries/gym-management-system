package com.example.exercise.service.ex;

public class PublicClassDeletedExcption extends ServiceException{

    public PublicClassDeletedExcption() {
        super();
    }

    public PublicClassDeletedExcption(String message) {
        super(message);
    }

    public PublicClassDeletedExcption(String message, Throwable cause) {
        super(message, cause);
    }

    public PublicClassDeletedExcption(Throwable cause) {
        super(cause);
    }

    protected PublicClassDeletedExcption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
