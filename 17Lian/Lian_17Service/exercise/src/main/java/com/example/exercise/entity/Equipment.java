package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Equipment {
    private String id;
    private String name;
    private String number;
    private String day;
    private String createTime;
    private Integer deleted;
}
