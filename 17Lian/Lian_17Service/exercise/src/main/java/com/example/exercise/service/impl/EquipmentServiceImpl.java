package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.Equipment;
import com.example.exercise.mapper.EquipmentMapper;
import com.example.exercise.service.EquipmentService;
import com.example.exercise.service.ex.EquipmentInsertException;
import com.example.exercise.service.ex.EquipmentNotFoundException;
import com.example.exercise.service.ex.EquipmentUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class EquipmentServiceImpl implements EquipmentService {
    @Autowired(required = false)
    private EquipmentMapper equipmentMapper;

    @Override
    public Page<Equipment> findAllEquipments(String current, String size) {
        Page<Equipment> page = new Page<>(Integer.parseInt(current), Integer.parseInt(size));
        QueryWrapper query = new QueryWrapper();
        query.notIn("deleted", 1);
        query.orderByDesc("create_time");
        Page<Equipment> data = equipmentMapper.selectPage(page, query);
        return data;
    }

    @Override
    public void deleteEquipment(String id) {
        Equipment equipment = equipmentMapper.selectById(id);
        if (equipment == null) {
            throw new EquipmentNotFoundException("器材未注册");
        }
        equipment.setDeleted(1);
        int rows = equipmentMapper.updateById(equipment);
        if (rows != 1) {
            throw new EquipmentUpdateException("器材更新时异常");
        }
    }

    @Override
    public void recoveryEquipment(String id) {
        Equipment equipment = equipmentMapper.selectById(id);
        if (equipment == null) {
            throw new EquipmentNotFoundException("器材未注册");
        }
        equipment.setDeleted(0);
        int rows = equipmentMapper.updateById(equipment);
        if (rows != 1) {
            throw new EquipmentUpdateException("器材更新时异常");
        }
    }

    @Override
    public void modifyEquipment(String id, Equipment newEquipment) {
        Equipment equipment = equipmentMapper.selectById(id);
        if (equipment == null) {
            throw new EquipmentNotFoundException("器材未注册");
        }
        equipment.setName(newEquipment.getName());
        equipment.setNumber(newEquipment.getNumber());
        equipment.setDay(newEquipment.getDay());
        int rows = equipmentMapper.updateById(equipment);
        if (rows != 1) {
            throw new EquipmentUpdateException("器材更新时异常");
        }
    }

    @Override
    public Equipment findOneEquipment(String id) {
        Equipment equipment = equipmentMapper.selectById(id);
        if (equipment == null) {
            throw new EquipmentNotFoundException("器材未注册");
        }
        return equipment;
    }

    @Override
    public void insertEquipment(Equipment equipment) {
        Date date = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String format = sf.format(date);
        equipment.setCreateTime(format);
        int rows = equipmentMapper.insert(equipment);
        if (rows != 1) {
            throw new EquipmentInsertException("器材插入式异常");
        }
    }
}
