package com.example.exercise.service.ex;

public class PhotoNameNotFound extends ServiceException{
    public PhotoNameNotFound() {
        super();
    }

    public PhotoNameNotFound(String message) {
        super(message);
    }

    public PhotoNameNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public PhotoNameNotFound(Throwable cause) {
        super(cause);
    }

    protected PhotoNameNotFound(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
