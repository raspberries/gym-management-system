package com.example.exercise.service.ex;

public class EquipmentUpdateException extends ServiceException{
    public EquipmentUpdateException() {
        super();
    }

    public EquipmentUpdateException(String message) {
        super(message);
    }

    public EquipmentUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public EquipmentUpdateException(Throwable cause) {
        super(cause);
    }

    protected EquipmentUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
