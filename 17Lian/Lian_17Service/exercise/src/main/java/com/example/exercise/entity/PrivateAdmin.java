package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrivateAdmin {
    private String id;
    private String privateName;
    private String privateHeadPortrait;
    private String cumulativeClass;
    private String certificate;
    private String favorableRate;
    private String evaluate;
    private String introduction;
    private Integer deleted;
}
