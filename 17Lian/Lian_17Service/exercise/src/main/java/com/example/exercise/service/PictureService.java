package com.example.exercise.service;

import org.springframework.web.multipart.MultipartFile;

public interface PictureService {

    byte[] getImage(String fileName);

    Object uploadImage(MultipartFile multipartFile);

    byte[] getStaticImage(String fileName);

    Object uploadStaticImage(MultipartFile multipartFile);
}