package com.example.exercise.service.ex;

public class EquipmentSizeException extends ServiceException{
        public EquipmentSizeException() {
                super();
        }

        public EquipmentSizeException(String message) {
                super(message);
        }

        public EquipmentSizeException(String message, Throwable cause) {
                super(message, cause);
        }

        public EquipmentSizeException(Throwable cause) {
                super(cause);
        }

        protected EquipmentSizeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
                super(message, cause, enableSuppression, writableStackTrace);
        }
}
