package com.example.exercise.service.ex;

public class EquipmentInsertException extends ServiceException{
    public EquipmentInsertException() {
        super();
    }

    public EquipmentInsertException(String message) {
        super(message);
    }

    public EquipmentInsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public EquipmentInsertException(Throwable cause) {
        super(cause);
    }

    protected EquipmentInsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
