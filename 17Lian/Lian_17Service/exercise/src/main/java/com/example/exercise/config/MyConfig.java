package com.example.exercise.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// 在Spring Boot中，Java Config的使用也已完全替代了applicationContext.xml。
// 实现了xml的零配置。在实现JavaConfig配置的时候就需要使用@Configuration和@Bean注解。
@Configuration
public class MyConfig implements WebMvcConfigurer {
    // 	addCorsMappings处理跨域请求。
    // 设置允许跨域访问的路径和域名以及允许发方法和header等，这里是设置的允许所有。
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        WebMvcConfigurer.super.addCorsMappings(registry);
        registry.addMapping("/**")
                .allowedHeaders("*")
                .exposedHeaders("*")
                .allowedMethods("*")
                .allowCredentials(true);
    }

    // 配置分页插件
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
