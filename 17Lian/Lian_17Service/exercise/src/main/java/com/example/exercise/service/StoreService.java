package com.example.exercise.service;

import com.example.exercise.entity.Store;

import java.util.List;

public interface StoreService {
    List<Store> findAllStore();
}
