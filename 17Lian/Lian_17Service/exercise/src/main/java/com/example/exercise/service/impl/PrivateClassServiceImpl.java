package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.exercise.entity.PrivateAdmin;
import com.example.exercise.entity.PrivateClass;
import com.example.exercise.mapper.PrivateAdminMapper;
import com.example.exercise.mapper.PrivateClassMapper;
import com.example.exercise.service.PrivateClassService;
import com.example.exercise.service.ex.PrivateAdminNotFoundException;
import com.example.exercise.service.ex.PrivateClassDeletedException;
import com.example.exercise.service.ex.PrivateClassException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrivateClassServiceImpl implements PrivateClassService {
    @Autowired(required = false)
    private PrivateClassMapper privateClassMapper;
    @Autowired(required = false)
    private PrivateAdminMapper privateAdminMapper;

    @Override
    public List<PrivateClass> findAllPrivateClass() {
        List<PrivateClass> data = privateClassMapper.findAllPrivateClass();
        return data;
    }

    @Override
    public void insertPrivateClass(String teacherName, PrivateClass privateClass) {
        QueryWrapper<PrivateAdmin> query = new QueryWrapper<PrivateAdmin>();
        query.eq("private_name", teacherName);
        PrivateAdmin privateAdmin = privateAdminMapper.selectOne(query);
        if (privateAdmin == null) {
            throw new PrivateAdminNotFoundException("私教未注册");
        }
        privateClass.setTeacherId(privateAdmin.getId());
        int rows = privateClassMapper.insert(privateClass);
        if (rows != 1) {
            throw new PrivateClassException("私人教课插入时异常");
        }
    }

    @Override
    public void deletePrivateClass(String id) {
        int rows = privateClassMapper.deleteById(id);
        if (rows != 1) {
            throw new PrivateClassDeletedException("私教课删除时异常");
        }
    }
}
