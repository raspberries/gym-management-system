package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.Store;

public interface StoreMapper extends BaseMapper<Store> {
}
