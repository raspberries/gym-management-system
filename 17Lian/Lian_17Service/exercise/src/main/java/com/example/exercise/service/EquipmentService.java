package com.example.exercise.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.Equipment;

public interface EquipmentService {
    Page<Equipment> findAllEquipments(String current, String size);

    void deleteEquipment(String id);

    void recoveryEquipment(String id);

    void modifyEquipment(String id, Equipment newEquipment);

    Equipment findOneEquipment(String id);

    void insertEquipment(Equipment equipment);
}
