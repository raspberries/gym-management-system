package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.Reserve;

import java.util.List;

public interface ReserveMapper extends BaseMapper<Reserve> {
    List<Reserve> findReserveEquipment(String userId);

    List<Reserve> findReservePrivateAdmin(String userId);

    List<Reserve> findReservePublicClass(String userId);
}
