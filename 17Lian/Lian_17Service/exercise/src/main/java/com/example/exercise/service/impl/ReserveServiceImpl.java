package com.example.exercise.service.impl;

import com.example.exercise.entity.Equipment;
import com.example.exercise.entity.Reserve;
import com.example.exercise.mapper.EquipmentMapper;
import com.example.exercise.mapper.ReserveMapper;
import com.example.exercise.service.ReserveService;
import com.example.exercise.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReserveServiceImpl implements ReserveService {
    @Autowired(required = false)
    private ReserveMapper reserveMapper;
    @Autowired(required = false)
    private EquipmentMapper equipmentMapper;

    @Override
    public void InsertEquipmentReserve(String equipmentId, String userId) {
        Reserve reserve = new Reserve();
        reserve.setEquipmentId(equipmentId);
        reserve.setUserId(userId);
        int rows = reserveMapper.insert(reserve);
        if (rows != 1) {
            throw new ReserveInsertException("预约插入时异常");
        }
        Equipment equipment = equipmentMapper.selectById(equipmentId);
        String num = equipment.getNumber();
        Integer a = Integer.parseInt(num) - 1;
        if (a < 0) {
            throw new EquipmentSizeException("器材数量长度异常");
        }
        num = String.valueOf(a);
        equipment.setNumber(num);
        equipmentMapper.updateById(equipment);
    }

    @Override
    public void updateEquipmentReserve(String id) {
        Reserve reserve = reserveMapper.selectById(id);
        if (reserve == null) {
            throw new ReserveNotFoundException("预约订单不存在");
        }
        reserve.setEquipmentCancel(1);
        int rows = reserveMapper.updateById(reserve);
        if (rows != 1) {
            throw new ReserveUpdateException("预约更新时异常");
        }
        String equipmentId = reserve.getEquipmentId();
        Equipment equipment = equipmentMapper.selectById(equipmentId);
        String num = equipment.getNumber();
        num = String.valueOf(Integer.parseInt(num) + 1);
        equipment.setNumber(num);
        equipmentMapper.updateById(equipment);

    }

    @Override
    public void insertPrivateAdminReserve(String PrivateAdminId, String userId) {
        Reserve reserve = new Reserve();
        reserve.setPrivateAdminId(PrivateAdminId);
        reserve.setUserId(userId);
        int rows = reserveMapper.insert(reserve);
        if (rows != 1) {
            throw new ReserveInsertException("预约插入时异常");
        }
    }

    @Override
    public void updatePrivateReserve(String id) {
        Reserve reserve = reserveMapper.selectById(id);
        if (reserve == null) {
            throw new ReserveNotFoundException("预约订单不存在");
        }
        reserve.setPrivateAdminCancel(1);
        int rows = reserveMapper.updateById(reserve);
        if (rows != 1) {
            throw new ReserveUpdateException("预约更新时异常");
        }
    }

    @Override
    public void insertPublicClassReserve(String PublicClassId, String userId) {
        Reserve reserve = new Reserve();
        reserve.setPublicClassId(PublicClassId);
        reserve.setUserId(userId);
        int rows = reserveMapper.insert(reserve);
        if (rows != 1) {
            throw new ReserveInsertException("预约插入时异常");
        }
    }

    @Override
    public void updatePublicReserve(String id) {
        Reserve reserve = reserveMapper.selectById(id);
        if (reserve == null) {
            throw new ReserveNotFoundException("预约订单不存在");
        }
        reserve.setPublicClassCancel(1);
        int rows = reserveMapper.updateById(reserve);
        if (rows != 1) {
            throw new ReserveUpdateException("预约更新时异常");
        }
    }

    @Override
    public List<Reserve> findReserveEquipment(String userId) {
        List<Reserve> data = reserveMapper.findReserveEquipment(userId);
        if (data == null) {
            throw new ReserveFindException("预约查询失败");
        }
        return data;
    }

    @Override
    public List<Reserve> findReservePrivateAdmin(String userId) {
        List<Reserve> data = reserveMapper.findReservePrivateAdmin(userId);
        if (data == null) {
            throw new ReserveFindException("预约查询失败");
        }
        return data;
    }

    @Override
    public List<Reserve> findReservePublicClass(String userId) {
        List<Reserve> data = reserveMapper.findReservePublicClass(userId);
        if (data == null) {
            throw new ReserveFindException("预约查询失败");
        }
        return data;
    }

}
