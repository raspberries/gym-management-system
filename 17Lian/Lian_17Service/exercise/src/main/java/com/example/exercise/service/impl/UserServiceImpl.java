package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.exercise.entity.User;
import com.example.exercise.mapper.UserMapper;
import com.example.exercise.service.UserService;
import com.example.exercise.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired(required = false)
    private UserMapper userMapper;

    @Override
    public User login(String account, String password) {
        QueryWrapper query = new QueryWrapper();
        User isExist = findByAccount(account);
        if (isExist == null) {
            throw new UserNotFoundException("账号未注册");
        }
        if (!isExist.getPassword().equals(password)) {
            throw new UserPasswordException("密码错误");
        }
        if (isExist.getDeleted() == 1) {
            throw new UserIsDeletedException("该用户已注销");
        }
        Map<String, String> map = new HashMap<>();
        map.put("account", account);
        map.put("password", password);
        query.allEq(map);
        User user = userMapper.selectOne(query);
        if (user == null) {
            throw new UserNotFoundException("账号未注册");
        }
        return user;
    }

    @Override
    public void register(User user) {
        String account = user.getAccount();
        User result = findByAccount(account);
        if (result != null) {
            throw new UserExistException("用户已注册");
        }
        //补全日志
        user.setCreatedUser(user.getUsername());
        user.setCreatedTime(new Date());
        user.setModifiedUser(user.getUsername());
        user.setModifiedTime(new Date());
        int row = userMapper.insert(user);
        if (row != 1) {
            throw new UserInsertException("用户在注册时产生异常");
        }
    }

    @Override
    public User findByAccount(String account) {
        QueryWrapper query = new QueryWrapper();
        query.eq("account", account);
        User user = userMapper.selectOne(query);
        return user;
    }

    @Override
    public User modify(User newUser) {
        User user = userMapper.selectById(newUser.getId());
        if (user == null) {
            throw new UserNotFoundException("用户未注册");
        }
        user.setUsername(newUser.getUsername());
        user.setAccount(newUser.getAccount());
        user.setPassword(newUser.getPassword());
        user.setUserHeadPortrait(newUser.getUserHeadPortrait());
        //填写日志
        user.setMoney(newUser.getMoney());
        user.setModifiedUser(newUser.getUsername());
        user.setModifiedTime(new Date());
        int row = userMapper.updateById(user);
        if (row != 1) {
            throw new UserUpdateException("用户更新资料异常");
        }
        User data = userMapper.selectById(newUser.getId());
        return data;
    }
}
