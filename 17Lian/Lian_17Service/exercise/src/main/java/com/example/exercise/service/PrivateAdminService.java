package com.example.exercise.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.PrivateAdmin;

public interface PrivateAdminService {
    Page<PrivateAdmin> findAllPrivateAdmins(String current, String size);

    void deletePrivateAdmin(String id);

    void recoveryPrivateAdmin(String id);

    void modifyPrivateAdmin(String id, PrivateAdmin newPrivateAdmin);

    PrivateAdmin findOnePrivateAdmin(String id);

    PrivateAdmin findOneAdminByName(String name);

    void insertPrivateAdmin(PrivateAdmin privateAdmin);
}
