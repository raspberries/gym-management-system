package com.example.exercise.util;

import lombok.Data;

import java.io.Serializable;

@Data
//Json格式的数据进行相应
//序列化 (Serialization)是将对象的状态信息转换为可以存储或传输的形式的过程。在序列化期间，对象将其当前状态写入到临时或持久性存储区。以后，可以通
//过从存储区中读取或反序列化对象的状态，重新创建该对象。
//对象的序列化处理非常简单，只需对象实现了Serializable 接口即可（该接口仅是一个标记，没有方法）
//序列化的对象包括基本数据类型，所有集合类以及其他许多东西，还有Class 对象
//对象序列化不仅保存了对象的“全景图”，而且能追踪对象内包含的所有句柄并保存那些对象；接着又能对每个对象内包含的句柄进行追踪
//使用transient关键字修饰的的变量，在序列化对象的过程中，该属性不会被序列化。
public class JsonResult<E> implements Serializable {
    //响应的状态码
    private Integer state;
    //描述信息
    private String message;
    //数据
    private E data;

    public JsonResult(Integer state) {
        this.state = state;
    }

    public JsonResult(Throwable e) {
        this.message = e.getMessage();
    }

    public JsonResult(Integer state, E data) {
        this.state = state;
        this.data = data;
    }

    public JsonResult() {
    }
}
