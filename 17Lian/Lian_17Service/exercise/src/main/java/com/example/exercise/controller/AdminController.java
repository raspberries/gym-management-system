package com.example.exercise.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.*;
import com.example.exercise.mapper.IncomeMapper;
import com.example.exercise.service.*;
import com.example.exercise.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// @RestController在实际开发中应用广泛，与@controller相比，不依赖于模板，前后端分离式的开发，开发效率也很高。
// @RequestMapping类定义处：规定初步的请求映射，相对于web应用的根目录；
@RestController
@RequestMapping("/admin")
public class AdminController extends BaseController {
    //将注解@Autowired添加到成员变量上，即表示这个成员变量会由Spring容器注入对应的Bean对象
    @Autowired
    private AdminService adminService;
    @Autowired
    private PrivateClassService privateClassService;
    @Autowired
    private PublicClassService publicClassService;
    @Autowired
    private EquipmentService equipmentService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private PrivateAdminService privateAdminService;
    @Autowired(required = false)
    private IncomeMapper incomeMapper;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ReserveService reserveService;

    // @RequestMapping方法定义处：进一步细分请求映射，相对于类定义处的URL。
    @RequestMapping("login")
    public JsonResult<Admin> login(String account, String password) {
        Admin data = adminService.login(account, password);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("findAllUsers")
    public JsonResult<Page<User>> lookAllUsers(@RequestParam(required = false, defaultValue = "1") String current, @RequestParam(required = false, defaultValue = "2000") String size) {
        //传递参数的数据
        Page<User> data = adminService.findAllUsers(current, size);
        return new JsonResult<>(OK, data);
        //状态+信息
    }

    @RequestMapping("deleteUser")
    public JsonResult<Void> delete(String id) {
        adminService.deleteUser(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("recoveryUser")
    public JsonResult<Void> recoveryUser(String id) {
        adminService.recoveryUser(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("modifyUser")
    public JsonResult<User> modifyUser(String id, User newUser) {
        User data = adminService.modifyUser(id, newUser);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("findOneUser")
    public JsonResult<User> findOneUser(String id) {
        User data = adminService.findOneUser(id);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("findAllEquipments")
    public JsonResult<Page<Equipment>> findAllEquipments(@RequestParam(required = false, defaultValue = "1") String current, @RequestParam(required = false, defaultValue = "2000") String size) {
        Page<Equipment> data = equipmentService.findAllEquipments(current, size);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("deleteEquipment")
    public JsonResult<Void> deleteEquipment(String id) {
        equipmentService.deleteEquipment(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("recoveryEquipment")
    public JsonResult<Void> recoveryEquipment(String id) {
        equipmentService.recoveryEquipment(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("modifyEquipment")
    public JsonResult<Void> modifyEquipment(String id, Equipment newEquipment) {
        equipmentService.modifyEquipment(id, newEquipment);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findOneEquipment")
    public JsonResult<Equipment> findOneEquipment(String id) {
        Equipment oneEquipment = equipmentService.findOneEquipment(id);
        return new JsonResult<>(OK, oneEquipment);
    }

    @RequestMapping("findAllOrders")
    public JsonResult<List<UserOrder>> findAllOrder() {
        List<UserOrder> data = orderService.findAllOrder();
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("modifyOrder")
    public JsonResult<Void> modifyOrder(UserOrder userOrder) {
        orderService.modifyOrder(userOrder);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findAllMembers")
    public JsonResult<List<Member>> findAllMembers() {
        List<Member> data = memberService.findAllMembers();
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("modifyMember")
    public JsonResult<Void> modifyMember(String userId, String title, String price) {
        memberService.modifyMember(userId, title, price);
        return new JsonResult<>(OK);
    }


    @RequestMapping("deletePrivateAdmin")
    public JsonResult<Void> deletePrivateAdmin(String id) {
        privateAdminService.deletePrivateAdmin(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("recoveryPrivateAdmin")
    public JsonResult<Void> recoveryPrivateAdmin(String id) {
        privateAdminService.recoveryPrivateAdmin(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("modifyPrivateAdmin")
    public JsonResult<Void> modifyPrivateAdmin(String id, PrivateAdmin newUser) {
        privateAdminService.modifyPrivateAdmin(id, newUser);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findOnePrivateAdmin")
    public JsonResult<PrivateAdmin> findOnePrivateAdmin(String id) {
        PrivateAdmin data = privateAdminService.findOnePrivateAdmin(id);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("Income")
    public JsonResult<Map<String, Integer>> Income() {
        Integer priceCount = incomeMapper.getPriceCount();
        Integer userCount = incomeMapper.getUserCount();
        Map<String, Integer> map = new HashMap<>();
        map.put("income", priceCount);
        map.put("people", userCount);
        return new JsonResult<>(OK, map);
    }

    @RequestMapping("insertEquipment")
    public JsonResult<Void> insertEquipment(Equipment equipment) {
        equipmentService.insertEquipment(equipment);
        return new JsonResult<>(OK);
    }

    @RequestMapping("insertPrivateAdmin")
    public JsonResult<Void> insertPrivateAdmin(PrivateAdmin privateAdmin) {
        privateAdminService.insertPrivateAdmin(privateAdmin);
        return new JsonResult<>(OK);
    }

    @RequestMapping("insertPrivateClass")
    public JsonResult<Void> insertPrivateClass(String teacherName, PrivateClass privateClass) {
        privateClassService.insertPrivateClass(teacherName, privateClass);
        return new JsonResult<>(OK);
    }

    @RequestMapping("insertPublicClass")
    public JsonResult<Void> insertPrivateClass(String teacherName, PublicClass publicClass) {
        publicClassService.insertPublicClass(teacherName, publicClass);
        return new JsonResult<>(OK);
    }

    @RequestMapping("deletePublicClass")
    public JsonResult<Void> deletePublicClass(String id) {
        publicClassService.deletePublicClass(id);
        return new JsonResult<>(OK);
    }

    @RequestMapping("deletePrivateClass")
    public JsonResult<Void> deletePrivateClass(String id) {
        privateClassService.deletePrivateClass(id);
        return new JsonResult<>(OK);
    }

    /**
     * 查询门店
     *
     * @return
     */
    @RequestMapping("findAllStore")
    public JsonResult<List<Store>> findAllStore() {
        List<Store> data = storeService.findAllStore();
        return new JsonResult<>(OK, data);
    }

    /**
     * 私教课程列表
     *
     * @return
     */
    @RequestMapping("findAllPrivateClass")
    public JsonResult<List<PrivateClass>> findAllPrivateClass() {
        List<PrivateClass> data = privateClassService.findAllPrivateClass();
        return new JsonResult<>(OK, data);
    }

    /**
     * 公共课
     *
     * @return
     */
    @RequestMapping("findAllPublicClass")
    public JsonResult<List<PublicClass>> findAllPublicClass() {
        List<PublicClass> data = publicClassService.findAllPublicClass();
        return new JsonResult<>(OK, data);
    }

    /**
     * 查询全部的私人教练
     *
     * @param current
     * @param size
     * @return
     */
    @RequestMapping("findAllPrivateAdmins")
    public JsonResult<Page<PrivateAdmin>> findAllPrivateAdmins
    (@RequestParam(required = false, defaultValue = "1") String current,
     @RequestParam(required = false, defaultValue = "2000") String size) {
        Page<PrivateAdmin> data = privateAdminService.findAllPrivateAdmins(current, size);
        return new JsonResult<>(OK, data);
    }

    /**
     * 私人教练
     *
     * @param name
     * @return
     */
    @RequestMapping("findOneAdminByName")
    public JsonResult<PrivateAdmin> findOneAdminByName(String name) {
        PrivateAdmin data = privateAdminService.findOneAdminByName(name);
        return new JsonResult<>(OK, data);
    }

    /**
     * 预约器材
     *
     * @param equipmentId
     * @param userId
     * @return
     */
    @RequestMapping("InsertEquipmentReserve")
    public JsonResult<Void> InsertEquipmentReserve(String equipmentId, String userId) {
        reserveService.InsertEquipmentReserve(equipmentId, userId);
        return new JsonResult<>(OK);
    }

    /**
     * 取消预约器材
     *
     * @param id 预约id
     * @return
     */
    @RequestMapping("updateEquipmentReserve")
    public JsonResult<Void> updateEquipmentReserve(String id) {
        reserveService.updateEquipmentReserve(id);
        return new JsonResult<>(OK);
    }

    /**
     * 私人教练预约
     *
     * @param PrivateAdminId 私人教练的id
     * @param userId
     * @return
     */
    @RequestMapping("insertPrivateAdminReserve")
    public JsonResult<Void> insertPrivateAdminReserve(String PrivateAdminId, String userId) {
        reserveService.insertPrivateAdminReserve(PrivateAdminId, userId);
        return new JsonResult<>(OK);
    }

    /**
     * 取消预约私人教练
     *
     * @param id 预约id
     * @return
     */
    @RequestMapping("updatePrivateReserve")
    public JsonResult<Void> updatePrivateReserve(String id) {
        reserveService.updatePrivateReserve(id);
        return new JsonResult<>(OK);
    }

    /**
     * 团体课预约
     *
     * @param publicClassId 团体课的id
     * @param userId
     * @return
     */
    @RequestMapping("insertPublicClassReserve")
    public JsonResult<Void> insertPublicClassReserve(String publicClassId, String userId) {
        reserveService.insertPublicClassReserve(publicClassId, userId);
        return new JsonResult<>(OK);
    }

    /**
     * 取消预约团体课
     *
     * @param id 预约id
     * @return
     */
    @RequestMapping("updatePublicReserve")
    public JsonResult<Void> updatePublicReserve(String id) {
        reserveService.updatePublicReserve(id);
        return new JsonResult<>(OK);
    }

    /**
     * 查看预约器材
     *
     * @param userId
     * @return
     */
    @RequestMapping("findReserveEquipment")
    public JsonResult<List<Reserve>> findReserveEquipment(String userId) {
        List<Reserve> data = reserveService.findReserveEquipment(userId);
        return new JsonResult<>(OK, data);
    }

    /**
     * 查看预约私人教练
     *
     * @param userId
     * @return
     */
    @RequestMapping("findReservePrivateAdmin")
    public JsonResult<List<Reserve>> findReservePrivateAdmin(String userId) {
        List<Reserve> data = reserveService.findReservePrivateAdmin(userId);
        return new JsonResult<>(OK, data);
    }

    /**
     * 查看预约团体课
     *
     * @param userId
     * @return
     */
    @RequestMapping("findReservePublicClass")
    public JsonResult<List<Reserve>> findReservePublicClass(String userId) {
        List<Reserve> data = reserveService.findReservePublicClass(userId);
        return new JsonResult<>(OK, data);
    }

    /**
     * 购买订单
     *
     * @param userId
     * @param title
     * @param price
     * @return
     */
    @RequestMapping("buyOfOrder")
    public JsonResult<Void> buyOfOrder(String userId, String title, float price) {
        orderService.buyOfOrder(userId, title, price);
        return new JsonResult<>(OK);
    }
}
