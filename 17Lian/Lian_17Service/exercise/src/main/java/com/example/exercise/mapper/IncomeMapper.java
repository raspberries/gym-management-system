package com.example.exercise.mapper;

public interface IncomeMapper {
    Integer getPriceCount();

    Integer getUserCount();
}
