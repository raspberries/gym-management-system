package com.example.exercise.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.Admin;
import com.example.exercise.entity.User;

public interface AdminService {
    Admin login(String account, String password);

    Page<User> findAllUsers(String current, String size);

    void deleteUser(String id);

    void recoveryUser(String id);

    User modifyUser(String id, User newUser);

    User findOneUser(String account);

}
