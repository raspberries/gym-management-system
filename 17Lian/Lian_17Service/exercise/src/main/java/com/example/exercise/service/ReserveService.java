package com.example.exercise.service;

import com.example.exercise.entity.Reserve;

import java.util.List;

public interface ReserveService {
    void InsertEquipmentReserve(String equipmentId, String userId);

    void updateEquipmentReserve(String id);

    void insertPrivateAdminReserve(String PrivateAdminId, String userId);

    void updatePrivateReserve(String id);

    void insertPublicClassReserve(String PublicClassId, String userId);

    void updatePublicReserve(String id);

    List<Reserve> findReserveEquipment(String userId);

    List<Reserve> findReservePrivateAdmin(String userId);

    List<Reserve> findReservePublicClass(String userId);

}
