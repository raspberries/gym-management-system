package com.example.exercise.service.impl;


import com.example.exercise.entity.ImageData;
import com.example.exercise.service.PictureService;
import com.example.exercise.service.ex.PhotoException;
import com.example.exercise.service.ex.PhotoFormatException;
import com.example.exercise.service.ex.PhotoNameNotFound;
import com.example.exercise.service.ex.PhotoSizeException;
import com.example.exercise.util.Constants;
import com.example.exercise.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

@Service
@Slf4j
public class PictureServiceImpl implements PictureService {

    @Override
    public byte[] getImage(String fileName) {
        String filePath = Constants.IMAGE_PATH + fileName;
        return getImageFile(filePath);
    }

    @Override
    public Object uploadImage(MultipartFile multipartFile) {
        //获取原始文件名
        String name = multipartFile.getOriginalFilename();
        if (name == null) {
            return new PhotoNameNotFound("获取图片失败");
        } else {
            if (!name.endsWith(".jpg") && !name.endsWith(".jpeg") && !name.endsWith(".png")) {
                return new PhotoFormatException("图片格式有误,只能为(jpg、jpeg、png)");
            }
        }
        BufferedOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = multipartFile.getInputStream();
            //图片不可大于2M
            if (inputStream.available() > 1024 * 1024 * 2) {
                return new PhotoSizeException("图片获取失败,原因:图片太大(不能大于10M)");
            }
            String filename = UUID.randomUUID().toString().replace("-", "") + name;
            outputStream = new BufferedOutputStream(new FileOutputStream(Constants.IMAGE_PATH + filename));
            byte[] b = new byte[1024 * 8];
            int len;
            while ((len = inputStream.read(b)) != -1) {
                outputStream.write(b, 0, len);
            }
            log.info("上传文件名-->" + name);
            return new JsonResult<ImageData>(200,
                    new ImageData(filename, Constants.BASE_URL + "/picture/getImage/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            throw new PhotoException("图片未知异常");
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Object uploadStaticImage(MultipartFile multipartFile) {
        //获取原始文件名
        String name = multipartFile.getOriginalFilename();
        if (name == null) {
            return new PhotoNameNotFound("获取图片失败");
        } else {
            if (!name.endsWith(".jpg") && !name.endsWith(".jpeg") && !name.endsWith(".png")) {
                return new PhotoFormatException("图片格式有误,只能为(jpg、jpeg、png)");
            }
        }
        BufferedOutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            inputStream = multipartFile.getInputStream();
            //图片不可大于2M
            if (inputStream.available() > 1024 * 1024 * 2) {
                return new PhotoSizeException("图片获取失败,原因:图片太大(不能大于10M)");
            }
            String filename = UUID.randomUUID().toString().replace("-", "") + name;
            outputStream = new BufferedOutputStream(new FileOutputStream(Constants.STATIC_IMAGE_PATH + filename));
            byte[] b = new byte[1024 * 8];
            int len;
            while ((len = inputStream.read(b)) != -1) {
                outputStream.write(b, 0, len);
            }
            log.info("上传文件名-->" + name);
            return new JsonResult<ImageData>(200,
                    new ImageData(filename, Constants.BASE_URL + "/getStaticImage/" + filename));
        } catch (IOException e) {
            e.printStackTrace();
            throw new PhotoException("图片未知异常");
        } finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public byte[] getStaticImage(String fileName) {
        String filePath = Constants.STATIC_IMAGE_PATH + fileName;
        return getImageFile(filePath);
    }

    /**
     * 获取图片文件
     *
     * @param filePath 图片的路径
     * @return
     */
    private byte[] getImageFile(String filePath) {
        FileInputStream stream = null;
        log.info("请求了一张图片:图片地址-->" + filePath);
        try {
            File file = new File(filePath);
            if (!file.exists()) {
                throw new IOException();
            }
            stream = new FileInputStream(filePath);
            int available = stream.available();
            byte[] bytes = new byte[available];
            stream.read(bytes, 0, available);
            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
            log.info("图片不存在");
            return null;
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
