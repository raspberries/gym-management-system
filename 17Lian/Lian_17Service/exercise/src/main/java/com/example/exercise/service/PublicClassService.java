package com.example.exercise.service;

import com.example.exercise.entity.PublicClass;

import java.util.List;

public interface PublicClassService {
    List<PublicClass> findAllPublicClass();

    void insertPublicClass(String teacherName, PublicClass publicClass);

    void deletePublicClass(String id);
}
