package com.example.exercise.service.ex;

public class UserPasswordException extends ServiceException{
    public UserPasswordException() {
        super();
    }

    public UserPasswordException(String message) {
        super(message);
    }

    public UserPasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserPasswordException(Throwable cause) {
        super(cause);
    }

    protected UserPasswordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
