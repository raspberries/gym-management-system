package com.example.exercise.service.impl;

import com.example.exercise.entity.Store;
import com.example.exercise.mapper.StoreMapper;
import com.example.exercise.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired(required = false)
    private StoreMapper storeMapper;

    @Override
    public List<Store> findAllStore() {
        List<Store> data = storeMapper.selectList(null);
        return data;
    }
}
