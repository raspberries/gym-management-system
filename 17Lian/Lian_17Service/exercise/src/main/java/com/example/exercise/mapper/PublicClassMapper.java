package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.PublicClass;

import java.util.List;

public interface PublicClassMapper extends BaseMapper<PublicClass> {
    List<PublicClass> findAllPublicClass();


}
