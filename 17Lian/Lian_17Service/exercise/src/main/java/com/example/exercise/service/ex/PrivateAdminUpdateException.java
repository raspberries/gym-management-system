package com.example.exercise.service.ex;

public class PrivateAdminUpdateException extends ServiceException{
    public PrivateAdminUpdateException() {
        super();
    }

    public PrivateAdminUpdateException(String message) {
        super(message);
    }

    public PrivateAdminUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivateAdminUpdateException(Throwable cause) {
        super(cause);
    }

    protected PrivateAdminUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
