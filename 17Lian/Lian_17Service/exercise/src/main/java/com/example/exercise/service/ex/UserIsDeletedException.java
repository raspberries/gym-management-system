package com.example.exercise.service.ex;

public class UserIsDeletedException extends ServiceException{
    public UserIsDeletedException() {
        super();
    }

    public UserIsDeletedException(String message) {
        super(message);
    }

    public UserIsDeletedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserIsDeletedException(Throwable cause) {
        super(cause);
    }

    protected UserIsDeletedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
