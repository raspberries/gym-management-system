package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.Equipment;


public interface EquipmentMapper extends BaseMapper<Equipment> {
}
