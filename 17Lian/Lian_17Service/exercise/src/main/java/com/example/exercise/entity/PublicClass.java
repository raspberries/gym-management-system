package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicClass {
    private String id;
    private String name;
    private String photo;
    private String title;
    private String teacherId;
    private String time;
    private Integer deleted;
    List<PrivateClass> privateClassList;
}
