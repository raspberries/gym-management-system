package com.example.exercise.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.exercise.entity.PrivateAdmin;

public interface PrivateAdminMapper extends BaseMapper<PrivateAdmin> {
}
