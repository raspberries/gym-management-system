package com.example.exercise.controller;

import com.example.exercise.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("picture")
public class PictureController extends BaseController {
    @Autowired
    public PictureService pictureService;

    @RequestMapping(value = "/getImage/{fileName}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] getImageById(@PathVariable("fileName") String fileName) {
        return pictureService.getImage(fileName);
    }

    @RequestMapping(value = "/getStaticImage/{fileName}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] getStaticImageById(@PathVariable("fileName") String fileName) {
        return pictureService.getStaticImage(fileName);
    }

    @RequestMapping(value = "/uploadImage")
    public Object uploadImage(@RequestPart(name = "image") MultipartFile multipartFile) {
        return pictureService.uploadImage(multipartFile);
    }

    @RequestMapping(value = "/uploadStaticImage")
    public Object uploadStaticImage(@RequestPart(name = "image") MultipartFile multipartFile) {
        return pictureService.uploadStaticImage(multipartFile);
    }
}
