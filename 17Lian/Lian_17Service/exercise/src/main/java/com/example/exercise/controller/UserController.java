package com.example.exercise.controller;

import com.example.exercise.entity.User;
import com.example.exercise.service.UserService;
import com.example.exercise.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;

    @RequestMapping("register")
    public JsonResult<Void> register(User user) {
        userService.register(user);
        return new JsonResult<>(OK);
    }

    @RequestMapping("login")
    public JsonResult<User> login(String account, String password) {
        User data = userService.login(account, password);
        return new JsonResult<User>(OK, data);
    }

    @RequestMapping("modify")
    public JsonResult<User> modify(User user) {
        User data = userService.modify(user);
        return new JsonResult<>(OK,data);
    }
}
