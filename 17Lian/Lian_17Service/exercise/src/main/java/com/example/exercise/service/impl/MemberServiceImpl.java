package com.example.exercise.service.impl;

import com.example.exercise.entity.Member;
import com.example.exercise.mapper.MemberMapper;
import com.example.exercise.service.MemberService;
import com.example.exercise.service.ex.MemberUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired(required = false)
    private MemberMapper memberMapper;

    @Override
    public List<Member> findAllMembers() {
        List<Member> data = memberMapper.findAllMember();
        return data;
    }

    @Override
    public Integer modifyMember(String userId, String title, String price) {
        Integer rows = memberMapper.modifyMember(userId, title, price);
        if (rows != 1) {
            throw new MemberUpdateException("会员卡更新时异常");
        }
        return rows;
    }

}
