package com.example.exercise.service.ex;

public class PrivateClassException extends ServiceException{
    public PrivateClassException() {
        super();
    }

    public PrivateClassException(String message) {
        super(message);
    }

    public PrivateClassException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivateClassException(Throwable cause) {
        super(cause);
    }

    protected PrivateClassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
