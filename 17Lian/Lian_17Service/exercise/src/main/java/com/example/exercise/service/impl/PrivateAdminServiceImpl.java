package com.example.exercise.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.exercise.entity.PrivateAdmin;
import com.example.exercise.mapper.PrivateAdminMapper;
import com.example.exercise.service.PrivateAdminService;
import com.example.exercise.service.ex.PrivateAdminInsertException;
import com.example.exercise.service.ex.PrivateAdminNotFoundException;
import com.example.exercise.service.ex.PrivateAdminUpdateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrivateAdminServiceImpl implements PrivateAdminService {
    @Autowired(required = false)
    private PrivateAdminMapper privateAdminMapper;

    @Override
    public Page<PrivateAdmin> findAllPrivateAdmins(String current, String size) {
        Page<PrivateAdmin> page = new Page<>(Integer.parseInt(current), Integer.parseInt(size));
        QueryWrapper query = new QueryWrapper();
        query.notIn("deleted", 1);
        Page<PrivateAdmin> data = privateAdminMapper.selectPage(page, query);
        return data;
    }

    @Override
    public void deletePrivateAdmin(String id) {
        PrivateAdmin privateAdmin = privateAdminMapper.selectById(id);
        if (privateAdmin == null) {
            throw new PrivateAdminNotFoundException("私教不存在");
        }
        privateAdmin.setDeleted(1);
        int rows = privateAdminMapper.updateById(privateAdmin);
        if (rows != 1) {
            throw new PrivateAdminUpdateException("私教更新时产生异常");
        }
    }

    @Override
    public void recoveryPrivateAdmin(String id) {
        PrivateAdmin privateAdmin = privateAdminMapper.selectById(id);
        if (privateAdmin == null) {
            throw new PrivateAdminNotFoundException("私教不存在");
        }
        privateAdmin.setDeleted(0);
        int rows = privateAdminMapper.updateById(privateAdmin);
        if (rows != 1) {
            throw new PrivateAdminUpdateException("私教更新时产生异常");
        }
    }

    @Override
    public void modifyPrivateAdmin(String id, PrivateAdmin newPrivateAdmin) {
        PrivateAdmin privateAdmin = privateAdminMapper.selectById(id);
        if (privateAdmin == null) {
            throw new PrivateAdminNotFoundException("私教不存在");
        }
        privateAdmin.setPrivateName(newPrivateAdmin.getPrivateName());
        privateAdmin.setCumulativeClass(newPrivateAdmin.getCumulativeClass());
        privateAdmin.setCertificate(newPrivateAdmin.getCertificate());
        privateAdmin.setFavorableRate(newPrivateAdmin.getFavorableRate());
        privateAdmin.setEvaluate(newPrivateAdmin.getEvaluate());
        privateAdmin.setIntroduction(newPrivateAdmin.getIntroduction());
        int row = privateAdminMapper.updateById(privateAdmin);
        if (row != 1) {
            throw new PrivateAdminUpdateException("私教更新时异常");
        }
    }

    @Override
    public PrivateAdmin findOnePrivateAdmin(String id) {
        PrivateAdmin data = privateAdminMapper.selectById(id);
        if (data == null) {
            throw new PrivateAdminNotFoundException("私教不存在");
        }
        return data;
    }

    @Override
    public PrivateAdmin findOneAdminByName(String name) {
        QueryWrapper query = new QueryWrapper();
        query.eq("private_name", name);
        query.notIn("deleted", 1);
        PrivateAdmin data = privateAdminMapper.selectOne(query);
        if (data == null) {
            throw new PrivateAdminNotFoundException("私教未找到");
        }
        return data;
    }

    @Override
    public void insertPrivateAdmin(PrivateAdmin privateAdmin) {
        int rows = privateAdminMapper.insert(privateAdmin);
        if (rows != 1) {
            throw new PrivateAdminInsertException("私教插入时异常");
        }
    }
}
