package com.example.exercise.service;

import com.example.exercise.entity.UserOrder;

import java.util.List;

public interface OrderService {
    List<UserOrder> findAllOrder();

    Integer modifyOrder(UserOrder userOrder);

    UserOrder findOneById(String id);

    void buyOfOrder(String userId, String title, float price);
}
