package com.example.exercise.service.ex;

public class PrivateAdminInsertException extends ServiceException{
    public PrivateAdminInsertException() {
        super();
    }

    public PrivateAdminInsertException(String message) {
        super(message);
    }

    public PrivateAdminInsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivateAdminInsertException(Throwable cause) {
        super(cause);
    }

    protected PrivateAdminInsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
