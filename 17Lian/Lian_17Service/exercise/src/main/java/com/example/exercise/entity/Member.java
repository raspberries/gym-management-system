package com.example.exercise.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Member {
    private String id;
    private String orderId;
    private String modifiedTime;
    List<UserOrder> userOrderList;
    List<User> UserList;
}
