package com.ranxi.lian_17.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class ShiJiaoList {
    /**
     * state : 200
     * message : null
     * data : {"records":[{"id":"2","privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":"214","certificate":"3","favorableRate":"96.67","evaluate":"3","introduction":"经历：1。擅长：1。","deleted":0},{"id":"3","privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":"187","certificate":"4","favorableRate":"100.0","evaluate":"2","introduction":"经历：1。擅长：1。","deleted":0},{"id":"4","privateName":"森林教练","privateHeadPortrait":"4.png","cumulativeClass":"685","certificate":"0","favorableRate":"98.0","evaluate":"5","introduction":"经历：国家二级运动员 入行三年，年均授课1000+ 曾多次参加马拉松、越野赛获得名次。擅长：减重减脂、增肌塑形、耐力训练、户外训练、提升运动表现。","deleted":0}],"total":3,"size":500,"current":1,"orders":[],"hitCount":false,"searchCount":true,"pages":1}
     */

    private int state;
    private Object message;
    private DataDTO data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * records : [{"id":"2","privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":"214","certificate":"3","favorableRate":"96.67","evaluate":"3","introduction":"经历：1。擅长：1。","deleted":0},{"id":"3","privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":"187","certificate":"4","favorableRate":"100.0","evaluate":"2","introduction":"经历：1。擅长：1。","deleted":0},{"id":"4","privateName":"森林教练","privateHeadPortrait":"4.png","cumulativeClass":"685","certificate":"0","favorableRate":"98.0","evaluate":"5","introduction":"经历：国家二级运动员 入行三年，年均授课1000+ 曾多次参加马拉松、越野赛获得名次。擅长：减重减脂、增肌塑形、耐力训练、户外训练、提升运动表现。","deleted":0}]
         * total : 3
         * size : 500
         * current : 1
         * orders : []
         * hitCount : false
         * searchCount : true
         * pages : 1
         */

        private int total;
        private int size;
        private int current;
        private boolean hitCount;
        private boolean searchCount;
        private int pages;
        private List<RecordsDTO> records;
        private List<?> orders;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        public boolean isHitCount() {
            return hitCount;
        }

        public void setHitCount(boolean hitCount) {
            this.hitCount = hitCount;
        }

        public boolean isSearchCount() {
            return searchCount;
        }

        public void setSearchCount(boolean searchCount) {
            this.searchCount = searchCount;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public List<RecordsDTO> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsDTO> records) {
            this.records = records;
        }

        public List<?> getOrders() {
            return orders;
        }

        public void setOrders(List<?> orders) {
            this.orders = orders;
        }

        public static class RecordsDTO implements Parcelable {
            /**
             * id : 2
             * privateName : 严涛
             * privateHeadPortrait : 2.png
             * cumulativeClass : 214
             * certificate : 3
             * favorableRate : 96.67
             * evaluate : 3
             * introduction : 经历：1。擅长：1。
             * deleted : 0
             */

            private String id;
            private String privateName;
            private String privateHeadPortrait;
            private String cumulativeClass;
            private String certificate;
            private String favorableRate;
            private String evaluate;
            private String introduction;
            private int deleted;

            protected RecordsDTO(Parcel in) {
                id = in.readString();
                privateName = in.readString();
                privateHeadPortrait = in.readString();
                cumulativeClass = in.readString();
                certificate = in.readString();
                favorableRate = in.readString();
                evaluate = in.readString();
                introduction = in.readString();
                deleted = in.readInt();
            }

            public static final Creator<RecordsDTO> CREATOR = new Creator<RecordsDTO>() {
                @Override
                public RecordsDTO createFromParcel(Parcel in) {
                    return new RecordsDTO(in);
                }

                @Override
                public RecordsDTO[] newArray(int size) {
                    return new RecordsDTO[size];
                }
            };

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getPrivateName() {
                return privateName;
            }

            public void setPrivateName(String privateName) {
                this.privateName = privateName;
            }

            public String getPrivateHeadPortrait() {
                return privateHeadPortrait;
            }

            public void setPrivateHeadPortrait(String privateHeadPortrait) {
                this.privateHeadPortrait = privateHeadPortrait;
            }

            public String getCumulativeClass() {
                return cumulativeClass;
            }

            public void setCumulativeClass(String cumulativeClass) {
                this.cumulativeClass = cumulativeClass;
            }

            public String getCertificate() {
                return certificate;
            }

            public void setCertificate(String certificate) {
                this.certificate = certificate;
            }

            public String getFavorableRate() {
                return favorableRate;
            }

            public void setFavorableRate(String favorableRate) {
                this.favorableRate = favorableRate;
            }

            public String getEvaluate() {
                return evaluate;
            }

            public void setEvaluate(String evaluate) {
                this.evaluate = evaluate;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public int getDeleted() {
                return deleted;
            }

            public void setDeleted(int deleted) {
                this.deleted = deleted;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(id);
                dest.writeString(privateName);
                dest.writeString(privateHeadPortrait);
                dest.writeString(cumulativeClass);
                dest.writeString(certificate);
                dest.writeString(favorableRate);
                dest.writeString(evaluate);
                dest.writeString(introduction);
                dest.writeInt(deleted);
            }
        }
    }
}
