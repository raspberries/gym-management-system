package com.ranxi.lian_17.domain;

import java.util.List;

public class MyPrivateClass {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1511659803017056258","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":0,"publicClassCancel":null,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.jpg","cumulativeClass":"181","certificate":"4","favorableRate":"90.0","evaluate":"2","introduction":"经历：1、美食营养学教练。擅长：1.减脂塑形，增肌","deleted":null}],"publicClassList":null},{"id":"1511659966511026177","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":1,"publicClassCancel":null,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.jpg","cumulativeClass":"181","certificate":"4","favorableRate":"90.0","evaluate":"2","introduction":"经历：1、美食营养学教练。擅长：1.减脂塑形，增肌","deleted":null}],"publicClassList":null}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * id : 1511659803017056258
         * equipmentId : null
         * userId : null
         * equipmentCancel : null
         * privateAdminCancel : 0
         * publicClassCancel : null
         * privateAdminId : null
         * publicClassId : null
         * equipmentList : null
         * privateAdminList : [{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.jpg","cumulativeClass":"181","certificate":"4","favorableRate":"90.0","evaluate":"2","introduction":"经历：1、美食营养学教练。擅长：1.减脂塑形，增肌","deleted":null}]
         * publicClassList : null
         */

        private String id;
        private Object equipmentId;
        private Object userId;
        private Object equipmentCancel;
        private int privateAdminCancel;
        private Object publicClassCancel;
        private Object privateAdminId;
        private Object publicClassId;
        private Object equipmentList;
        private Object publicClassList;
        private List<PrivateAdminListDTO> privateAdminList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(Object equipmentId) {
            this.equipmentId = equipmentId;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public Object getEquipmentCancel() {
            return equipmentCancel;
        }

        public void setEquipmentCancel(Object equipmentCancel) {
            this.equipmentCancel = equipmentCancel;
        }

        public int getPrivateAdminCancel() {
            return privateAdminCancel;
        }

        public void setPrivateAdminCancel(int privateAdminCancel) {
            this.privateAdminCancel = privateAdminCancel;
        }

        public Object getPublicClassCancel() {
            return publicClassCancel;
        }

        public void setPublicClassCancel(Object publicClassCancel) {
            this.publicClassCancel = publicClassCancel;
        }

        public Object getPrivateAdminId() {
            return privateAdminId;
        }

        public void setPrivateAdminId(Object privateAdminId) {
            this.privateAdminId = privateAdminId;
        }

        public Object getPublicClassId() {
            return publicClassId;
        }

        public void setPublicClassId(Object publicClassId) {
            this.publicClassId = publicClassId;
        }

        public Object getEquipmentList() {
            return equipmentList;
        }

        public void setEquipmentList(Object equipmentList) {
            this.equipmentList = equipmentList;
        }

        public Object getPublicClassList() {
            return publicClassList;
        }

        public void setPublicClassList(Object publicClassList) {
            this.publicClassList = publicClassList;
        }

        public List<PrivateAdminListDTO> getPrivateAdminList() {
            return privateAdminList;
        }

        public void setPrivateAdminList(List<PrivateAdminListDTO> privateAdminList) {
            this.privateAdminList = privateAdminList;
        }

        public static class PrivateAdminListDTO {
            /**
             * id : null
             * privateName : 雷丽
             * privateHeadPortrait : 1.jpg
             * cumulativeClass : 181
             * certificate : 4
             * favorableRate : 90.0
             * evaluate : 2
             * introduction : 经历：1、美食营养学教练。擅长：1.减脂塑形，增肌
             * deleted : null
             */

            private Object id;
            private String privateName;
            private String privateHeadPortrait;
            private String cumulativeClass;
            private String certificate;
            private String favorableRate;
            private String evaluate;
            private String introduction;
            private Object deleted;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getPrivateName() {
                return privateName;
            }

            public void setPrivateName(String privateName) {
                this.privateName = privateName;
            }

            public String getPrivateHeadPortrait() {
                return privateHeadPortrait;
            }

            public void setPrivateHeadPortrait(String privateHeadPortrait) {
                this.privateHeadPortrait = privateHeadPortrait;
            }

            public String getCumulativeClass() {
                return cumulativeClass;
            }

            public void setCumulativeClass(String cumulativeClass) {
                this.cumulativeClass = cumulativeClass;
            }

            public String getCertificate() {
                return certificate;
            }

            public void setCertificate(String certificate) {
                this.certificate = certificate;
            }

            public String getFavorableRate() {
                return favorableRate;
            }

            public void setFavorableRate(String favorableRate) {
                this.favorableRate = favorableRate;
            }

            public String getEvaluate() {
                return evaluate;
            }

            public void setEvaluate(String evaluate) {
                this.evaluate = evaluate;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }
        }
    }
}
