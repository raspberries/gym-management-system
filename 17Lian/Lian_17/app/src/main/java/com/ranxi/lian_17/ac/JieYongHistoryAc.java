package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.Result;
import com.ranxi.lian_17.domain.WoDeQiCaiYuYue;

import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JieYongHistoryAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mList;
    private BaseRecyclerAdapter<WoDeQiCaiYuYue.DataDTO> mListAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jie_yong_history);
        setStateColor();
    }

    @Override
    protected void initView() {

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mList = findViewById(R.id.list);
        show(mBack, mTitle);
        mTitle.setText("借用历史");
        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<WoDeQiCaiYuYue.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.wode_yuyue_item;
            }

            @Override
            protected void initData(WoDeQiCaiYuYue.DataDTO data, int position, View view) {
                TextView mT1;
                TextView mSubmit;
                mT1 = view.findViewById(R.id.t1);
                mSubmit = view.findViewById(R.id.submit);
                mT1.setText(data.getEquipmentList().get(0).getName());
                mSubmit.setOnClickListener(v -> {
                    mData.remove(position);
                    notifyDataSetChanged();
                    mApi.unQiCai(data.getId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            Result body = response.body();
                            if (body.getState() == 200) {
                                Toast.makeText(JieYongHistoryAc.this, "取消成功", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(JieYongHistoryAc.this, body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable throwable) {

                        }
                    });
                });
            }
        };
        mList.setAdapter(mListAd);
    }

    @Override
    protected void initData() {
        mApi.getWoDeQicaiYuyue(App.user.getId()).enqueue(new Callback<WoDeQiCaiYuYue>() {
            @Override
            public void onResponse(Call<WoDeQiCaiYuYue> call, Response<WoDeQiCaiYuYue> response) {
                List<WoDeQiCaiYuYue.DataDTO> data = response.body().getData();
                ListIterator<WoDeQiCaiYuYue.DataDTO> iterator = data.listIterator();
                while (iterator.hasNext()) {
                    WoDeQiCaiYuYue.DataDTO next = iterator.next();
                    if (next.getEquipmentCancel()==1) {
                        iterator.remove();
                    }
                }
                mListAd.setData(data);
            }

            @Override
            public void onFailure(Call<WoDeQiCaiYuYue> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {

    }
}