package com.ranxi.lian_17.base;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.domain.Api;
import com.youth.banner.Banner;
import com.youth.banner.indicator.CircleIndicator;

public abstract class BaseAc extends AppCompatActivity {
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        getWindow().setStatusBarColor(Color.parseColor("#333333"));
        initView();
        initData();
        initListener();
    }

    public void setStateColor(){
        View view = getWindow().getDecorView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    protected abstract void initView();

    protected abstract void initData();

    protected abstract void initListener();

    public Context getContext() {
        return this;
    }


    public static Api mApi = App.mApi;
    public void setImage(ImageView image, String userHeadPortrait) {
        Glide.with(getContext()).load(App.imageUrl + userHeadPortrait)
                .apply(RequestOptions.circleCropTransform())
                .error(R.mipmap.login_tx)
                .into(image);
    }
    public void requestFocus(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    public Banner setBanner(Banner banner) {
        return banner.setIndicator(new CircleIndicator(getContext()))
                .setIndicatorNormalColorRes(R.color.colorGray)
                .setIndicatorSelectedColorRes(R.color.colorPrimary)
                .setIndicatorWidth(25, 25);
    }

    public void error() {
        Toast.makeText(this, "内容不能为空！", Toast.LENGTH_SHORT).show();
    }

    public void show(View... views) {
        for (View view : views) {
            view.setOnClickListener(v -> finish());
            view.setVisibility(View.VISIBLE);
        }
    }


    public void toActivity(Class<?> clazz) {
        Intent intent = new Intent(getContext(), clazz);
        startActivity(intent);
    }

    public void toService(String name) {
        Class<?> clazz = null;
        switch (name) {

        }
        if (clazz != null) {
            Intent intent = new Intent(getContext(), clazz);
            startActivity(intent);
        }

    }
}
