package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.UserLogin;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMoneyAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextInputEditText mT1;
    private TextView mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mT1 = findViewById(R.id.t1);
        mSubmit = findViewById(R.id.submit);
        show(mBack, mTitle);
        mTitle.setText("充值");
        mSubmit.setOnClickListener(v -> {
            String s = mT1.getText().toString();
            try {
                long i = Integer.parseInt(s);
                if (i > 0) {
                    Map<String, Object> data = new HashMap<>();
                    data.put("id", App.user.getId());
                    double v1 = App.user.getMoney() + i;
                    data.put("money", (int) v1);
                    mApi.modifyUser(data).enqueue(new Callback<UserLogin>() {
                        @Override
                        public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                            Toast.makeText(AddMoneyAc.this, "充值成功", Toast.LENGTH_SHORT).show();
                            App.money = (int) v1;
                            App.user.setMoney(v1);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<UserLogin> call, Throwable throwable) {

                        }
                    });
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this, "输入有误！", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}