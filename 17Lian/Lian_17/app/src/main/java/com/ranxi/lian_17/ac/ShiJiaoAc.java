package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.MyPrivateClass;
import com.ranxi.lian_17.domain.Result;

import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShiJiaoAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mList;
    private BaseRecyclerAdapter<MyPrivateClass.DataDTO> mListAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shi_jiao);
        setStateColor();
    }

    @Override
    protected void initView() {

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mList = findViewById(R.id.list);
        show(mBack, mTitle);
        mTitle.setText("我的私教");
        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<MyPrivateClass.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.shijiao_history_item;
            }

            @Override
            protected void initData(MyPrivateClass.DataDTO data, int position, View view) {
                ImageView mImage;
                TextView mT1;
                TextView mSubmit;
                TextView mT2;
                TextView mT3;

                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);

                mImage = view.findViewById(R.id.image);
                mT1 = view.findViewById(R.id.t1);
                mSubmit = view.findViewById(R.id.submit);
                MyPrivateClass.DataDTO.PrivateAdminListDTO list = data.getPrivateAdminList().get(0);
                Glide.with(getContext()).load(App.imageUrl + list.getPrivateHeadPortrait())
                        .error(R.mipmap.renwu)
                        .into(mImage);
                mT1.setText("教练名称：" + list.getPrivateName());
                mT2.setText("累计上课：" + list.getCumulativeClass());
                mT3.setText("好评率：" + list.getFavorableRate()+"%");
                mSubmit.setOnClickListener(v -> {
                    mData.remove(0);
                    notifyDataSetChanged();
                    mApi.unShijiao(data.getId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            Toast.makeText(ShiJiaoAc.this, "取消成功", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable throwable) {

                        }
                    });
                });
            }
        };
        mList.setAdapter(mListAd);
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void initData() {
        mApi.getMyPrivateClass(App.user.getId()).enqueue(new Callback<MyPrivateClass>() {
            @Override
            public void onResponse(Call<MyPrivateClass> call, Response<MyPrivateClass> response) {
                List<MyPrivateClass.DataDTO> data = response.body().getData();
                ListIterator<MyPrivateClass.DataDTO> iterator = data.listIterator();
                while (iterator.hasNext()) {
                    MyPrivateClass.DataDTO next = iterator.next();
                    if (next.getPrivateAdminCancel() == 1) {
                        iterator.remove();
                    }
                }
                mListAd.setData(data);
            }

            @Override
            public void onFailure(Call<MyPrivateClass> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {

    }
}