package com.ranxi.lian_17;

import android.app.Application;
import android.text.TextUtils;

import com.ranxi.lian_17.domain.Api;
import com.ranxi.lian_17.domain.UserLogin;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    public static String baseUrl = "http://10.0.2.2:8083";
    public static String imageUrl = "http://10.0.2.2:8083/picture/getImage/";
    public static Api mApi;
    public static UserLogin.DataDTO user;
    public static int money = 0;
    public static String price;

    public static boolean isEmpty(CharSequence... text) {
        if (text != null) {
            for (CharSequence charSequence : text) {
                if (TextUtils.isEmpty(charSequence))
                    return true;
            }
        } else return true;
        return false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        load();
    }

    private void load() {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseUrl + "/")
                .build();
        mApi = retrofit.create(Api.class);
    }
}
