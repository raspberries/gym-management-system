package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.UserLogin;
import com.ranxi.lian_17.domain.Vip;

import java.util.ArrayList;
import java.util.List;

public class VipAc extends BaseAc {
    private RecyclerView mVipCard;
    private BaseRecyclerAdapter<Vip> mVipAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vip);
        setStateColor();
    }

    private ImageView mBack;
    private TextView mTitle;

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        show(mBack, mTitle);
        mTitle.setText("续费VIP");

        mVipCard = findViewById(R.id.vipCard);
        mVipCard.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mVipAd = new BaseRecyclerAdapter<Vip>() {
            @Override
            protected int getRootView() {
                return R.layout.vip_item;
            }

            @Override
            protected void initData(Vip data, int position, View view) {
                TextView mT1;
                TextView mT2;
                TextView mT3;

                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);
                mT1.setText(data.getName());
                mT2.setText(data.getPrice());
                mT3.setText("续费");
                mT3.setOnClickListener(v -> {
                    UserLogin.DataDTO user = App.user;
                    if (user != null) {
                        Intent intent = new Intent(getContext(), ZhiFuAc.class);
                        intent.putExtra("userId", App.user.getId());
                        intent.putExtra("title", data.getName());
                        intent.putExtra("price", data.getPrice());
                        intent.putExtra("tag", 2);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), LoginAc.class);
                        startActivityForResult(intent, 1);
                    }
                });
            }
        };
        mVipCard.setAdapter(mVipAd);
        List<Vip> vipData = new ArrayList<>();
        vipData.add(new Vip("季卡", "489"));
        vipData.add(new Vip("月卡", "219"));
        vipData.add(new Vip("年卡", "1350.4"));
        mVipAd.setData(vipData);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}