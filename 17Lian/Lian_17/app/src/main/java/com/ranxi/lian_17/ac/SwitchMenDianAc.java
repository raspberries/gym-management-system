package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lljjcoder.Interface.OnCityItemClickListener;
import com.lljjcoder.bean.CityBean;
import com.lljjcoder.bean.DistrictBean;
import com.lljjcoder.bean.ProvinceBean;
import com.lljjcoder.citywheel.CityConfig;
import com.lljjcoder.style.citypickerview.CityPickerView;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.MenDian;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 切换门店
 */
public class SwitchMenDianAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextView mSelected;
    private SearchView mSearch;
    private RecyclerView mList;
    private BaseRecyclerAdapter<MenDian.DataDTO> mListAd;
    private List<MenDian.DataDTO> mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_men_dian);

        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        show(mBack, mTitle);
        mTitle.setText("选择门店");

        mSelected = findViewById(R.id.selected);
        mSearch = findViewById(R.id.search);
        mList = findViewById(R.id.list);

        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<MenDian.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.mendian_item;
            }

            @Override
            protected void initData(MenDian.DataDTO data, int position, View view) {
                TextView mDistance;
                TextView mLocation;
                TextView mName;

                mName = view.findViewById(R.id.name);

                mDistance = view.findViewById(R.id.distance);
                mLocation = view.findViewById(R.id.location);

                mDistance.setText(data.getLocation() + "km");
                mName.setText(data.getName());
                mLocation.setText(data.getTitle());
            }
        };
        mList.setAdapter(mListAd);
        mListAd.setOnItemListener((data, position, view) -> {
            Intent intent = new Intent();
            intent.putExtra("data", data);
            SwitchMenDianAc.this.setResult(200, intent);
            SwitchMenDianAc.this.finish();
        });
    }

    @Override
    protected void initData() {
        mApi.getMenDian().enqueue(new Callback<MenDian>() {
            @Override
            public void onResponse(Call<MenDian> call, Response<MenDian> response) {
                mData = response.body().getData();
                mListAd.setData(mData);
            }

            @Override
            public void onFailure(Call<MenDian> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestFocus(mTitle);
    }

    @Override
    protected void initListener() {
        mSelected.setOnClickListener(v -> {
            CityPickerView cityPickerView = new CityPickerView();
            cityPickerView.init(getContext());
            CityConfig build = new CityConfig.Builder()
                    .cityCyclic(false)
                    .districtCyclic(false)
                    .districtCyclic(false)
                    .build();
            cityPickerView.setConfig(build);
            cityPickerView.showCityPicker();
            cityPickerView.setOnCityItemClickListener(new OnCityItemClickListener() {
                @Override
                public void onSelected(ProvinceBean province, CityBean city, DistrictBean district) {
                    mSelected.setText(province.getName());
                    init(null);
                }

                @Override
                public void onCancel() {
                    super.onCancel();
                }
            });
        });
        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                init(query);
                mSearch.setQuery("", false);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        mSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return true;
            }
        });
    }

    private void init(String key) {
        List<MenDian.DataDTO> list = new ArrayList<>();
        if (mData != null && mData.size() > 0) {
            if (key != null) {
                for (MenDian.DataDTO datum : mData) {
                    if (datum.getName().contains(key)) {
                        list.add(datum);
                    }
                }
            } else {
                Collections.shuffle(mData);
                list.add(mData.get(0));
                list.add(mData.get(1));
            }
        }
        if (list.size() == 0) {
            Toast.makeText(this, "没有找到类似的门店！", Toast.LENGTH_SHORT).show();
        } else {
            mListAd.setData(list);
        }
    }
}