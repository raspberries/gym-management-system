package com.ranxi.lian_17.ac;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;

public class KaiMenAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private ImageView mImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kai_men);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mImage = findViewById(R.id.image);
        show(mBack, mTitle);
        mTitle.setText("开门码");
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap("content",
                    BarcodeFormat.QR_CODE, 600, 600);
            ImageView imageViewQrCode = (ImageView) findViewById(R.id.image);
            imageViewQrCode.setImageBitmap(bitmap);
        } catch (Exception e) {

        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}