package com.ranxi.lian_17.domain;

public class UserLogin {

    /**
     * state : 200
     * message : null
     * data : {"createdTime":"2022-04-07T02:43:36.000+00:00","createdUser":null,"modifiedTime":"2022-04-07T02:05:23.000+00:00","modifiedUser":"txin","id":"3e87a812e73205a4773f99fc0c76ebe3","username":"txin","account":"1234","password":"88888","userHeadPortrait":"ab13071c562a48099ed5d70aeb227851IMG_20220308_080054.jpg","money":12626,"deleted":0}
     */

    private int state;
    private Object message;
    private DataDTO data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * createdTime : 2022-04-07T02:43:36.000+00:00
         * createdUser : null
         * modifiedTime : 2022-04-07T02:05:23.000+00:00
         * modifiedUser : txin
         * id : 3e87a812e73205a4773f99fc0c76ebe3
         * username : txin
         * account : 1234
         * password : 88888
         * userHeadPortrait : ab13071c562a48099ed5d70aeb227851IMG_20220308_080054.jpg
         * money : 12626.0
         * deleted : 0
         */

        private String createdTime;
        private Object createdUser;
        private String modifiedTime;
        private String modifiedUser;
        private String id;
        private String username;
        private String account;
        private String password;
        private String userHeadPortrait;
        private double money;
        private int deleted;

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public Object getCreatedUser() {
            return createdUser;
        }

        public void setCreatedUser(Object createdUser) {
            this.createdUser = createdUser;
        }

        public String getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(String modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public String getModifiedUser() {
            return modifiedUser;
        }

        public void setModifiedUser(String modifiedUser) {
            this.modifiedUser = modifiedUser;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUserHeadPortrait() {
            return userHeadPortrait;
        }

        public void setUserHeadPortrait(String userHeadPortrait) {
            this.userHeadPortrait = userHeadPortrait;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public int getDeleted() {
            return deleted;
        }

        public void setDeleted(int deleted) {
            this.deleted = deleted;
        }
    }
}
