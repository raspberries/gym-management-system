package com.ranxi.lian_17.fr;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.ac.LoginAc;
import com.ranxi.lian_17.ac.SettingAc;
import com.ranxi.lian_17.base.BaseFr;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.MyOrder;
import com.ranxi.lian_17.domain.MyPrivateClass;
import com.ranxi.lian_17.domain.Service;
import com.ranxi.lian_17.domain.UserLogin;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalFr extends BaseFr {
    private static final String TAG = "PersonalFr";
    private BaseRecyclerAdapter<Service> mServiceAdapter;

    @Override
    protected int getRootView() {
        return R.layout.fr_personal;
    }

    private TextView mN1;
    private TextView mN2;
    private ImageView mImage;
    private TextView mLogin;
    private RecyclerView mFunction;
    private ImageView mSetting;
    private TextView mName;
    private TextView mN3;

    @Override
    protected void initView(View view) {
        mImage = view.findViewById(R.id.image);
        mLogin = view.findViewById(R.id.login);
        mFunction = view.findViewById(R.id.function);
        mSetting = view.findViewById(R.id.setting);
        mName = view.findViewById(R.id.name);

        mN3 = view.findViewById(R.id.n3);


        mN1 = view.findViewById(R.id.n1);
        mN2 = view.findViewById(R.id.n2);

        mFunction.setLayoutManager(new GridLayoutManager(getContext(), 4, RecyclerView.VERTICAL, false));
        mServiceAdapter = new BaseRecyclerAdapter<Service>() {
            @Override
            protected int getRootView() {
                return R.layout.service_item;
            }

            @Override
            protected void initData(Service data, int position, View view) {
                ImageView mImage;
                TextView mName;

                mImage = view.findViewById(R.id.image);
                mName = view.findViewById(R.id.name);
                mImage.setImageResource(data.getImage());
                mName.setText(data.getName());
            }
        };
        mFunction.setAdapter(mServiceAdapter);
    }

    @Override
    protected void initData() {
        List<Service> data = new ArrayList<>();
        data.add(new Service(R.mipmap.chongzhi, "充值"));
        data.add(new Service(R.mipmap.vip, "续费会员"));
        data.add(new Service(R.mipmap.sijiao, "我的团体课"));
        data.add(new Service(R.mipmap.tuantike, "我的私教"));
        data.add(new Service(R.mipmap.qicai, "我的器材"));
        data.add(new Service(R.mipmap.lishi, "借用历史"));
        mServiceAdapter.setData(data);
    }

    @Override
    protected void initListener() {
        mServiceAdapter.setOnItemListener((data, position, view) -> {
            UserLogin.DataDTO user = App.user;
            if (user != null) {
                toService(data.getName());
            } else {
                Toast.makeText(getContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), LoginAc.class);
                startActivityForResult(intent, 1);
            }
        });
        mLogin.setOnClickListener(v -> {
            UserLogin.DataDTO user = App.user;
            if (user != null) {
                return;
            }
            Intent intent = new Intent(getContext(), LoginAc.class);
            startActivityForResult(intent, 3);
        });
        mSetting.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), SettingAc.class);
            startActivityForResult(intent, 4);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3 && resultCode == 200) {
            mSetting.setVisibility(View.VISIBLE);
            mLogin.setVisibility(View.GONE);
            mName.setVisibility(View.VISIBLE);
            init();
        }

        if (requestCode == 4 && resultCode == 200) {
            init();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        init();
    }

    private void init() {
        UserLogin.DataDTO user = App.user;
        if (user != null) {
            Log.d(TAG, "init: " + user);
            mName.setText(user.getUsername());
            setImage(mImage, user.getUserHeadPortrait());
            mSetting.setVisibility(View.VISIBLE);
            mLogin.setVisibility(View.GONE);
            mName.setVisibility(View.VISIBLE);
            mApi.getMyPrivateClass(App.user.getId()).enqueue(new Callback<MyPrivateClass>() {
                @Override
                public void onResponse(Call<MyPrivateClass> call, Response<MyPrivateClass> response) {
                    List<MyPrivateClass.DataDTO> data = new ArrayList<>();
                    if (response.body() != null) {
                        data = response.body().getData();
                    }
                    ListIterator<MyPrivateClass.DataDTO> iterator = data.listIterator();
                    while (iterator.hasNext()) {
                        MyPrivateClass.DataDTO next = iterator.next();
                        if (next.getPrivateAdminCancel() == 1) {
                            iterator.remove();
                        }
                    }
                    if (data.size() > 0)
                        mN1.setText(data.size() + "");
                }

                @Override
                public void onFailure(Call<MyPrivateClass> call, Throwable throwable) {

                }
            });

            mApi.getOrder().enqueue(new Callback<MyOrder>() {
                @Override
                public void onResponse(Call<MyOrder> call, Response<MyOrder> response) {
                    List<MyOrder.DataDTO> data = response.body().getData();
                    int count = 0;
                    for (MyOrder.DataDTO datum : data) {
                        if (datum.getUserId().equals(user.getId())) {
                            String equals = datum.getTitle();
                            switch (equals) {
                                case "购买了一张季卡":
                                    count += 90;
                                    break;
                                case "购买了一张年卡":
                                    count += 365;
                                    break;
                                case "购买了一张月卡":
                                    count += 30;
                                    break;
                            }
                        }
                    }
                    mN2.setText(count + "");
                }

                @Override
                public void onFailure(Call<MyOrder> call, Throwable throwable) {

                }
            });
            mN3.setText(App.money + "");
        }
    }

}
