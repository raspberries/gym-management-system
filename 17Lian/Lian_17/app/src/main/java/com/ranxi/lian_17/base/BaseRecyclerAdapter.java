package com.ranxi.lian_17.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerAdapter<T> extends RecyclerView.Adapter<BaseRecyclerAdapter<T>.InnerHolder> {
    private static final String TAG = "BaseRecyclerAdapter";
    @NonNull
    @Override
    public BaseRecyclerAdapter<T>.InnerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InnerHolder(LayoutInflater.from(parent.getContext()).inflate(getRootView(), parent, false));
    }

    protected abstract int getRootView();

    public List<T> mData = new ArrayList<>();

    public void setData(List<T> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull BaseRecyclerAdapter<T>.InnerHolder holder, int position) {
        holder.setData(mData.get(position), position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() > 0)
            return mData.size();
        return 0;
    }

    public class InnerHolder extends RecyclerView.ViewHolder {
        public InnerHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void setData(T data, int position, View view) {
            if (mOnItemListener != null) {
                view.setOnClickListener(v -> {
                    onClick(data, position, view);
                    mOnItemListener.onClick(data, position, view);
                });
            }
            initData(data, position, view);
        }
    }

    protected abstract void initData(T data, int position, View view);

    public void onClick(T data, int position, View view) {

    }

    public interface OnItemListener<T> {

        void onClick(T data, int position, View view);
    }

    private OnItemListener<T> mOnItemListener;

    public void setOnItemListener(OnItemListener<T> onItemListener) {
        mOnItemListener = onItemListener;
    }
}
