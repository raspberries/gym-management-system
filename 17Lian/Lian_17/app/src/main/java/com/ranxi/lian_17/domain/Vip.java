package com.ranxi.lian_17.domain;

public class Vip {
    private String name;
    private String price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Vip(String name, String price) {
        this.name = name;
        this.price = price;
    }
}
