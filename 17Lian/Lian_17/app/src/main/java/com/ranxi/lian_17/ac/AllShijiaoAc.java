package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.ShiJiaoList;
import com.youth.banner.util.BannerUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllShijiaoAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mList;
    private BaseRecyclerAdapter<ShiJiaoList.DataDTO.RecordsDTO> mShijiaoListAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_shijiao);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mList = findViewById(R.id.list);
        show(mBack, mTitle);
        mTitle.setText("私人教练");

        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mShijiaoListAd = new BaseRecyclerAdapter<ShiJiaoList.DataDTO.RecordsDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.shijiao_all_item;
            }

            @Override
            protected void initData(ShiJiaoList.DataDTO.RecordsDTO data, int position, View view) {
                ImageView mImage;
                TextView mT1;
                TextView mT3;
                TextView mT4;

                mImage = view.findViewById(R.id.image);
                mT1 = view.findViewById(R.id.t1);
                mT3 = view.findViewById(R.id.t3);
                mT4 = view.findViewById(R.id.t4);
                Glide.with(getContext()).load(App.imageUrl + data.getPrivateHeadPortrait())
                        .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(15f))))
                        .error(R.mipmap.shijiao_tp)
                        .into(mImage);
                mT1.setText(data.getPrivateName());
                mT3.setText("好评率 " + data.getFavorableRate() + " | 累计上课" + data.getCumulativeClass() + "节");
                mT4.setText(data.getIntroduction());
            }
        };
        mList.setAdapter(mShijiaoListAd);
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void initData() {
        mApi.getShijiaoList().enqueue(new Callback<ShiJiaoList>() {
            @Override
            public void onResponse(Call<ShiJiaoList> call, Response<ShiJiaoList> response) {
                List<ShiJiaoList.DataDTO.RecordsDTO> records = response.body().getData().getRecords();
                mShijiaoListAd.setData(records);
            }

            @Override
            public void onFailure(Call<ShiJiaoList> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {
        mShijiaoListAd.setOnItemListener((data, position, view) -> {
            Intent intent = new Intent(getContext(), ShiJIaoDetailAc.class);
            intent.putExtra("data", data);
            startActivity(intent);
        });
    }
}