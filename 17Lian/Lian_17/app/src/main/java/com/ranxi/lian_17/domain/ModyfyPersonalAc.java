package com.ranxi.lian_17.domain;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.youth.banner.util.BannerUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModyfyPersonalAc extends BaseAc {
    private ImageView mImage;
    private TextInputEditText mT1;
    private TextView mSubmit;
    private Uri mUri;
    private String mName;
    private String mMimeType;
    private String mPath;
    private ImageView mBack;
    private TextView mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modyfy_personal);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        show(mBack, mTitle);
        mTitle.setText("个人信息");
        mImage = findViewById(R.id.image);
        mT1 = findViewById(R.id.t1);
        mSubmit = findViewById(R.id.submit);
    }

    @Override
    protected void initData() {
        UserLogin.DataDTO user = App.user;
        String userHeadPortrait = user.getUserHeadPortrait();
        Glide.with(getContext()).load(App.imageUrl + userHeadPortrait)
                .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(10f))))
                .error(R.mipmap.login_tx)
                .into(mImage);

        mT1.setText(user.getUsername());
        mSubmit.setOnClickListener(v -> {
            String s = mT1.getText().toString();
            if (!App.isEmpty(s)) {
                if (mUri != null) {
                    RequestBody body = RequestBody.create(MediaType.parse(mMimeType), new File(mPath));
                    MultipartBody.Part part = MultipartBody.Part.createFormData("image", mName, body);
                    mApi.uploadImage(part).enqueue(new Callback<ImageUoload>() {
                        @Override
                        public void onResponse(Call<ImageUoload> call, Response<ImageUoload> response) {
                            ImageUoload.DataDTO data = response.body().getData();
                            String fileName = data.getFileName();
                            Log.d(TAG, "onResponse: " + data.getFileUrl());
                            Map<String, Object> map = new HashMap<>();
                            map.put("username", s);
                            map.put("userHeadPortrait", fileName);
                            map.put("id", user.getId());
                            modify(map);
                        }

                        @Override
                        public void onFailure(Call<ImageUoload> call, Throwable throwable) {
                            Log.d(TAG, "onFailure: " + throwable.getMessage());
                        }
                    });
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("username", s);
                    map.put("id", user.getId());
                    modify(map);
                }

            } else error();
        });

        mImage.setOnClickListener(v -> {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                int i = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                if (i == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                } else {
                    init();
                }
            } else {
                init();
            }

        });
    }

    private void modify(Map<String, Object> data) {
        mApi.modifyUser(data).enqueue(new Callback<UserLogin>() {
            @Override
            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                UserLogin body = response.body();
                if (body.getState() == 200) {
                    Toast.makeText(getContext(), "修改成功", Toast.LENGTH_SHORT).show();
                    App.user = body.getData();
                    setResult(200);
                    finish();
                } else {
                    Toast.makeText(getContext(), body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserLogin> call, Throwable throwable) {

            }
        });

    }

    private void init() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            init();
        } else {
            Toast.makeText(this, "请授予权限！", Toast.LENGTH_SHORT).show();
        }
    }

    private static final String TAG = "SettingAc";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            mUri = data.getData();
            Cursor query = getContentResolver().query(mUri,
                    new String[]{"_data", "_display_name", "mime_type"},
                    null, null, null, null);
            if (query != null && query.moveToNext()) {
                mPath = query.getString(0);
                mName = query.getString(1);
                mMimeType = query.getString(2);
                Log.d(TAG, "onActivityResult: " + mName + "   " + mMimeType);
            }
            if (query != null) {
                query.close();
            }
            Glide.with(getContext()).load(mUri).into(mImage);
        }
    }

    @Override
    protected void initListener() {

    }
}