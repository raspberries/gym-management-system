package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.Result;
import com.ranxi.lian_17.domain.ShiJiaoList;
import com.ranxi.lian_17.domain.ShijiaoClassList;
import com.ranxi.lian_17.domain.UserLogin;
import com.ranxi.lian_17.domain.XueYuan;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.util.BannerUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShiJIaoDetailAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private Banner mBanner;
    private ImageView mImage;
    private TextView mT1;
    private TextView mT2;
    private TextView mT3;
    private TextView mT4;
    private TextView mT5;
    private TextView mT6;
    private RecyclerView mClassList;
    private BaseRecyclerAdapter<ShijiaoClassList.DataDTO> mListAd;
    private RecyclerView mXueyuanList;
    private BaseRecyclerAdapter<XueYuan> mXueyuanListAd;
    private ShijiaoClassList.DataDTO data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shi_j_iao_detail);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mBanner = findViewById(R.id.banner);
        mXueyuanList = findViewById(R.id.xueyuanList);

        show(mBack, mTitle);
        mTitle.setText("私教详情");
        List<Integer> images = new ArrayList<>();
        images.add(R.mipmap.shijiao_banner1);
        images.add(R.mipmap.shijiao_banner2);
        images.add(R.mipmap.shijiao_banner3);
        images.add(R.mipmap.shijiao_banner4);
        Collections.shuffle(images);
        mBanner.setAdapter(new BannerImageAdapter<Integer>(images) {
            @Override
            public void onBindView(BannerImageHolder bannerImageHolder, Integer integer, int i, int i1) {
                bannerImageHolder.imageView.setImageResource(integer);
            }
        });
        mImage = findViewById(R.id.image);
        mT1 = findViewById(R.id.t1);
        mT2 = findViewById(R.id.t2);
        mT3 = findViewById(R.id.t3);
        mT4 = findViewById(R.id.t4);
        mT5 = findViewById(R.id.t5);
        mT6 = findViewById(R.id.t6);
        mClassList = findViewById(R.id.classList);
        mClassList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<ShijiaoClassList.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.shijiao_class_list;
            }

            @Override
            protected void initData(ShijiaoClassList.DataDTO data, int position, View view) {
                ImageView mImage;
                TextView mT1;
                TextView mT2;
                TextView mT3;
                TextView mT4;
                TextView mSubmit;

                mImage = view.findViewById(R.id.image);
                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);
                mT4 = view.findViewById(R.id.t4);
                mT4.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                mSubmit = view.findViewById(R.id.submit);

                Glide.with(getContext())
                        .load(App.imageUrl + data.getPhoto())
                        .error(R.mipmap.renwu)
                        .into(mImage);
                mT1.setText(data.getName());
                mT2.setText(data.getTitle());
                mT3.setText(data.getPrice());

            }
        };
        mClassList.setAdapter(mListAd);
        mXueyuanList.setLayoutManager(new LinearLayoutManager(getContext()));
        mXueyuanList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mXueyuanListAd = new BaseRecyclerAdapter<XueYuan>() {
            @Override
            protected int getRootView() {
                return R.layout.xueyuan_item;
            }

            @Override
            protected void initData(XueYuan data, int position, View view) {
                ImageView mImage1;
                ImageView mImage2;
                TextView mT1;
                TextView mT2;

                mImage1 = view.findViewById(R.id.image1);
                mImage2 = view.findViewById(R.id.image2);
                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);

                Glide.with(getContext()).load(data.getImage1())
                        .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(5f))))
                        .into(mImage1);
                Glide.with(getContext()).load(data.getImage2())
                        .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(5f))))
                        .into(mImage2);
                mT1.setText(data.getTitle());
                mT2.setText(data.getDate());
            }
        };
        mXueyuanList.setAdapter(mXueyuanListAd);

        List<XueYuan> xueyuanData = new ArrayList<>();
        xueyuanData.add(new XueYuan(R.mipmap.x1, R.mipmap.x2, "前后完成两个人，赵哥成功减脂40斤", "减脂5个月"));
        xueyuanData.add(new XueYuan(R.mipmap.x3, R.mipmap.x4, "减掉的不止是40斤更是以重积极的生活方式", "减脂4个月"));
        xueyuanData.add(new XueYuan(R.mipmap.x5, R.mipmap.x6, "6个月减重38斤，后期力量塑形训练为主", "减脂8个月"));
        Collections.shuffle(xueyuanData);
        mXueyuanListAd.setData(xueyuanData);
    }

    @Override
    protected void initData() {
        ShiJiaoList.DataDTO.RecordsDTO data = getIntent().getParcelableExtra("data");
        Glide.with(getContext())
                .load(App.imageUrl + data.getPrivateHeadPortrait())
                .error(R.mipmap.renwu)
                .apply(RequestOptions.circleCropTransform())
                .into(mImage);
        mT1.setText(data.getPrivateName());
        mT2.setText(data.getCumulativeClass());
        mT3.setText(data.getCertificate() + "");
        mT4.setText(data.getFavorableRate());
        mT5.setText(data.getEvaluate() + "");
        mT6.setText(data.getIntroduction());

        mApi.getShijiaoClassList().enqueue(new Callback<ShijiaoClassList>() {
            @Override
            public void onResponse(Call<ShijiaoClassList> call, Response<ShijiaoClassList> response) {
                List<ShijiaoClassList.DataDTO> data1 = response.body().getData();
                List<ShijiaoClassList.DataDTO> list = new ArrayList<>();
                for (ShijiaoClassList.DataDTO dataDTO : data1) {
                    if (dataDTO.getTeacherId().equals(data.getId())) {
                        list.add(dataDTO);
                    }
                }
                mListAd.setData(list);
            }

            @Override
            public void onFailure(Call<ShijiaoClassList> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {
        mListAd.setOnItemListener((data, position, view) -> {
            UserLogin.DataDTO user = App.user;
            if (user != null) {
                this.data = data;
                Intent intent = new Intent(getContext(), ZhiFuAc.class);
                intent.putExtra("userId", App.user.getId());
                intent.putExtra("tag", 2);
                intent.putExtra("title", "私教");
                String price = data.getPrice();
                intent.putExtra("price", price);
                startActivityForResult(intent, 1);
            } else {
                Toast.makeText(getContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), LoginAc.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 200) {
            Log.d(TAG, "onActivityResult: 到之类1l");
            if (App.user != null && this.data != null) {
                init();
                Log.d(TAG, "onActivityResult: 是否购买私教");
            }
        }
    }

    private static final String TAG = "ShiJIaoDetailAc";

    private void init() {
        mApi.payShiJiao(data.getTeacherId(), App.user.getId()).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result body = response.body();
                if (body.getState() == 200) {
                    Toast.makeText(ShiJIaoDetailAc.this, "购买成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ShiJIaoDetailAc.this, body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable throwable) {

            }
        });
        Map<String, Object> mm = new HashMap<>();
        mm.put("userId", App.user.getId());
        mm.put("title", "购买了私教课程");
        mm.put("price", data.getPrice());
        mApi.payOrdeer(mm).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
            }

            @Override
            public void onFailure(Call<Result> call, Throwable throwable) {

            }
        });
    }
}