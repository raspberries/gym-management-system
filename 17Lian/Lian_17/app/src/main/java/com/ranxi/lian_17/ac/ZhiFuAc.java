package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.Result;
import com.ranxi.lian_17.domain.UserLogin;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ZhiFuAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextView mT1;
    private TextView mT2;
    private TextView mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zhi_fu);
        setStateColor();
    }

    private RadioButton mB4;

    @Override
    protected void initView() {
        Intent intent = getIntent();

        mB4 = findViewById(R.id.b4);

        String userId = intent.getStringExtra("userId");
        String title = intent.getStringExtra("title");
        String price = intent.getStringExtra("price");
        int tag = intent.getIntExtra("tag", -1);
        Log.d(TAG, "initView: tag-->" + tag);
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mT1 = findViewById(R.id.t1);
        mT2 = findViewById(R.id.t2);
        mSubmit = findViewById(R.id.submit);
        show(mBack, mTitle);
        mTitle.setText("支付订单");

        mT2.setText("金额：" + price + "元");
        mT1.setText("物品：" + title);
        mSubmit.setOnClickListener(v -> {
            Map<String, Object> mm = new HashMap<>();
            if (tag == 2) {
                if (mB4.isChecked() && App.money < Double.parseDouble(price)) {
                    Toast.makeText(this, "余额不足，请充值后再试！", Toast.LENGTH_SHORT).show();
                    return;
                }
                mm.put("userId", userId);
                mm.put("title", "购买了一张" + title);
                mm.put("price", price);
                mApi.payOrdeer(mm).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        Result body = response.body();
                        if (body.getState() == 200) {
                            Toast.makeText(getContext(), "购买成功！", Toast.LENGTH_SHORT).show();
                            if (mB4.isChecked()) {
                                double v1 = Double.parseDouble(price);
                                App.money = (int) (App.user.getMoney() - v1);
                                modify((int) v1);
                                App.user.setMoney(App.money);
                            }
                            setResult(200);
                            finish();
                        } else {
                            Toast.makeText(getContext(),
                                    body.getMessage().toString(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable throwable) {

                    }
                });
            }


           /* if (tag != 2) {
                modify(Integer.parseInt(price));
                mm.put("userId", userId);
                mm.put("title", "购买了一张" + title);
                mm.put("price", price);
                mApi.payOrdeer(mm).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        Result body = response.body();
                        if (body.getState() == 200) {
                            Toast.makeText(getContext(), "购买成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getContext(),
                                    body.getMessage().toString(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable throwable) {

                    }
                });
            } else {
                if (mB4.isChecked()) {
                    modify((int) Float.parseFloat(price));
                    setResult(200);
                }
                mm.put("userId", userId);
                mm.put("title", "购买了" + title);
                mm.put("price", price);
                mApi.payOrdeer(mm).enqueue(new Callback<Result>() {
                    @Override
                    public void onResponse(Call<Result> call, Response<Result> response) {
                        Result body = response.body();
                        if (body.getState() == 200) {
                            Toast.makeText(getContext(), "购买成功！", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getContext(),
                                    body.getMessage().toString(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Result> call, Throwable throwable) {

                    }
                });
            }*/
        });
    }

    private static final String TAG = "ZhiFuAc";

    public void modify(int money) {
        Map<String, Object> data = new HashMap<>();
        data.put("id", App.user.getId());
        Log.d(TAG, "modify: " + App.user.getMoney() + "   " + money);
        data.put("money", (int) (App.user.getMoney() - money));
        mApi.modifyUser(data).enqueue(new Callback<UserLogin>() {
            @Override
            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
            }

            @Override
            public void onFailure(Call<UserLogin> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}