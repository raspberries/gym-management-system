package com.ranxi.lian_17.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class TuanList {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1","name":"有氧搏击 咪炼健身","photo":"2.png","title":"中强度","teacherId":"1","time":"19:20-20:10","deleted":0,"privateClassList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"2","name":"人鱼马甲 咪炼健身","photo":"2.png","title":"中强度、稳定性、马甲线","teacherId":"2","time":"20:20-21:10","deleted":0,"privateClassList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"3","name":"体态调整 拓普","photo":"2.png","title":"体态调整 拓普","teacherId":"3","time":"20:20-21:20","deleted":0,"privateClassList":[{"id":null,"privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"4","name":"拳击课 拓普","photo":"2.png","title":"拳击训练","teacherId":"4","time":"19:20-20:40","deleted":0,"privateClassList":[{"id":null,"privateName":"森林教练","privateHeadPortrait":"4.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"5","name":"综合格斗 拓普","photo":"2.png","title":"综合格斗","teacherId":"3","time":"19:20-20:20","deleted":0,"privateClassList":[{"id":null,"privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"6","name":"减脂课 拓普","photo":"2.png","title":"减脂课","teacherId":"2","time":"19:20-20:40","deleted":0,"privateClassList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"7","name":"拉伸课 拓普","photo":"2.png","title":"拉伸课","teacherId":"1","time":"15:20-16:20","deleted":0,"privateClassList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"8","name":"人鱼马甲 咪炼健身","photo":"2.png","title":"人鱼马甲","teacherId":"2","time":"19:20-20:40","deleted":0,"privateClassList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"9","name":"拳击课 拓普","photo":"2.png","title":"拳击","teacherId":"3","time":"14:50-20:30","deleted":0,"privateClassList":[{"id":null,"privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"10","name":"有氧搏击 咪炼健身","photo":"2.png","title":"有氧","teacherId":"4","time":"19:20-20:40","deleted":0,"privateClassList":[{"id":null,"privateName":"森林教练","privateHeadPortrait":"4.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"11","name":"拳击课 拓普","photo":"2.png","title":"拳击","teacherId":"2","time":"16:20-17:40","deleted":0,"privateClassList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"12","name":"有氧搏击 咪炼健身","photo":"2.png","title":"有氧","teacherId":"3","time":"17:30-18:30","deleted":0,"privateClassList":[{"id":null,"privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"13","name":"拳击课 拓普","photo":"2.png","title":"拳击","teacherId":"2","time":"19:20-20:40","deleted":0,"privateClassList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"14","name":"拳击课 拓有氧搏击 咪炼健身","photo":"2.png","title":"拳击","teacherId":"1","time":"13:20-14:40","deleted":0,"privateClassList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO implements Parcelable {
        /**
         * id : 1
         * name : 有氧搏击 咪炼健身
         * photo : 2.png
         * title : 中强度
         * teacherId : 1
         * time : 19:20-20:10
         * deleted : 0
         * privateClassList : [{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]
         */

        private String id;
        private String name;
        private String photo;
        private String title;
        private String teacherId;
        private String time;
        private int deleted;
        private List<PrivateClassListDTO> privateClassList;

        protected DataDTO(Parcel in) {
            id = in.readString();
            name = in.readString();
            photo = in.readString();
            title = in.readString();
            teacherId = in.readString();
            time = in.readString();
            deleted = in.readInt();
        }

        public static final Creator<DataDTO> CREATOR = new Creator<DataDTO>() {
            @Override
            public DataDTO createFromParcel(Parcel in) {
                return new DataDTO(in);
            }

            @Override
            public DataDTO[] newArray(int size) {
                return new DataDTO[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public int getDeleted() {
            return deleted;
        }

        public void setDeleted(int deleted) {
            this.deleted = deleted;
        }

        public List<PrivateClassListDTO> getPrivateClassList() {
            return privateClassList;
        }

        public void setPrivateClassList(List<PrivateClassListDTO> privateClassList) {
            this.privateClassList = privateClassList;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(photo);
            dest.writeString(title);
            dest.writeString(teacherId);
            dest.writeString(time);
            dest.writeInt(deleted);
        }

        public static class PrivateClassListDTO {
            /**
             * id : null
             * privateName : 雷丽
             * privateHeadPortrait : 1.png
             * cumulativeClass : null
             * certificate : null
             * favorableRate : null
             * evaluate : null
             * introduction : null
             * deleted : null
             */

            private Object id;
            private String privateName;
            private String privateHeadPortrait;
            private Object cumulativeClass;
            private Object certificate;
            private Object favorableRate;
            private Object evaluate;
            private Object introduction;
            private Object deleted;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getPrivateName() {
                return privateName;
            }

            public void setPrivateName(String privateName) {
                this.privateName = privateName;
            }

            public String getPrivateHeadPortrait() {
                return privateHeadPortrait;
            }

            public void setPrivateHeadPortrait(String privateHeadPortrait) {
                this.privateHeadPortrait = privateHeadPortrait;
            }

            public Object getCumulativeClass() {
                return cumulativeClass;
            }

            public void setCumulativeClass(Object cumulativeClass) {
                this.cumulativeClass = cumulativeClass;
            }

            public Object getCertificate() {
                return certificate;
            }

            public void setCertificate(Object certificate) {
                this.certificate = certificate;
            }

            public Object getFavorableRate() {
                return favorableRate;
            }

            public void setFavorableRate(Object favorableRate) {
                this.favorableRate = favorableRate;
            }

            public Object getEvaluate() {
                return evaluate;
            }

            public void setEvaluate(Object evaluate) {
                this.evaluate = evaluate;
            }

            public Object getIntroduction() {
                return introduction;
            }

            public void setIntroduction(Object introduction) {
                this.introduction = introduction;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }
        }
    }
}
