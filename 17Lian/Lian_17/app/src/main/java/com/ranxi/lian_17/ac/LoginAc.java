package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.UserLogin;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextInputEditText mT1;
    private TextInputEditText mT2;
    private TextView mTo;
    private Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setStateColor();
    }

    private static final String TAG = "LoginAc";

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mT1 = findViewById(R.id.tag1);
        mT2 = findViewById(R.id.t2);
        mTo = findViewById(R.id.to);
        mSubmit = findViewById(R.id.submit);
        show(mBack, mTitle);
        mTitle.setText("登录");
        mTo.setOnClickListener(v -> {
            toActivity(RegisterAc.class);
        });
        mSubmit.setOnClickListener(v -> {
            String s1 = mT1.getText().toString();
            String s2 = mT2.getText().toString();
            if (!App.isEmpty(s1, s2)) {
                Map<String, Object> data = new HashMap<>();
                data.put("account", s1);
                data.put("password", s2);
                mApi.userLogin(data).enqueue(new Callback<UserLogin>() {
                    @Override
                    public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                        UserLogin body = response.body();
                        if (body.getState() == 200) {
                            App.user = body.getData();
                            App.money = (int) body.getData().getMoney();
                            setResult(200);
                            finish();
                        } else {
                            Toast.makeText(LoginAc.this, "账号或者密码错误！", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserLogin> call, Throwable throwable) {
                        Toast.makeText(LoginAc.this, "网络错误！", Toast.LENGTH_SHORT).show();
                    }
                });
            } else error();
        });

    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}