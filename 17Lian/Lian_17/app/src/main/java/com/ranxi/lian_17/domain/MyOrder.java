package com.ranxi.lian_17.domain;

import java.util.List;

public class MyOrder {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"年卡","price":"1680.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511521827941916674","userId":"52e447fa5285dd1ea71faad9aff1d7f9","title":"团体课","price":"30.0","createTime":null,"deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试9","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511523076695302146","userId":"52e447fa5285dd1ea71faad9aff1d7f9","title":"私教","price":"30.0","createTime":"2022-04-06 09:55:58","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试9","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511668432877228034","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:33:34","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511668665782734849","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:34:30","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511669640039862274","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:38:22","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511670221206818818","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:40:40","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511670274310901762","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了私教课程","price":"260.0","createTime":"2022-04-06 07:40:53","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511670322310516738","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:41:04","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511671961268359169","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了一张月卡","price":"219.0","createTime":"2022-04-06 07:47:35","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511672394258944003","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了团体课","price":"30.0","createTime":"2022-04-06 07:49:18","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511679256530358273","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了一张季卡","price":"489.0","createTime":"2022-04-06 08:16:35","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"1511679695271333889","userId":"3e87a812e73205a4773f99fc0c76ebe3","title":"购买了一张季卡","price":"489.0","createTime":"2022-04-06 08:18:19","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"2","userId":"52e447fa5285dd1ea71faad9aff1d7f9","title":"季卡","price":"659.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试9","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"3","userId":"9e6ca4e888613ff7671b60be38b8ac95","title":"月卡","price":"259.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试4","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"4","userId":"ab0d3e9b01851b292a6eea8d57bcb939","title":"年卡","price":"1680.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试1","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"5","userId":"e149aa3927c4231e2a0852d6794e5205","title":"季卡","price":"659.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试6","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"6","userId":"e696f601a604f135d5529e998d45190b","title":"月卡","price":"259.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试5","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]},{"id":"7","userId":"fae82cdeb94235cae3ab1e188cdd5633","title":"月卡","price":"259.0","createTime":"2022-04-01 18:16:52","deleted":"0","userList":[{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"测试3","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * id : 1
         * userId : 3e87a812e73205a4773f99fc0c76ebe3
         * title : 年卡
         * price : 1680.0
         * createTime : 2022-04-01 18:16:52
         * deleted : 0
         * userList : [{"createdTime":null,"createdUser":null,"modifiedTime":null,"modifiedUser":null,"id":null,"username":"txin","account":null,"password":null,"userHeadPortrait":null,"deleted":null}]
         */

        private String id;
        private String userId;
        private String title;
        private String price;
        private String createTime;
        private String deleted;
        private List<UserListDTO> userList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getDeleted() {
            return deleted;
        }

        public void setDeleted(String deleted) {
            this.deleted = deleted;
        }

        public List<UserListDTO> getUserList() {
            return userList;
        }

        public void setUserList(List<UserListDTO> userList) {
            this.userList = userList;
        }

        public static class UserListDTO {
            /**
             * createdTime : null
             * createdUser : null
             * modifiedTime : null
             * modifiedUser : null
             * id : null
             * username : txin
             * account : null
             * password : null
             * userHeadPortrait : null
             * deleted : null
             */

            private Object createdTime;
            private Object createdUser;
            private Object modifiedTime;
            private Object modifiedUser;
            private Object id;
            private String username;
            private Object account;
            private Object password;
            private Object userHeadPortrait;
            private Object deleted;

            public Object getCreatedTime() {
                return createdTime;
            }

            public void setCreatedTime(Object createdTime) {
                this.createdTime = createdTime;
            }

            public Object getCreatedUser() {
                return createdUser;
            }

            public void setCreatedUser(Object createdUser) {
                this.createdUser = createdUser;
            }

            public Object getModifiedTime() {
                return modifiedTime;
            }

            public void setModifiedTime(Object modifiedTime) {
                this.modifiedTime = modifiedTime;
            }

            public Object getModifiedUser() {
                return modifiedUser;
            }

            public void setModifiedUser(Object modifiedUser) {
                this.modifiedUser = modifiedUser;
            }

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public Object getAccount() {
                return account;
            }

            public void setAccount(Object account) {
                this.account = account;
            }

            public Object getPassword() {
                return password;
            }

            public void setPassword(Object password) {
                this.password = password;
            }

            public Object getUserHeadPortrait() {
                return userHeadPortrait;
            }

            public void setUserHeadPortrait(Object userHeadPortrait) {
                this.userHeadPortrait = userHeadPortrait;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }
        }
    }
}
