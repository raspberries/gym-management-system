package com.ranxi.lian_17.fr;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.ScanOptions;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.ac.AllShijiaoAc;
import com.ranxi.lian_17.ac.KaiMenAc;
import com.ranxi.lian_17.ac.LoginAc;
import com.ranxi.lian_17.ac.MainActivity;
import com.ranxi.lian_17.ac.ShiJIaoDetailAc;
import com.ranxi.lian_17.ac.SwitchMenDianAc;
import com.ranxi.lian_17.ac.TuanDetailAc;
import com.ranxi.lian_17.ac.ZhiFuAc;
import com.ranxi.lian_17.base.BaseFr;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.MenDian;
import com.ranxi.lian_17.domain.ShiJiaoList;
import com.ranxi.lian_17.domain.TuanCategory;
import com.ranxi.lian_17.domain.TuanList;
import com.ranxi.lian_17.domain.UserLogin;
import com.ranxi.lian_17.domain.Vip;
import com.ranxi.lian_17.domain.XueYuan;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.util.BannerUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFr extends BaseFr {
    private static final String TAG = "HomeFr";
    private TextView mTitle;
    private Banner mBanner;
    private LinearLayout mMendian;
    private TextView mTenantName;
    private TextView mLocation;
    private TextView mDistance;
    private ImageView mCallPhone;
    private RecyclerView mVipCard;
    private RecyclerView mTuanCategory;
    private RecyclerView mTuanList;
    private BaseRecyclerAdapter<TuanCategory> mTuanAd;
    private LinearLayout mTuanTag;
    private List<TuanList.DataDTO> mTuanClassList;
    private BaseRecyclerAdapter<TuanList.DataDTO> mTuanListAd;
    private LinearLayout mKaimen;
    private LinearLayout mShaoma;
    private RecyclerView mShijiaoList;
    private RecyclerView mXueyuanList;
    private BaseRecyclerAdapter<ShiJiaoList.DataDTO.RecordsDTO> mShijiaoAd;
    private BaseRecyclerAdapter<XueYuan> mXueyuanAd;
    private TextView mShijiaoAll;
    private BaseRecyclerAdapter<Vip> mVipAd;

    @Override
    protected int getRootView() {
        return R.layout.fr_home;
    }

    private Calendar mCalendar = Calendar.getInstance();

    @Override
    protected void initView(View view) {
        mTitle = view.findViewById(R.id.title);
        mBanner = view.findViewById(R.id.banner);
        mTitle.setText("17练");
        //切换门店
        mMendian = view.findViewById(R.id.mendian);
        mTenantName = view.findViewById(R.id.tenantName);
        mLocation = view.findViewById(R.id.location);
        mDistance = view.findViewById(R.id.distance);
        mCallPhone = view.findViewById(R.id.callPhone);

        mVipCard = view.findViewById(R.id.vipCard);
        mVipCard.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mVipAd = new BaseRecyclerAdapter<Vip>() {
            @Override
            protected int getRootView() {
                return R.layout.vip_item;
            }

            @Override
            protected void initData(Vip data, int position, View view) {
                TextView mT1;
                TextView mT2;
                TextView mT3;

                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);
                mT1.setText(data.getName());
                mT2.setText(data.getPrice());
                mT3.setOnClickListener(v -> {
                    UserLogin.DataDTO user = App.user;
                    if (user != null) {
                        Intent intent = new Intent(getContext(), ZhiFuAc.class);
                        intent.putExtra("userId", App.user.getId());
                        intent.putExtra("title", data.getName());
                        intent.putExtra("tag", 2);
                        intent.putExtra("price", data.getPrice());
                        startActivityForResult(intent, 4);
                    } else {
                        Toast.makeText(getContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), LoginAc.class);
                        startActivityForResult(intent, 1);
                    }
                });
            }
        };
        mVipCard.setAdapter(mVipAd);
        List<Vip> vipData = new ArrayList<>();
        vipData.add(new Vip("季卡", "489"));
        vipData.add(new Vip("月卡", "219"));
        vipData.add(new Vip("年卡", "1350.4"));
        mVipAd.setData(vipData);


        mTuanTag = view.findViewById(R.id.tuanTag);
        mTuanCategory = view.findViewById(R.id.tuanCategory);
        mTuanList = view.findViewById(R.id.tuanList);

        mTuanCategory.setLayoutManager(new GridLayoutManager(getContext(), 7, RecyclerView.VERTICAL, false));
        mTuanAd = new BaseRecyclerAdapter<TuanCategory>() {

            @Override
            protected int getRootView() {
                return R.layout.ke_date_item;
            }

            private Map<Integer, View> mIntegerViewMap = new HashMap<>();
            private int mCurrentTextColor;

            @Override
            protected void initData(TuanCategory data, int position, View view) {
                TextView mT1;
                TextView mT2;

                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT1.setText(data.getTitle());
                mT2.setText(data.getNum() + "");
                mCurrentTextColor = mT1.getCurrentTextColor();
                mIntegerViewMap.put(position, view);
                if (position == 0) {
                    mT1.setTextColor(Color.BLACK);
                    mT2.setTextColor(Color.BLACK);
                }
            }

            @Override
            public void onClick(TuanCategory data, int position, View view) {
                for (Map.Entry<Integer, View> entry : mIntegerViewMap.entrySet()) {
                    View value = entry.getValue();
                    TextView mT1;
                    TextView mT2;
                    mT1 = value.findViewById(R.id.t1);
                    mT2 = value.findViewById(R.id.t2);
                    if (position == entry.getKey()) {
                        mT1.setTextColor(Color.BLACK);
                        mT2.setTextColor(Color.BLACK);
                    } else {
                        mT1.setTextColor(mCurrentTextColor);
                        mT2.setTextColor(mCurrentTextColor);
                    }
                }
            }
        };
        mTuanCategory.setAdapter(mTuanAd);
        List<TuanCategory> tuanList = new ArrayList<>();
        for (int j = 0; j < 7; j++) {
            tuanList.add(getTuan(j));
        }
        mTuanAd.setData(tuanList);

        mTuanList.setLayoutManager(new LinearLayoutManager(getContext()));
        mTuanListAd = new BaseRecyclerAdapter<TuanList.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.tuan_class_list_item;
            }

            @Override
            protected void initData(TuanList.DataDTO data, int position, View view) {
                TextView mT1;
                TextView mT2;
                TextView mT3;
                TextView mT4;
                TextView mSubmit;

                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);
                mT4 = view.findViewById(R.id.t4);
                mSubmit = view.findViewById(R.id.submit);
                mT1.setText(data.getName());
                mT2.setText(data.getPrivateClassList().get(0).getPrivateName());
                mT3.setText(data.getTime());
                mT4.setText(data.getTitle());
            }
        };
        mTuanList.setAdapter(mTuanListAd);


        //开门码
        mKaimen = view.findViewById(R.id.kaimen);
        mKaimen.setOnClickListener(v -> {
            UserLogin.DataDTO user = App.user;
            if (user == null) {
                Toast.makeText(getContext(), "请先登录后再试！", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).setMenu(R.id.personal);
            } else {
                toActivity(KaiMenAc.class);
            }
        });
        mShaoma = view.findViewById(R.id.shaoma);
        //扫码签到
        mShaoma.setOnClickListener(v -> {
            UserLogin.DataDTO user = App.user;
            if (user == null) {
                Toast.makeText(getContext(), "请先登录后再试！", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).setMenu(R.id.personal);
            } else {
                ScanOptions options = new ScanOptions();
                options.setOrientationLocked(true);
                options.setBeepEnabled(true);
                options.setBarcodeImageEnabled(true);
                options.setPrompt("请放置签到二维码");
                Intent intent = options.createScanIntent(getContext());
                startActivityForResult(intent, 2);
            }

        });


        mShijiaoList = view.findViewById(R.id.shijiaoList);
        mXueyuanList = view.findViewById(R.id.xueyuanList);
        mShijiaoList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mShijiaoAd = new BaseRecyclerAdapter<ShiJiaoList.DataDTO.RecordsDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.shijiao_item;
            }

            @Override
            protected void initData(ShiJiaoList.DataDTO.RecordsDTO data, int position, View view) {
                ImageView mImage;
                TextView mT1;
                TextView mT2;

                mImage = view.findViewById(R.id.image);
                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                Glide.with(getContext()).load(App.imageUrl + data.getPrivateHeadPortrait())
                        .error(R.mipmap.shijiao_tp)
                        .into(mImage);
                mT1.setText(data.getPrivateName());
                mT2.setText("好评率" + data.getFavorableRate());
            }
        };
        mShijiaoList.setAdapter(mShijiaoAd);

        mXueyuanList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        mXueyuanAd = new BaseRecyclerAdapter<XueYuan>() {
            @Override
            protected int getRootView() {
                return R.layout.xueyuan_item;
            }

            @Override
            protected void initData(XueYuan data, int position, View view) {
                ImageView mImage1;
                ImageView mImage2;
                TextView mT1;
                TextView mT2;

                mImage1 = view.findViewById(R.id.image1);
                mImage2 = view.findViewById(R.id.image2);
                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);

                Glide.with(getContext()).load(data.getImage1())
                        .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(5f))))
                        .into(mImage1);
                Glide.with(getContext()).load(data.getImage2())
                        .apply(new RequestOptions().transform(new RoundedCorners((int) BannerUtils.dp2px(5f))))
                        .into(mImage2);
                mT1.setText(data.getTitle());
                mT2.setText(data.getDate());
            }
        };
        mXueyuanList.setAdapter(mXueyuanAd);
        List<XueYuan> xueyuanData = new ArrayList<>();
        xueyuanData.add(new XueYuan(R.mipmap.x1, R.mipmap.x2, "前后完成两个人，赵哥成功减脂40斤", "减脂5个月"));
        xueyuanData.add(new XueYuan(R.mipmap.x3, R.mipmap.x4, "减掉的不止是40斤更是以重积极的生活方式", "减脂4个月"));
        xueyuanData.add(new XueYuan(R.mipmap.x5, R.mipmap.x6, "6个月减重38斤，后期力量塑形训练为主", "减脂8个月"));
        mXueyuanAd.setData(xueyuanData);

        //全部的私教
        mShijiaoAll = view.findViewById(R.id.shijiaoAll);
        mShijiaoAll.setOnClickListener(v -> toActivity(AllShijiaoAc.class));
    }

    private int today = mCalendar.get(Calendar.DAY_OF_MONTH);

    private TuanCategory getTuan(int j) {
        String title = "";
        int num = today;
        switch (j) {
            case 0:
                title = "今天";
                break;
            case 1:
                title = "明天";
                num = num + 1;
                break;
            case 2:
                num = num + 2;
                title = getWeek(num);
                break;
            case 3:
                num = num + 3;
                title = getWeek(num);
                break;
            case 4:
                num = num + 4;
                title = getWeek(num);
                break;
            case 5:
                num = num + 5;
                title = getWeek(num);
                break;
            case 6:
                num = num + 6;
                title = getWeek(num);
                break;
        }
        return new TuanCategory(title, num);
    }

    private String getWeek(int num) {
        mCalendar.set(Calendar.YEAR, Calendar.MONTH - 1, num);
        int i = mCalendar.get(Calendar.DAY_OF_WEEK);
        String target = null;
        switch (i) {
            case 1:
                target = "周日";
                break;
            case 2:
                target = "周一";
                break;
            case 3:
                target = "周二";
                break;
            case 4:
                target = "周三";
                break;
            case 5:
                target = "周四";
                break;
            case 6:
                target = "周五";
                break;
            case 7:
                target = "周六";
                break;
        }
        return target;
    }

    @Override
    protected void initData() {
        initBanner();
        initTuan();
        initShiJiao();
    }

    private void initShiJiao() {
        mApi.getShijiaoList().enqueue(new Callback<ShiJiaoList>() {
            @Override
            public void onResponse(Call<ShiJiaoList> call, Response<ShiJiaoList> response) {
                ShiJiaoList.DataDTO data = response.body().getData();
                List<ShiJiaoList.DataDTO.RecordsDTO> records = data.getRecords();
                mShijiaoAd.setData(records);
            }

            @Override
            public void onFailure(Call<ShiJiaoList> call, Throwable throwable) {

            }
        });
    }

    private void initTuan() {
        mApi.getTuanList().enqueue(new Callback<TuanList>() {
            @Override
            public void onResponse(Call<TuanList> call, Response<TuanList> response) {
                mTuanClassList = response.body().getData();
                switchTuanClass();
            }

            @Override
            public void onFailure(Call<TuanList> call, Throwable throwable) {
                Toast.makeText(getContext(), "网络错误！", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Random mRandom = new Random();

    /**
     * 切换团体课
     */
    private void switchTuanClass() {
        Collections.shuffle(mTuanClassList);
        Log.d(TAG, "switchTuanClass: " + mTuanClassList.size());
        int i = mRandom.nextInt(mTuanClassList.size() / 2);
        List<TuanList.DataDTO> list = mTuanClassList.subList(0, i);
        if (list.size() == 0) {
            mTuanList.setVisibility(View.GONE);
            mTuanTag.setVisibility(View.VISIBLE);
        } else {
            mTuanList.setVisibility(View.VISIBLE);
            mTuanListAd.setData(list);
            mTuanTag.setVisibility(View.GONE);
        }
    }

    private void initBanner() {
        List<Integer> images = new ArrayList<>();
        images.add(R.mipmap.banner1);
        images.add(R.mipmap.banner2);
        images.add(R.mipmap.banner3);
        images.add(R.mipmap.banner4);
        images.add(R.mipmap.banner5);
        mBanner.setAdapter(new BannerImageAdapter<Integer>(images) {
            @Override
            public void onBindView(BannerImageHolder bannerImageHolder, Integer integer, int i, int i1) {
                Glide.with(getContext()).load(integer).into(bannerImageHolder.imageView);
            }
        }, false);
        mBanner.isAutoLoop(false);
        mBanner.setBannerRound2(BannerUtils.dp2px(10));
    }

    @Override
    protected void initListener() {
        //切换门店
        mMendian.setOnClickListener(v -> {
            toActivityForResult(SwitchMenDianAc.class, 1);
        });
        mApi.getMenDian().enqueue(new Callback<MenDian>() {
            @Override
            public void onResponse(Call<MenDian> call, Response<MenDian> response) {
                List<MenDian.DataDTO> data = response.body().getData();
                MenDian.DataDTO dataDTO = data.get(0);
                mTenantName.setText(dataDTO.getName());
                mLocation.setText(dataDTO.getTitle());
                mDistance.setText("距我" + dataDTO.getLocation() + "km");
            }

            @Override
            public void onFailure(Call<MenDian> call, Throwable throwable) {

            }
        });

        mCallPhone.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:123"));
            startActivity(intent);
        });
        mTuanAd.setOnItemListener(new BaseRecyclerAdapter.OnItemListener<TuanCategory>() {
            int tuanNum = -1;

            @Override
            public void onClick(TuanCategory data, int position, View view) {
                int num = data.getNum();
                if (num != tuanNum) {
                    HomeFr.this.switchTuanClass();
                }
                tuanNum = num;
            }
        });

        mShijiaoAd.setOnItemListener(new BaseRecyclerAdapter.OnItemListener<ShiJiaoList.DataDTO.RecordsDTO>() {
            @Override
            public void onClick(ShiJiaoList.DataDTO.RecordsDTO data, int position, View view) {
                Intent intent = new Intent(HomeFr.this.getContext(), ShiJIaoDetailAc.class);
                intent.putExtra("data", data);
                HomeFr.this.startActivity(intent);
            }
        });

        mTuanListAd.setOnItemListener(new BaseRecyclerAdapter.OnItemListener<TuanList.DataDTO>() {
            @Override
            public void onClick(TuanList.DataDTO data, int position, View view) {
                Intent intent = new Intent(HomeFr.this.getContext(), TuanDetailAc.class);
                intent.putExtra("data", data);
                TuanList.DataDTO.PrivateClassListDTO listDTO = data.getPrivateClassList().get(0);
                intent.putExtra("name", listDTO.getPrivateName());
                intent.putExtra("tx", listDTO.getPrivateHeadPortrait());
                HomeFr.this.startActivity(intent);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == 200 && data != null) {
            MenDian.DataDTO dto = data.getParcelableExtra("data");
            mTenantName.setText(dto.getName());
            mLocation.setText(dto.getTitle());
            mDistance.setText("距我" + dto.getLocation() + "km");
        }
        if (requestCode == 2) {
            IntentResult intentResult = IntentIntegrator.parseActivityResult(resultCode, data);
            String contents = intentResult.getContents();
            String barcodeImagePath = intentResult.getBarcodeImagePath();
            Log.d(TAG, "onActivityResult: " + contents + "   " + barcodeImagePath);
        }
        Log.d(TAG, "onActivityResult: " + requestCode);
        if (requestCode == 4) {
            if (resultCode == 200) {

            }
        }
    }
}
