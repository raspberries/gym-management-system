package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.fr.HomeFr;
import com.ranxi.lian_17.fr.PersonalFr;

public class MainActivity extends BaseAc {
    private Fragment mFragment;
    private FrameLayout mContainer;
    private BottomNavigationView mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void initView() {
        mContainer = findViewById(R.id.container);
        mMenu = findViewById(R.id.menu);
        HomeFr homeFr = new HomeFr();
        PersonalFr personalFr = new PersonalFr();
        swFr(homeFr);
        mMenu.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.home:
                    swFr(homeFr);
                    break;
                case R.id.personal:
                    swFr(personalFr);
                    break;
            }
            return true;
        });
    }

    public void swFr(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (mFragment != null)
            transaction.hide(mFragment);
        if (fragment.isAdded()) {
            transaction.show(fragment);
        } else transaction.add(R.id.container, fragment);
        mFragment = fragment;
        transaction.commit();

    }

    public void setMenu(int id) {
        mMenu.setSelectedItemId(id);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}