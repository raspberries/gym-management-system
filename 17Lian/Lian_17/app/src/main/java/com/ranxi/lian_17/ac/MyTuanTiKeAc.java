package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.MyTuanList;
import com.ranxi.lian_17.domain.Result;

import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTuanTiKeAc extends BaseAc {
    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mList;
    private BaseRecyclerAdapter<MyTuanList.DataDTO> mListAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tuan_ti_ke);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mList = findViewById(R.id.list);
        show(mBack, mTitle);
        mTitle.setText("我的团体课");
        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<MyTuanList.DataDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.wode_tuantike_item;
            }

            @Override
            protected void initData(MyTuanList.DataDTO data, int position, View view) {
                ImageView mImage;
                TextView mT1;
                TextView mSubmit;
                TextView mT2;
                TextView mT3;

                mT2 = view.findViewById(R.id.t2);
                mT3 = view.findViewById(R.id.t3);

                mImage = view.findViewById(R.id.image);
                mT1 = view.findViewById(R.id.t1);
                mSubmit = view.findViewById(R.id.submit);
                MyTuanList.DataDTO.PublicClassListDTO list = data.getPublicClassList().get(0);
                Glide.with(getContext()).load(App.imageUrl + list.getPhoto())
                        .error(R.mipmap.renwu)
                        .into(mImage);
                mT1.setText("课程名称：" + list.getName());
                mT2.setText("类型：" + list.getTitle());
                mT3.setText("好评率：99.8%");
                mSubmit.setOnClickListener(v -> {
                    mData.remove(position);
                    notifyDataSetChanged();
                    mApi.unTuanTi(data.getId()).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            Toast.makeText(getContext(), "取消成功", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable throwable) {

                        }
                    });
                });
            }
        };
        mList.setAdapter(mListAd);
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    private static final String TAG = "MyTuanTiKeAc";

    @Override
    protected void initData() {
        mApi.getMyTuan(App.user.getId()).enqueue(new Callback<MyTuanList>() {
            @Override
            public void onResponse(Call<MyTuanList> call, Response<MyTuanList> response) {
                List<MyTuanList.DataDTO> data = response.body().getData();
                ListIterator<MyTuanList.DataDTO> iterator = data.listIterator();
                while (iterator.hasNext()) {
                    MyTuanList.DataDTO next = iterator.next();
                    if (next.getPublicClassCancel() == 1) {
                        iterator.remove();
                    }
                }
                Log.d(TAG, "onResponse: " + data.size());
                mListAd.setData(data);
            }

            @Override
            public void onFailure(Call<MyTuanList> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {

    }
}