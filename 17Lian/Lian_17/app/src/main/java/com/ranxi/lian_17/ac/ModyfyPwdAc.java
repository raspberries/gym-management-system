package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.UserLogin;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModyfyPwdAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextInputEditText mT1;
    private TextInputEditText mT2;
    private TextInputEditText mT3;
    private TextView mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modyfy_pwd);
        setStateColor();
    }

    @Override
    protected void initView() {

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mT1 = findViewById(R.id.t1);
        mT2 = findViewById(R.id.t2);
        mT3 = findViewById(R.id.t3);
        mSubmit = findViewById(R.id.submit);
        show(mBack, mTitle);
        mTitle.setText("修改密码");
        mSubmit.setOnClickListener(v -> {
            String s1 = mT1.getText().toString();
            String s2 = mT2.getText().toString();
            String s3 = mT3.getText().toString();
            if (!App.isEmpty(s1, s2, s3)) {
                if (s2.equals(s3)) {
                    UserLogin.DataDTO user = App.user;
                    if (s2.equals(user.getPassword())) {
                        Map<String, Object> data = new HashMap<>();
                        data.put("id", user.getId());
                        data.put("password", s2);
                        mApi.modifyUser(data).enqueue(new Callback<UserLogin>() {
                            @Override
                            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                                UserLogin body = response.body();
                                if (body.getState() == 200) {
                                    Toast.makeText(ModyfyPwdAc.this, "修改成功", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(ModyfyPwdAc.this, body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<UserLogin> call, Throwable throwable) {

                            }
                        });
                    }else {
                        Toast.makeText(this, "原密码错误！", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(this, "两次输入的新密码不一致!", Toast.LENGTH_SHORT).show();
                }
            } else error();
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}