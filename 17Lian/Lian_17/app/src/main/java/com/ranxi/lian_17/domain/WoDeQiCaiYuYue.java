package com.ranxi.lian_17.domain;

import java.util.List;

public class WoDeQiCaiYuYue {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1511611432923402242","equipmentId":null,"userId":null,"equipmentCancel":0,"privateAdminCancel":null,"publicClassCancel":null,"privateAdminId":null,"publicClassId":null,"equipmentList":[{"id":null,"name":"哑铃","number":null,"day":null,"createTime":null,"deleted":null}],"privateAdminList":null,"publicClassList":null}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * id : 1511611432923402242
         * equipmentId : null
         * userId : null
         * equipmentCancel : 0
         * privateAdminCancel : null
         * publicClassCancel : null
         * privateAdminId : null
         * publicClassId : null
         * equipmentList : [{"id":null,"name":"哑铃","number":null,"day":null,"createTime":null,"deleted":null}]
         * privateAdminList : null
         * publicClassList : null
         */

        private String id;
        private Object equipmentId;
        private Object userId;
        private int equipmentCancel;
        private Object privateAdminCancel;
        private Object publicClassCancel;
        private Object privateAdminId;
        private Object publicClassId;
        private Object privateAdminList;
        private Object publicClassList;
        private List<EquipmentListDTO> equipmentList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(Object equipmentId) {
            this.equipmentId = equipmentId;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public int getEquipmentCancel() {
            return equipmentCancel;
        }

        public void setEquipmentCancel(int equipmentCancel) {
            this.equipmentCancel = equipmentCancel;
        }

        public Object getPrivateAdminCancel() {
            return privateAdminCancel;
        }

        public void setPrivateAdminCancel(Object privateAdminCancel) {
            this.privateAdminCancel = privateAdminCancel;
        }

        public Object getPublicClassCancel() {
            return publicClassCancel;
        }

        public void setPublicClassCancel(Object publicClassCancel) {
            this.publicClassCancel = publicClassCancel;
        }

        public Object getPrivateAdminId() {
            return privateAdminId;
        }

        public void setPrivateAdminId(Object privateAdminId) {
            this.privateAdminId = privateAdminId;
        }

        public Object getPublicClassId() {
            return publicClassId;
        }

        public void setPublicClassId(Object publicClassId) {
            this.publicClassId = publicClassId;
        }

        public Object getPrivateAdminList() {
            return privateAdminList;
        }

        public void setPrivateAdminList(Object privateAdminList) {
            this.privateAdminList = privateAdminList;
        }

        public Object getPublicClassList() {
            return publicClassList;
        }

        public void setPublicClassList(Object publicClassList) {
            this.publicClassList = publicClassList;
        }

        public List<EquipmentListDTO> getEquipmentList() {
            return equipmentList;
        }

        public void setEquipmentList(List<EquipmentListDTO> equipmentList) {
            this.equipmentList = equipmentList;
        }

        public static class EquipmentListDTO {
            /**
             * id : null
             * name : 哑铃
             * number : null
             * day : null
             * createTime : null
             * deleted : null
             */

            private Object id;
            private String name;
            private Object number;
            private Object day;
            private Object createTime;
            private Object deleted;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getNumber() {
                return number;
            }

            public void setNumber(Object number) {
                this.number = number;
            }

            public Object getDay() {
                return day;
            }

            public void setDay(Object day) {
                this.day = day;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }
        }
    }
}
