package com.ranxi.lian_17.domain;

public class XueYuan {
    private int image1;
    private int image2;
    private String title;
    private String date;

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public int getImage2() {
        return image2;
    }

    public void setImage2(int image2) {
        this.image2 = image2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public XueYuan(int image1, int image2, String title, String date) {
        this.image1 = image1;
        this.image2 = image2;
        this.title = title;
        this.date = date;
    }
}
