package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.Result;
import com.ranxi.lian_17.domain.TuanList;
import com.ranxi.lian_17.domain.UserLogin;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TuanDetailAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private Banner mBanner;
    private TextView mT1;
    private RatingBar mBar;
    private TextView mT2;
    private TextView mT3;
    private TextView mT4;
    private ImageView mTx;
    private LinearLayout mSubmit;
    private TuanList.DataDTO mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuan_detail);
    }

    @Override
    protected void initView() {

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mSubmit = findViewById(R.id.submit);

        mTx = findViewById(R.id.tx);
        mBanner = findViewById(R.id.banner);
        mT1 = findViewById(R.id.t1);
        mBar = findViewById(R.id.bar);
        mT2 = findViewById(R.id.t2);
        mT3 = findViewById(R.id.t3);
        mT4 = findViewById(R.id.t4);
        show(mBack, mTitle);
        mTitle.setText("团课详情");
        List<Integer> images = new ArrayList<>();
        images.add(R.mipmap.tuan_banner1);
        images.add(R.mipmap.tuan_banner2);
        mBanner.setAdapter(new BannerImageAdapter<Integer>(images) {
            @Override
            public void onBindView(BannerImageHolder bannerImageHolder, Integer integer, int i, int i1) {
                bannerImageHolder.imageView.setImageResource(integer);
            }
        });
    }

    private Random mRandom = new Random();

    @Override
    protected void initData() {
        Intent intent = getIntent();
        mData = intent.getParcelableExtra("data");
        String name = intent.getStringExtra("name");
        String tx = intent.getStringExtra("tx");
        Glide.with(getContext()).load(App.imageUrl + tx).error(R.mipmap.renwu).into(mTx);
        int i = mRandom.nextInt(2) + 3;
        mT1.setText(mData.getName());
        mBar.setRating(i);
        mT2.setText("评价" + mRandom.nextInt(100));
        mT3.setText("参团" + mRandom.nextInt(100) + "人");
        mT4.setText(name);
    }

    @Override
    protected void initListener() {
        mSubmit.setOnClickListener(v -> {
            UserLogin.DataDTO user = App.user;
            if (user != null) {
                Intent intent = new Intent(getContext(), ZhiFuAc.class);
                intent.putExtra("userId", App.user.getId());
                intent.putExtra("tag", 2);
                intent.putExtra("title", "团体课");
                intent.putExtra("price", "29.9");
                startActivityForResult(intent, 1);
            } else {
                Toast.makeText(getContext(), "请先登录！", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), LoginAc.class);
                startActivityForResult(intent, 1);
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 200) {
            init();
        }
    }

    private void init() {
        Map<String, Object> mm = new HashMap<>();
        mm.put("userId", App.user.getId());
        mm.put("title", "购买了团体课");
        mm.put("price", "29.9");
        mApi.payOrdeer(mm).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Result body = response.body();
                if (body.getState() == 200) {
                    Toast.makeText(TuanDetailAc.this, "购买成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable throwable) {

            }
        });
        Map<String, Object> data = new HashMap<>();
        data.put("publicClassId", mData.getId());
        data.put("userId", App.user.getId());
        mApi.payTuanTike(data).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                Log.d(TAG, "onResponse: " + response.body());
            }

            @Override
            public void onFailure(Call<Result> call, Throwable throwable) {

            }
        });
    }

    private static final String TAG = "TuanDetailAc";
}