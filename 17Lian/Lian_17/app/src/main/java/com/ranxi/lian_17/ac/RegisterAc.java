package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.UserRegister;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private TextInputEditText mT1;
    private TextInputEditText mT2;
    private TextInputEditText mT3;
    private TextView mTo;
    private Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setStateColor();
    }

    @Override
    protected void initView() {
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mT1 = findViewById(R.id.t1);
        mT2 = findViewById(R.id.t2);
        mT3 = findViewById(R.id.t3);
        mTo = findViewById(R.id.to);
        mSubmit = findViewById(R.id.submit);
        show(mBack, mTitle);
        mTitle.setText("注册");

        mSubmit.setOnClickListener(v -> {
            String s1 = mT1.getText().toString();
            String s2 = mT2.getText().toString();
            String s3 = mT3.getText().toString();
            if (!App.isEmpty(s1, s2, s3)) {
                Map<String, Object> data = new HashMap<>();
                data.put("username", s2);
                data.put("account", s1);
                data.put("password", s3);
                mApi.userRegister(data).enqueue(new Callback<UserRegister>() {
                    @Override
                    public void onResponse(Call<UserRegister> call, Response<UserRegister> response) {
                        UserRegister body = response.body();
                        if (body.getState() == 200) {
                            Toast.makeText(RegisterAc.this, "注册成功", Toast.LENGTH_SHORT).show();
                            finish();
                        } else
                            Toast.makeText(RegisterAc.this, body.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<UserRegister> call, Throwable throwable) {
                        Toast.makeText(getContext(), "网络错误！", Toast.LENGTH_SHORT).show();
                    }
                });
            } else error();
        });
        mTo.setOnClickListener(v -> finish());
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}