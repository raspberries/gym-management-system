package com.ranxi.lian_17.ac;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.base.BaseRecyclerAdapter;
import com.ranxi.lian_17.domain.QiCaiList;
import com.ranxi.lian_17.domain.Result;
import com.ranxi.lian_17.domain.WoDeQiCaiYuYue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WoDeQiCaiAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;
    private RecyclerView mList;
    private BaseRecyclerAdapter<QiCaiList.DataDTO.RecordsDTO> mListAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wo_de_qi_cai);
        setStateColor();
    }

    @Override
    protected void initView() {

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mList = findViewById(R.id.list);
        show(mBack, mTitle);
        mTitle.setText("我的器材");
        mList.setLayoutManager(new LinearLayoutManager(getContext()));
        mListAd = new BaseRecyclerAdapter<QiCaiList.DataDTO.RecordsDTO>() {
            @Override
            protected int getRootView() {
                return R.layout.qicai_item;
            }

            @Override
            protected void initData(QiCaiList.DataDTO.RecordsDTO data, int position, View view) {
                TextView mT1;
                TextView mT2;
                TextView mSubmit;

                mT1 = view.findViewById(R.id.t1);
                mT2 = view.findViewById(R.id.t2);
                mT1.setText("名称：" + data.getName());
                mT2.setText("剩余数量：" + data.getNumber());
                mSubmit = view.findViewById(R.id.submit);
                if (Integer.parseInt(data.getNumber()) == 0) {
                    mSubmit.setEnabled(false);
                    mSubmit.setText("没有了");
                } else {
                    mSubmit.setText("借用");
                }
                if (data.isYuyue()) {
                    mSubmit.setText("已借用");
                } else {
                    mSubmit.setOnClickListener(v -> {
                        mSubmit.setEnabled(false);
                        mSubmit.setText("已借用");
                        mT2.setText("剩余数量：" + (Integer.parseInt(data.getNumber()) - 1));
                        Map<String, Object> body = new HashMap<>();
                        body.put("equipmentId", data.getId());
                        body.put("userId", App.user.getId());
                        mApi.yuyueQiCai(body).enqueue(new Callback<Result>() {
                            @Override
                            public void onResponse(Call<Result> call, Response<Result> response) {
                                Result body = response.body();
                                if (body.getState() == 200) {
                                    Toast.makeText(WoDeQiCaiAc.this, "借用成功", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Result> call, Throwable throwable) {

                            }
                        });
                    });

                }
            }
        };
        mList.setAdapter(mListAd);
        mList.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void initData() {
        mApi.getQiCaiList().enqueue(new Callback<QiCaiList>() {
            @Override
            public void onResponse(Call<QiCaiList> call, Response<QiCaiList> response) {
                List<QiCaiList.DataDTO.RecordsDTO> data = response.body().getData().getRecords();
                mApi.getWoDeQicaiYuyue(App.user.getId()).enqueue(new Callback<WoDeQiCaiYuYue>() {
                    @Override
                    public void onResponse(Call<WoDeQiCaiYuYue> call, Response<WoDeQiCaiYuYue> response) {
                        List<WoDeQiCaiYuYue.DataDTO> list = response.body().getData();
                        for (WoDeQiCaiYuYue.DataDTO dataDTO : list) {
                            if (dataDTO.getEquipmentCancel() == 0) {
                                //已经借用
                                for (QiCaiList.DataDTO.RecordsDTO dto : data) {
                                    if (dto.getName().equals(dataDTO.getEquipmentList().get(0).getName())) {
                                        dto.setYuyue(true);
                                        break;
                                    }
                                }
                            }
                        }
                        mListAd.setData(data);
                    }

                    @Override
                    public void onFailure(Call<WoDeQiCaiYuYue> call, Throwable throwable) {

                    }
                });

            }

            @Override
            public void onFailure(Call<QiCaiList> call, Throwable throwable) {

            }
        });
    }

    @Override
    protected void initListener() {

    }
}