package com.ranxi.lian_17.domain;

public class Result {
    /**
     * state : 200
     * message : null
     * data : null
     */

    private int state;
    private Object message;
    private Object data;

    @Override
    public String toString() {
        return "Result{" +
                "state=" + state +
                ", message=" + message +
                ", data=" + data +
                '}';
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
