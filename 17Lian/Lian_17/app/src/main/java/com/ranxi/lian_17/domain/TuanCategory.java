package com.ranxi.lian_17.domain;

public class TuanCategory {
    private String title;
    private int num;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public TuanCategory(String title, int num) {
        this.title = title;
        this.num = num;
    }
}
