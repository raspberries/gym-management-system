package com.ranxi.lian_17.domain;

public class ImageUoload {
    /**
     * state : 200
     * message : null
     * data : {"fileName":"7142b8845c1b4c3b9e76882822841502返回2.png","fileUrl":"http://127.0.0.1:8082/getImage/7142b8845c1b4c3b9e76882822841502返回2.png"}
     */

    private int state;
    private Object message;
    private DataDTO data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * fileName : 7142b8845c1b4c3b9e76882822841502返回2.png
         * fileUrl : http://127.0.0.1:8082/getImage/7142b8845c1b4c3b9e76882822841502返回2.png
         */

        private String fileName;
        private String fileUrl;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public String getFileUrl() {
            return fileUrl;
        }

        public void setFileUrl(String fileUrl) {
            this.fileUrl = fileUrl;
        }
    }
}
