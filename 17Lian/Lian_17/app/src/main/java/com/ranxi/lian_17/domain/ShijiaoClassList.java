package com.ranxi.lian_17.domain;

import java.util.List;

public class ShijiaoClassList {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1","teacherId":"1","name":"增肌课","title":"增肌课 托妞","photo":"1.jpg","price":"260","deleted":0,"privateAdminList":[{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"2","teacherId":"2","name":"减脂课","title":"增肌课 托妞","photo":"2.jpg","price":"260","deleted":0,"privateAdminList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"3","teacherId":"3","name":"拉伸课","title":"拉伸课 托妞","photo":"1.jpg","price":"260","deleted":0,"privateAdminList":[{"id":null,"privateName":"转山","privateHeadPortrait":"3.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"4","teacherId":"4","name":"拳击课","title":"拳击课 托妞","photo":"2.jpg","price":"260","deleted":0,"privateAdminList":[{"id":null,"privateName":"森林教练","privateHeadPortrait":"4.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]},{"id":"5","teacherId":"2","name":"体态调整","title":"体态调整 托妞","photo":"1.jpg","price":"260","deleted":0,"privateAdminList":[{"id":null,"privateName":"严涛","privateHeadPortrait":"2.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        public boolean isIs() {
            return is;
        }

        public void setIs(boolean is) {
            this.is = is;
        }

        /**
         * id : 1
         * teacherId : 1
         * name : 增肌课
         * title : 增肌课 托妞
         * photo : 1.jpg
         * price : 260
         * deleted : 0
         * privateAdminList : [{"id":null,"privateName":"雷丽","privateHeadPortrait":"1.png","cumulativeClass":null,"certificate":null,"favorableRate":null,"evaluate":null,"introduction":null,"deleted":null}]
         */

        private boolean is;
        private String id;
        private String teacherId;
        private String name;
        private String title;
        private String photo;
        private String price;
        private int deleted;
        private List<PrivateAdminListDTO> privateAdminList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTeacherId() {
            return teacherId;
        }

        public void setTeacherId(String teacherId) {
            this.teacherId = teacherId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public int getDeleted() {
            return deleted;
        }

        public void setDeleted(int deleted) {
            this.deleted = deleted;
        }

        public List<PrivateAdminListDTO> getPrivateAdminList() {
            return privateAdminList;
        }

        public void setPrivateAdminList(List<PrivateAdminListDTO> privateAdminList) {
            this.privateAdminList = privateAdminList;
        }

        public static class PrivateAdminListDTO {
            /**
             * id : null
             * privateName : 雷丽
             * privateHeadPortrait : 1.png
             * cumulativeClass : null
             * certificate : null
             * favorableRate : null
             * evaluate : null
             * introduction : null
             * deleted : null
             */

            private Object id;
            private String privateName;
            private String privateHeadPortrait;
            private Object cumulativeClass;
            private Object certificate;
            private Object favorableRate;
            private Object evaluate;
            private Object introduction;
            private Object deleted;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getPrivateName() {
                return privateName;
            }

            public void setPrivateName(String privateName) {
                this.privateName = privateName;
            }

            public String getPrivateHeadPortrait() {
                return privateHeadPortrait;
            }

            public void setPrivateHeadPortrait(String privateHeadPortrait) {
                this.privateHeadPortrait = privateHeadPortrait;
            }

            public Object getCumulativeClass() {
                return cumulativeClass;
            }

            public void setCumulativeClass(Object cumulativeClass) {
                this.cumulativeClass = cumulativeClass;
            }

            public Object getCertificate() {
                return certificate;
            }

            public void setCertificate(Object certificate) {
                this.certificate = certificate;
            }

            public Object getFavorableRate() {
                return favorableRate;
            }

            public void setFavorableRate(Object favorableRate) {
                this.favorableRate = favorableRate;
            }

            public Object getEvaluate() {
                return evaluate;
            }

            public void setEvaluate(Object evaluate) {
                this.evaluate = evaluate;
            }

            public Object getIntroduction() {
                return introduction;
            }

            public void setIntroduction(Object introduction) {
                this.introduction = introduction;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }
        }
    }
}
