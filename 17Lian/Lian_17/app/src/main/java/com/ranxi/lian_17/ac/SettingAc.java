package com.ranxi.lian_17.ac;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.base.BaseAc;
import com.ranxi.lian_17.domain.ModyfyPersonalAc;

public class SettingAc extends BaseAc {

    private ImageView mBack;
    private TextView mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setStateColor();
    }

    private TextView mPersonalInfo;
    private TextView mModifyPwd;
    private TextView mLogout;

    @Override
    protected void initView() {
        mPersonalInfo = findViewById(R.id.personalInfo);
        mModifyPwd = findViewById(R.id.modifyPwd);
        mLogout = findViewById(R.id.logout);

        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        show(mBack, mTitle);
        mTitle.setText("个人信息");
        mPersonalInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ModyfyPersonalAc.class);
            startActivityForResult(intent, 1);
        });
        mModifyPwd.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ModyfyPwdAc.class);
            startActivity(intent);
        });
        mLogout.setOnClickListener(v -> {
            Intent intent=new Intent(getContext(),MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
            App.user=null;
            startActivity(intent);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 200) {
            setResult(200);
        }
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {

    }
}