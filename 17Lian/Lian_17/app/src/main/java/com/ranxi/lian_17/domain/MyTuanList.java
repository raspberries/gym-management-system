package com.ranxi.lian_17.domain;

import java.util.List;

public class MyTuanList {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1511672394258944002","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"人鱼马甲 咪炼健身","photo":"1.jpg","title":"人鱼马甲","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511683093240582146","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"有氧搏击 咪炼健身","photo":"1.jpg","title":"有氧","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511685643855564801","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"人鱼马甲 咪炼健身","photo":"1.jpg","title":"人鱼马甲","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511685700600303618","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"人鱼马甲 咪炼健身","photo":"1.jpg","title":"人鱼马甲","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511685798646353922","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"拳击课 拓普","photo":"1.jpg","title":"拳击","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511686233515986946","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"人鱼马甲 咪炼健身","photo":"1.jpg","title":"人鱼马甲","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511686292819251202","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"人鱼马甲 咪炼健身","photo":"2.jpg","title":"中强度、稳定性、马甲线","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511887581763162114","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"有氧搏击 咪炼健身","photo":"1.jpg","title":"有氧","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511887754153250817","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"有氧搏击 咪炼健身","photo":"1.jpg","title":"有氧","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511887763762401282","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"体态调整 拓普","photo":"2.jpg","title":"体态调整 拓普","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511892725003276290","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"减脂课 拓普","photo":"1.jpg","title":"减脂课","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]},{"id":"1511892806360190978","equipmentId":null,"userId":null,"equipmentCancel":null,"privateAdminCancel":null,"publicClassCancel":0,"privateAdminId":null,"publicClassId":null,"equipmentList":null,"privateAdminList":null,"publicClassList":[{"id":null,"name":"拳击课 拓普","photo":"1.jpg","title":"拳击训练","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * id : 1511672394258944002
         * equipmentId : null
         * userId : null
         * equipmentCancel : null
         * privateAdminCancel : null
         * publicClassCancel : 0
         * privateAdminId : null
         * publicClassId : null
         * equipmentList : null
         * privateAdminList : null
         * publicClassList : [{"id":null,"name":"人鱼马甲 咪炼健身","photo":"1.jpg","title":"人鱼马甲","teacherId":null,"time":null,"deleted":null,"privateClassList":null}]
         */

        private String id;
        private Object equipmentId;
        private Object userId;
        private Object equipmentCancel;
        private Object privateAdminCancel;
        private int publicClassCancel;
        private Object privateAdminId;
        private Object publicClassId;
        private Object equipmentList;
        private Object privateAdminList;
        private List<PublicClassListDTO> publicClassList;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Object getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(Object equipmentId) {
            this.equipmentId = equipmentId;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public Object getEquipmentCancel() {
            return equipmentCancel;
        }

        public void setEquipmentCancel(Object equipmentCancel) {
            this.equipmentCancel = equipmentCancel;
        }

        public Object getPrivateAdminCancel() {
            return privateAdminCancel;
        }

        public void setPrivateAdminCancel(Object privateAdminCancel) {
            this.privateAdminCancel = privateAdminCancel;
        }

        public int getPublicClassCancel() {
            return publicClassCancel;
        }

        public void setPublicClassCancel(int publicClassCancel) {
            this.publicClassCancel = publicClassCancel;
        }

        public Object getPrivateAdminId() {
            return privateAdminId;
        }

        public void setPrivateAdminId(Object privateAdminId) {
            this.privateAdminId = privateAdminId;
        }

        public Object getPublicClassId() {
            return publicClassId;
        }

        public void setPublicClassId(Object publicClassId) {
            this.publicClassId = publicClassId;
        }

        public Object getEquipmentList() {
            return equipmentList;
        }

        public void setEquipmentList(Object equipmentList) {
            this.equipmentList = equipmentList;
        }

        public Object getPrivateAdminList() {
            return privateAdminList;
        }

        public void setPrivateAdminList(Object privateAdminList) {
            this.privateAdminList = privateAdminList;
        }

        public List<PublicClassListDTO> getPublicClassList() {
            return publicClassList;
        }

        public void setPublicClassList(List<PublicClassListDTO> publicClassList) {
            this.publicClassList = publicClassList;
        }

        public static class PublicClassListDTO {
            /**
             * id : null
             * name : 人鱼马甲 咪炼健身
             * photo : 1.jpg
             * title : 人鱼马甲
             * teacherId : null
             * time : null
             * deleted : null
             * privateClassList : null
             */

            private Object id;
            private String name;
            private String photo;
            private String title;
            private Object teacherId;
            private Object time;
            private Object deleted;
            private Object privateClassList;

            public Object getId() {
                return id;
            }

            public void setId(Object id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhoto() {
                return photo;
            }

            public void setPhoto(String photo) {
                this.photo = photo;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public Object getTeacherId() {
                return teacherId;
            }

            public void setTeacherId(Object teacherId) {
                this.teacherId = teacherId;
            }

            public Object getTime() {
                return time;
            }

            public void setTime(Object time) {
                this.time = time;
            }

            public Object getDeleted() {
                return deleted;
            }

            public void setDeleted(Object deleted) {
                this.deleted = deleted;
            }

            public Object getPrivateClassList() {
                return privateClassList;
            }

            public void setPrivateClassList(Object privateClassList) {
                this.privateClassList = privateClassList;
            }
        }
    }
}
