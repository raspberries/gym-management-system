package com.ranxi.lian_17.base;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ranxi.lian_17.App;
import com.ranxi.lian_17.R;
import com.ranxi.lian_17.ac.AddMoneyAc;
import com.ranxi.lian_17.ac.JieYongHistoryAc;
import com.ranxi.lian_17.ac.MyTuanTiKeAc;
import com.ranxi.lian_17.ac.ShiJiaoAc;
import com.ranxi.lian_17.ac.VipAc;
import com.ranxi.lian_17.ac.WoDeQiCaiAc;
import com.ranxi.lian_17.domain.Api;
import com.youth.banner.Banner;
import com.youth.banner.indicator.CircleIndicator;

public abstract class BaseFr extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getRootView(), container, false);
    }

    protected abstract int getRootView();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initData();
        initListener();
    }

    protected abstract void initView(View view);

    protected abstract void initData();

    protected abstract void initListener();

    public void requestFocus(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    public void setImage(ImageView image, String userHeadPortrait) {
        Glide.with(getContext()).load(App.imageUrl + userHeadPortrait)
                .apply(RequestOptions.circleCropTransform())
                .error(R.mipmap.login_tx)
                .into(image);
    }

    public Banner setBanner(Banner banner) {
        return banner.setIndicator(new CircleIndicator(getContext()))
                .setIndicatorNormalColorRes(R.color.colorGray)
                .setIndicatorSelectedColorRes(R.color.colorPrimary)
                .setIndicatorWidth(25, 25);
    }

    public void error() {
        Toast.makeText(getContext(), "内容不能为空！", Toast.LENGTH_SHORT).show();
    }

    public void show(View... views) {
        for (View view : views) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static Api mApi = App.mApi;

    public void toActivity(Class<?> clazz) {
        Intent intent = new Intent(getContext(), clazz);
        startActivity(intent);
    }

    public void toActivityForResult(Class<?> clazz, int id) {
        Intent intent = new Intent(getContext(), clazz);
        startActivityForResult(intent, id);
    }

    public void toService(String name) {
        Class<?> clazz = null;
        switch (name) {
            case "我的器材":
                clazz = WoDeQiCaiAc.class;
                break;
            case "借用历史":
                clazz = JieYongHistoryAc.class;
                break;
            case "我的团体课":
                clazz = MyTuanTiKeAc.class;
                break;
            case "我的私教":
                clazz = ShiJiaoAc.class;
                break;
            case "续费会员":
                clazz = VipAc.class;
                break;
            case "充值":
                clazz = AddMoneyAc.class;
                break;
        }
        if (clazz != null) {
            Intent intent = new Intent(getContext(), clazz);
            startActivity(intent);
        }

    }
}
