package com.ranxi.lian_17.domain;

import java.util.List;

public class QiCaiList {
    /**
     * state : 200
     * message : null
     * data : {"records":[{"id":"2","name":"杠铃","number":"4","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"3","name":"椭圆机","number":"2","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"4","name":"动感单车","number":"4","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"5","name":"划船器","number":"8","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"6","name":"跑步机","number":"6","day":"7","createTime":"2022-03-29 11:13:01","deleted":0}],"total":5,"size":500,"current":1,"orders":[],"hitCount":false,"searchCount":true,"pages":1}
     */

    private int state;
    private Object message;
    private DataDTO data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * records : [{"id":"2","name":"杠铃","number":"4","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"3","name":"椭圆机","number":"2","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"4","name":"动感单车","number":"4","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"5","name":"划船器","number":"8","day":"7","createTime":"2022-03-29 11:13:01","deleted":0},{"id":"6","name":"跑步机","number":"6","day":"7","createTime":"2022-03-29 11:13:01","deleted":0}]
         * total : 5
         * size : 500
         * current : 1
         * orders : []
         * hitCount : false
         * searchCount : true
         * pages : 1
         */

        private int total;
        private int size;
        private int current;
        private boolean hitCount;
        private boolean searchCount;
        private int pages;
        private List<RecordsDTO> records;
        private List<?> orders;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getCurrent() {
            return current;
        }

        public void setCurrent(int current) {
            this.current = current;
        }

        public boolean isHitCount() {
            return hitCount;
        }

        public void setHitCount(boolean hitCount) {
            this.hitCount = hitCount;
        }

        public boolean isSearchCount() {
            return searchCount;
        }

        public void setSearchCount(boolean searchCount) {
            this.searchCount = searchCount;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public List<RecordsDTO> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsDTO> records) {
            this.records = records;
        }

        public List<?> getOrders() {
            return orders;
        }

        public void setOrders(List<?> orders) {
            this.orders = orders;
        }

        public static class RecordsDTO {
            public boolean isYuyue() {
                return isYuyue;
            }

            public void setYuyue(boolean yuyue) {
                isYuyue = yuyue;
            }

            /**
             * id : 2
             * name : 杠铃
             * number : 4
             * day : 7
             * createTime : 2022-03-29 11:13:01
             * deleted : 0
             */

private boolean isYuyue=false;
            private String id;
            private String name;
            private String number;
            private String day;
            private String createTime;
            private int deleted;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getNumber() {
                return number;
            }

            public void setNumber(String number) {
                this.number = number;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public int getDeleted() {
                return deleted;
            }

            public void setDeleted(int deleted) {
                this.deleted = deleted;
            }
        }
    }
}
