package com.ranxi.lian_17.domain;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface Api {

    @POST("/user/login")
    Call<UserLogin> userLogin(@QueryMap Map<String, Object> data);

    @POST("/user/register")
    Call<UserRegister> userRegister(@QueryMap Map<String, Object> data);

    @POST("/user/modify")
    Call<UserLogin> modifyUser(@QueryMap Map<String, Object> data);

    @Multipart
    @POST("/picture/uploadImage")
    Call<ImageUoload> uploadImage(@Part MultipartBody.Part part);

    @GET("/admin/findAllStore")
    Call<MenDian> getMenDian();

    /**
     * 获取团体课
     */
    @GET("/admin/findAllPublicClass")
    Call<TuanList> getTuanList();

    @GET("/admin/findAllPrivateAdmins")
    Call<ShiJiaoList> getShijiaoList();

    @GET("/admin/findAllPrivateClass")
    Call<ShijiaoClassList> getShijiaoClassList();

    @GET("/admin/findAllEquipments")
    Call<QiCaiList> getQiCaiList();

    @GET("/admin/InsertEquipmentReserve")
    Call<Result> yuyueQiCai(@QueryMap Map<String, Object> data);

    /**
     * 查看预约器材
     *
     * @return
     */
    @GET("/admin/findReserveEquipment")
    Call<WoDeQiCaiYuYue> getWoDeQicaiYuyue(@Query("userId") String userId);

    @GET("/admin/updateEquipmentReserve")
    Call<Result> unQiCai(@Query("id") String id);

    /**
     * 查询预约私教
     *
     * @return
     */
    @GET("/admin/findReservePrivateAdmin")
    Call<MyPrivateClass> getMyPrivateClass(@Query("userId") String userId);

    @GET("/admin/insertPrivateAdminReserve")
    Call<Result> payShiJiao(@Query("PrivateAdminId") String teacherId, @Query("userId") String id);

    @GET("/admin/updatePrivateReserve")
    Call<Result> unShijiao(@Query("id") String id);

    @GET("/admin/buyOfOrder")
    Call<Result> payOrdeer(@QueryMap Map<String, Object> data);

    @GET("/admin/insertPublicClassReserve")
    Call<Result> payTuanTike(@QueryMap Map<String, Object> data);

    @GET("/admin/findAllOrders")
    Call<MyOrder> getOrder();

    @GET("/admin/findReservePublicClass")
    Call<MyTuanList> getMyTuan(@Query("userId") String id);

    @GET("/admin/updatePublicReserve")
    Call<Result> unTuanTi(@Query("id") String id);
}
