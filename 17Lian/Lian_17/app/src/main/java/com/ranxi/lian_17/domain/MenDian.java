package com.ranxi.lian_17.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MenDian {
    /**
     * state : 200
     * message : null
     * data : [{"id":"1","name":"三体云健身","location":"589.41","title":"中海汇德里"},{"id":"2","name":"尼安德特健身","location":"590.49","title":"万宏路万宏国际A座1层N17号"},{"id":"3","name":"瀚斯健身","location":"594.90","title":"环城南路668号云纺国际商贸11层19号"},{"id":"4","name":"悦成健身","location":"598.30","title":"新亚洲体育诚悦成商业广场3楼"},{"id":"5","name":"玩铁健身","location":"599.15","title":"华都花园B区东50号商铺"}]
     */

    private int state;
    private Object message;
    private List<DataDTO> data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO implements Parcelable {
        /**
         * id : 1
         * name : 三体云健身
         * location : 589.41
         * title : 中海汇德里
         */

        private String id;
        private String name;
        private String location;
        private String title;

        protected DataDTO(Parcel in) {
            id = in.readString();
            name = in.readString();
            location = in.readString();
            title = in.readString();
        }

        public static final Creator<DataDTO> CREATOR = new Creator<DataDTO>() {
            @Override
            public DataDTO createFromParcel(Parcel in) {
                return new DataDTO(in);
            }

            @Override
            public DataDTO[] newArray(int size) {
                return new DataDTO[size];
            }
        };

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(id);
            dest.writeString(name);
            dest.writeString(location);
            dest.writeString(title);
        }
    }
}
