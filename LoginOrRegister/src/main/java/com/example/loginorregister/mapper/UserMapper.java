package com.example.loginorregister.mapper;

import com.example.loginorregister.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    User userLogin(User user);

    int userRegister(User user);

    List<User> getUser();
}
