package com.example.loginorregister.controller;

import com.example.loginorregister.domain.User;
import com.example.loginorregister.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserMapper mUserMapper;

    @PostMapping("/login")
    public User userLogin(@RequestBody User user) {
        return mUserMapper.userLogin(user);
    }

    @PostMapping("/register")
    public int userRegister(@RequestBody User user) {
        System.out.println(user);
        return mUserMapper.userRegister(user);
    }
    @GetMapping("/getUser")
    public List<User> userRegister() {
        return mUserMapper.getUser();
    }
}